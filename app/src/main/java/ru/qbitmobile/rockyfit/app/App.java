package ru.qbitmobile.rockyfit.app;

import android.content.Context;

import androidx.multidex.MultiDexApplication;

import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.model.DaoMaster;
import ru.qbitmobile.rockyfit.contentmanager.model.DaoSession;
import ru.qbitmobile.rockyfit.contentmanager.module.configurator.ConfigurationService;
import ru.qbitmobile.rockyfit.contentmanager.module.configurator.ProductionConfigurationModule;
import ru.qbitmobile.rockyfit.contentmanager.module.contextholder.IContextHolder;
import ru.qbitmobile.rockyfit.contentmanager.module.db.DaoOpenHelper;
import ru.qbitmobile.rockyfit.contentmanager.module.db.DBModule;
import ru.qbitmobile.rockyfit.contentmanager.module.db.DatabaseService;
import ru.qbitmobile.rockyfit.contentmanager.module.network.NetworkModule;
import ru.qbitmobile.rockyfit.contentmanager.module.network.NetworkService;
import ru.qbitmobile.rockyfit.contentmanager.module.network.connection.ConnectionModule;
import ru.qbitmobile.rockyfit.contentmanager.module.network.connection.ConnectionService;
import ru.qbitmobile.rockyfit.contentmanager.module.network.connection.InternetAvailabilityChecker;
import ru.qbitmobile.rockyfit.contentmanager.module.network.networktask.NetworkTaskModule;
import ru.qbitmobile.rockyfit.contentmanager.module.network.networktask.NetworkTaskService;
import ru.qbitmobile.rockyfit.contentmanager.module.sharedpreference.SharedPreferenceService;
import ru.qbitmobile.rockyfit.contentmanager.module.subscription.SubscriptionService;
import ru.qbitmobile.rockyfit.helper.DeviceIdProvider;

public class App extends MultiDexApplication {

    private static Context sAppContext;

    private static DaoSession sDaoSession;
    private static DaoOpenHelper sDaoOpenHelper;
    private static final String dbname = "rocky-db";


    @Override
    public void onCreate() {
        super.onCreate();

        sAppContext = this;

        InternetAvailabilityChecker.init(this);

        DependencyGraph.init();
        DependencyGraph.install(ConfigurationService.class, new ProductionConfigurationModule());
        DependencyGraph.install(DatabaseService.class, getDatabaseModule());
        DependencyGraph.install(ConnectionService.class, new ConnectionModule());
        DependencyGraph.install(NetworkTaskService.class, new NetworkTaskModule());
        DependencyGraph.install(NetworkService.class, new NetworkModule());

        DependencyGraph.get(IContextHolder.class).setContext(sAppContext);
        if (DependencyGraph.get(SharedPreferenceService.class).sharedGetDeviceId() == null) {
            String uuid = DeviceIdProvider.retrieveDeviceId();
            DependencyGraph.get(SharedPreferenceService.class).sharedSetDeviceId(uuid);
        }

        DependencyGraph.get(SubscriptionService.class).subscriptionInit();
    }

    private static DatabaseService getDatabaseModule() {
        if (sDaoSession == null) {
            sDaoSession = new DaoMaster(getDaoOpenHelper().getWritableDb()).newSession();
        }

        return new DBModule(sDaoSession);
    }

    private static DaoOpenHelper getDaoOpenHelper() {
        if (sDaoOpenHelper == null)
            sDaoOpenHelper = new DaoOpenHelper(sAppContext, dbname, null);
        return sDaoOpenHelper;
    }

}
