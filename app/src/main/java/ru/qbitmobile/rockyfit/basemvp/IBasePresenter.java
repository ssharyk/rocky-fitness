package ru.qbitmobile.rockyfit.basemvp;

public interface IBasePresenter {
    int TOAST_SHORT = 0;
    int TOAST_LONG = 1;

    void onCreate();
    void onResume();
    void onPause();
    void onDestroy();
}
