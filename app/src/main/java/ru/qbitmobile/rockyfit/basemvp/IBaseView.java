package ru.qbitmobile.rockyfit.basemvp;

import android.content.Context;

import androidx.annotation.StringRes;
import androidx.lifecycle.LifecycleOwner;

public interface IBaseView {
    BaseAppCompatActivity getMvpActivity();

    Context getContext();
    String getString(@StringRes int stringRes);

    LifecycleOwner getLifecycleOwner();

    void runOnUiThread(Runnable runnable);

    void makeToast(@StringRes int key, int length);
}
