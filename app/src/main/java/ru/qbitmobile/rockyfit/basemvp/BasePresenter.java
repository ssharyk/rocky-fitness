package ru.qbitmobile.rockyfit.basemvp;

/**
 * Базовый класс для всех презенткров в приложении
 */
public class BasePresenter implements IBasePresenter {

    @Override
    public void onCreate() {
    }

    @Override
    public void onResume() {
    }

    @Override
    public void onPause() {
    }

    @Override
    public void onDestroy() {
    }

}
