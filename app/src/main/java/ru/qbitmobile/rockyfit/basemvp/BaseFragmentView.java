package ru.qbitmobile.rockyfit.basemvp;

import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;

/**
 * Базовый класс для всех фрагментов в приложении, являющихся Passive View
 */
public class BaseFragmentView extends Fragment implements IBaseView {

    @Override
    public LifecycleOwner getLifecycleOwner() {
        return this;
    }

    @Override
    public BaseAppCompatActivity getMvpActivity() {
        if (getActivity() instanceof BaseAppCompatActivity) {
            return ((BaseAppCompatActivity) getActivity());
        }
        return null;
    }

    @Override
    public void runOnUiThread(Runnable runnable) {
        if (getActivity() != null) {
            getActivity().runOnUiThread(runnable);
        }
    }

    @Override
    public void makeToast(int key, int length) {
        if (getActivity() != null) {
            Toast.makeText(getActivity(), getResources().getString(key), length).show();
        }
    }
}
