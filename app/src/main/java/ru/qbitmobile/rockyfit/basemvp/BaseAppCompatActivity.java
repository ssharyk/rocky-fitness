package ru.qbitmobile.rockyfit.basemvp;

import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.LifecycleOwner;

import ru.qbitmobile.rockyfit.R;
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.module.contextholder.IContextHolder;

/**
 * Базовый класс для всех Activity в приложении, являющихся Passive View
 */
public class BaseAppCompatActivity extends AppCompatActivity implements IBaseView {
    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public BaseAppCompatActivity getMvpActivity() {
        return this;
    }

    @Override
    public LifecycleOwner getLifecycleOwner() {
        return this;
    }

    protected void inflateFragment(Fragment fragment, @IdRes int containerResId) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(containerResId, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void makeToast(@StringRes int key, int length) {
        Toast.makeText(this, getResources().getString(key), length).show();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DependencyGraph.get(IContextHolder.class).setContext(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Bitmap logoBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.main_logo);
            ActivityManager.TaskDescription taskDesc = new ActivityManager.TaskDescription(getString(R.string.app_name), logoBitmap, getResources().getColor(R.color.colorPrimary));
            setTaskDescription(taskDesc);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        DependencyGraph.get(IContextHolder.class).setContext(this);
    }
}
