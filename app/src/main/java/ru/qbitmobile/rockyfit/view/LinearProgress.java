package ru.qbitmobile.rockyfit.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import ru.qbitmobile.rockyfit.R;

public class LinearProgress extends View {

    // region Properties

    private int progressLevel;
    private int activeHeight;
    private int activeColor;
    private int inactiveHeight;
    private int inactiveColor;

    private final static int DEFAULT_ACTIVE_HEIGHT = 3;
    private final static int DEFAULT_INACTIVE_HEIGHT = 1;
    private final static int DEFAULT_ACTIVE_COLOR = R.color.colorPrimary;
    private final static int DEFAULT_INACTIVE_COLOR = R.color.colorAccent;

    // endregion

    // region Construction

    public LinearProgress(Context context) {
        this(context, null);
    }

    public LinearProgress(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LinearProgress(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public LinearProgress(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        Resources.Theme theme = context.getTheme();
        TypedArray a = theme.obtainStyledAttributes(attrs,
                R.styleable.LinearProgress, defStyleAttr, 0);

        this.progressLevel = a.getInt(R.styleable.LinearProgress_progressLevel, 0);
        this.activeHeight = a.getDimensionPixelSize(R.styleable.LinearProgress_progressActiveHeight, DEFAULT_ACTIVE_HEIGHT);
        this.inactiveHeight = a.getDimensionPixelSize(R.styleable.LinearProgress_progressInactiveHeight, DEFAULT_INACTIVE_HEIGHT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            this.activeColor = a.getColor(R.styleable.LinearProgress_progressActiveColor, context.getResources().getColor(DEFAULT_ACTIVE_COLOR, theme));
            this.inactiveColor = a.getColor(R.styleable.LinearProgress_progressActiveColor, context.getResources().getColor(DEFAULT_INACTIVE_COLOR, theme));
        } else {
            this.activeColor = a.getColor(R.styleable.LinearProgress_progressActiveColor, context.getResources().getColor(DEFAULT_ACTIVE_COLOR));
            this.inactiveColor = a.getColor(R.styleable.LinearProgress_progressActiveColor, context.getResources().getColor(DEFAULT_INACTIVE_COLOR));
        }
    }

    // endregion

    // region Draw

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        Paint activePaint = new Paint();
        activePaint.setColor(activeColor);
        int activeTop = (getMeasuredHeight() - activeHeight) / 2;
        int activeRight = getMeasuredWidth() * this.progressLevel / 100;
        canvas.drawRect(0, activeTop, activeRight, activeTop + activeHeight, activePaint);

        Paint inactivePaint = new Paint();
        inactivePaint.setColor(inactiveColor);
        int inactiveTop = (getMeasuredHeight() - inactiveHeight) / 2;
        canvas.drawRect(0, inactiveTop, getMeasuredWidth(), inactiveTop + inactiveHeight, inactivePaint);
    }

    // endregion

    // region Get/set

    public void setProgressLevel(int progress) {
        this.progressLevel = progress;
    }

    // endregion
}
