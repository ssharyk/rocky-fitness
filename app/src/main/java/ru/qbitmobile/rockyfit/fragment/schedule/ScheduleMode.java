package ru.qbitmobile.rockyfit.fragment.schedule;

import androidx.annotation.IntDef;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target({ElementType.PARAMETER, ElementType.FIELD, ElementType.TYPE_USE})
@IntDef({ScheduleMode.GRID, ScheduleMode.LINEAR, ScheduleMode.NUMBERS})
@Retention(RetentionPolicy.SOURCE)
public @interface ScheduleMode {
    int GRID = 1;
    int LINEAR = 2;
    int NUMBERS = 3;
}
