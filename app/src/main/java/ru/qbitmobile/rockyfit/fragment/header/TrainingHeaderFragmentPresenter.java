package ru.qbitmobile.rockyfit.fragment.header;

import ru.qbitmobile.rockyfit.R;
import ru.qbitmobile.rockyfit.basemvp.BasePresenter;
import ru.qbitmobile.rockyfit.contentmanager.model.Training;

public class TrainingHeaderFragmentPresenter extends BasePresenter implements LessonHeaderContract.ILessonHeaderPresenter {

    private LessonHeaderContract.ILessonHeaderView mView;
    private Training mTraining;

    public TrainingHeaderFragmentPresenter(LessonHeaderContract.ILessonHeaderView view, Training training) {
        this.mView = view;
        this.mTraining = training;
    }

    @Override
    public void init() {
        mView.showShortDescription(mTraining.getDescription());
        mView.showLessonId(mTraining.getId());
        mView.setPrimaryButtonText(mView.getContext().getResources().getString(R.string.start_training));
    }

    @Override
    public void onPrimaryButtonClick() {
        long firstVideoId = mTraining.getFirstVideoId();
        if (firstVideoId != -1L) {
            mView.launchVideo(firstVideoId);
        } else {
            mView.makeToast(R.string.warning_no_lessons_for_training, super.TOAST_SHORT);
        }
    }
}
