package ru.qbitmobile.rockyfit.fragment.header;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;

import ru.qbitmobile.rockyfit.R;
import ru.qbitmobile.rockyfit.activity.videolesson.VideoLessonView;
import ru.qbitmobile.rockyfit.basemvp.BaseFragmentView;
import ru.qbitmobile.rockyfit.commoncomponent.helper.Time;
import ru.qbitmobile.rockyfit.contentmanager.model.Lesson;
import ru.qbitmobile.rockyfit.contentmanager.model.Training;
import ru.qbitmobile.rockyfit.contentmanager.enumeration.LessonType;

public class LessonHeaderFragment extends BaseFragmentView implements LessonHeaderContract.ILessonHeaderView {

    private @LessonType long mContentType;
    private Lesson mContent;
    private LessonHeaderContract.ILessonHeaderPresenter mPresenter;

    private View mCustomView;
    private TextView mLessonId;
    private TextView mCoachName;
    private TextView mShortDescription;
    private TextView mTrainingName;
    private TextView mInfoLessonsCount;
    private TextView mInfoTotalDuration;
    private TextView mInfoComplexity;
    private View mContainer;
    private Button mPrimaryButton;
    private ImageView mBackground;

    private static final String EXTRA_FRAGMENT_TYPE = "ru.qbitmobile.rockyfit.fragment.header.FRAGMENT_TYPE";
    private static final String EXTRA_CONTENT = "ru.qbitmobile.rockyfit.fragment.header.CONTENT";

    public static LessonHeaderFragment newInstance(@LessonType long type, Lesson lesson) {
        Bundle arguments = new Bundle();
        arguments.putLong(EXTRA_FRAGMENT_TYPE, type);
        arguments.putParcelable(EXTRA_CONTENT, lesson);

        LessonHeaderFragment fragment = new LessonHeaderFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    protected LessonHeaderContract.ILessonHeaderPresenter createPresenter() {
        if (this.mContentType == LessonType.MASTER_CLASS) {
            return new CourseHeaderFragmentPresenter(this, (Training) mContent);
        } else if (this.mContentType == LessonType.TRAINING) {
            return new TrainingHeaderFragmentPresenter(this, (Training) mContent);
        }

        return null;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mCustomView = inflater.inflate(R.layout.fragment_lesson_header, container, false);

        resolveBundle();
        mPresenter = createPresenter();

        return mCustomView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        findViews();
        subscribeViews();
        initToolbar();
        showLessonInfo(mContent);
        mPresenter.init();
    }

    private void resolveBundle() {
        mContentType = getArguments().getLong(EXTRA_FRAGMENT_TYPE);
        mContent = getArguments().getParcelable(EXTRA_CONTENT);
    }

    private void findViews() {
        mContainer = mCustomView.findViewById(R.id.lesson_header_container);
        mCoachName = mCustomView.findViewById(R.id.lesson_header_coach_name);
        mLessonId = mCustomView.findViewById(R.id.lesson_header_lesson_id);
        mShortDescription = mCustomView.findViewById(R.id.lesson_header_short_description);
        mTrainingName = mCustomView.findViewById(R.id.lesson_header_training_name);
        mInfoLessonsCount = mCustomView.findViewById(R.id.lesson_header_steps_count);
        mInfoTotalDuration = mCustomView.findViewById(R.id.lesson_header_duration);
        mInfoComplexity = mCustomView.findViewById(R.id.lesson_header_complexity);
        mPrimaryButton = mCustomView.findViewById(R.id.lesson_header_button_start_course);
        mBackground = mCustomView.findViewById(R.id.lesson_header_background);
    }

    private void subscribeViews() {
        mPrimaryButton.setOnClickListener(v -> mPresenter.onPrimaryButtonClick());
    }

    private void initToolbar() {
        Toolbar toolbar = mCustomView.findViewById(R.id.lesson_header_toolbar);
        if (getActivity() != null && getActivity() instanceof AppCompatActivity) {
            ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

            ActionBar actionbar = ((AppCompatActivity) getActivity()).getSupportActionBar();
            if (actionbar != null) {
                actionbar.setDisplayShowTitleEnabled(false);
                actionbar.setDisplayHomeAsUpEnabled(true);
                actionbar.setHomeAsUpIndicator(R.drawable.icon_back);
            }
        }
    }

    @Override
    public void showLessonInfo(Lesson lesson) {
        if (lesson.getCoach() != null) {
            mCoachName.setText(lesson.getCoach().getFullName());
        }

        mTrainingName.setText(lesson.getName());

        String lessons = getResources().getString(R.string.lessons) + ": " + lesson.getLessonsCount();
        mInfoLessonsCount.setText(lessons);

        Time duration = new Time(0, 0, (int) lesson.getLessonsTotalDuration());
        mInfoTotalDuration.setText(duration.toString());
        mInfoComplexity.setText(lesson.getComplexityLevelTitle());

        Glide.with(this)
                .load(lesson.getPreviewImageUri())
                .override(getResources().getDisplayMetrics().widthPixels,
                        getResources().getDimensionPixelSize(R.dimen.lesson_header_height))
                .into(mBackground);
    }

    @Override
    public void hideCourseName() {
        mShortDescription.setVisibility(View.GONE);
    }

    @Override
    public void showShortDescription(String name) {
        mShortDescription.setText(name);
        mShortDescription.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLessonId() {
        mLessonId.setVisibility(View.GONE);
    }

    @Override
    public void showLessonId(long id) {
        mLessonId.setText(String.valueOf(id));
//        mLessonId.setVisibility(View.VISIBLE);
    }

    @Override
    public void setPrimaryButtonText(String text) {
        if (mPrimaryButton != null) {
            mPrimaryButton.setText(text);
        }
    }

    @Override
    public void launchVideo(long videoId) {
        getActivity().startActivity(
                VideoLessonView.createIntent(getContext(), videoId)
        );
    }
}
