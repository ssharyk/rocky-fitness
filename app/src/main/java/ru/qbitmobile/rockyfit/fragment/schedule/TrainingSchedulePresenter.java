package ru.qbitmobile.rockyfit.fragment.schedule;

import android.content.Context;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import ru.qbitmobile.rockyfit.adapter.AdapterVideoCard;
import ru.qbitmobile.rockyfit.contentmanager.module.subscription.SubscriptionService;
import ru.qbitmobile.rockyfit.contentmanager.updateobserver.VideoLessonUpdateObserver;
import ru.qbitmobile.rockyfit.commoncomponent.helper.Constant;
import ru.qbitmobile.rockyfit.likescontroller.VideoLessonItemClickListener;
import ru.qbitmobile.rockyfit.basemvp.BasePresenter;
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.enumeration.LessonType;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;
import ru.qbitmobile.rockyfit.contentmanager.repository.IVideoLessonsRepository;

import java.util.ArrayList;
import java.util.List;

public class TrainingSchedulePresenter extends BasePresenter
        implements ScheduleFragmentContract.ISchedulePresenter, AdapterVideoCard.Owner {

    private ScheduleFragmentContract.IScheduleView mView;
    private @ScheduleMode int mCurrentMode = ScheduleMode.LINEAR;
    private RecyclerView.Adapter mVideosAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private VideoLessonItemClickListener mVideoClickListener = new VideoLessonItemClickListener() {
        @Override
        public void onItemClick(int position, VideoLesson item) {
            if (item.isUnblocked()) {
                if (mView != null) {
                    mView.launchVideo(item.getId());
                }
            } else {
                DependencyGraph.get(SubscriptionService.class).subscriptionShowDialog(() -> {
                    if (mView != null) {
                        mView.runOnUiThread(() -> {
                            mVideosAdapter.notifyDataSetChanged();
                            mView.launchVideo(item.getId());
                        });
                    }
                });
            }
        }
    };
    private final int OBSERVER_TYPE = Constant.ObserverTypes.VIDEO_LESSON_SCHEDULE;

    private long mTrainingId;
    private List<VideoLesson> mVideoLessons = new ArrayList<>();

    public TrainingSchedulePresenter(ScheduleFragmentContract.IScheduleView view, @LessonType long lessonType, long trainingId) {
        this.mView = view;
        this.mTrainingId = trainingId;
    }

    @Override
    public void init() {
        mVideoLessons = DependencyGraph.get(IVideoLessonsRepository.class).getVideoLessonsByTrainingId(mTrainingId);
        if (mVideoLessons != null && mVideoLessons.size() > 0) {
            mView.hideNoLessons();
        } else {
            mView.showNoLessons();
            mView.hideModeOptions();
        }
        onModeChanged();
        mView.hideModeOptions();
    }

    @Override
    public void setScheduleMode(@ScheduleMode int mode) {
        if (mCurrentMode == mode) {
            return;
        }

        mCurrentMode = mode;
        onModeChanged();
    }

    private void onModeChanged() {
        switch (mCurrentMode) {
            case ScheduleMode.GRID:
                break;

            case ScheduleMode.LINEAR:
                mVideosAdapter = new AdapterVideoCard(this, mVideoLessons);
                ((AdapterVideoCard) mVideosAdapter).setItemClickListener(mVideoClickListener);
                mLayoutManager = new LinearLayoutManager(mView.getContext());
                mView.setGridModeInactive();
                mView.setLinearModeActive();
                break;

            case ScheduleMode.NUMBERS:
                break;
        }

        mView.setLayoutManager(mLayoutManager);
        mView.setAdapter(mVideosAdapter);
    }

    @Override
    public Context getContext() {
        return mView.getContext();
    }

    @Override
    public void subscribeToChanges(VideoLessonUpdateObserver observer) {
        if (mVideoLessons == null) return;
        for (VideoLesson lesson : mVideoLessons) {
            if (lesson.getId() == observer.getVideoLessonId()) {
                lesson.addObserver(observer);
            }
        }
    }

    @Override
    public int getObserverType() {
        return OBSERVER_TYPE;
    }

    @Override
    public void onDestroy() {
        if (mVideoLessons == null) return;
        for (VideoLesson lesson : mVideoLessons) {
            lesson.removeObservers(OBSERVER_TYPE);
        }
    }
}
