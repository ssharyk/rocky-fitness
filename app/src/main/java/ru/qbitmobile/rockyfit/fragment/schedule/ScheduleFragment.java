package ru.qbitmobile.rockyfit.fragment.schedule;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import ru.qbitmobile.rockyfit.R;
import ru.qbitmobile.rockyfit.activity.videolesson.VideoLessonView;
import ru.qbitmobile.rockyfit.basemvp.BaseFragmentView;
import ru.qbitmobile.rockyfit.contentmanager.enumeration.LessonType;

public class ScheduleFragment extends BaseFragmentView implements ScheduleFragmentContract.IScheduleView {

    private static final String EXTRA_LESSON_TYPE = "ru.qbitmobile.rockyfit.fragment.schedule.LESSON_TYPE";
    private static final String EXTRA_LESSON_ID = "ru.qbitmobile.rockyfit.fragment.schedule.LESSON_ID";

    public static ScheduleFragment newInstance(@LessonType long lessonType, long lessonId) {
        ScheduleFragment fragment = new ScheduleFragment();
        Bundle arguments = new Bundle();
        arguments.putLong(EXTRA_LESSON_TYPE, lessonType);
        arguments.putLong(EXTRA_LESSON_ID, lessonId);
        fragment.setArguments(arguments);
        return fragment;
    }

    private @LessonType long mLessonType;
    private long mLessonId;
    private View mCustomView;
    private View mModesContainer;
    private RecyclerView mTrainingsList;
    private ImageView mModeGrid;
    private ImageView mModeLinear;
    private TextView mNoResult;

    private ScheduleFragmentContract.ISchedulePresenter mPresenter;

    protected ScheduleFragmentContract.ISchedulePresenter createPresenter() {
        if (mLessonType == LessonType.MASTER_CLASS) {
            return new TrainingSchedulePresenter(this, LessonType.MASTER_CLASS, mLessonId);
        } else if (mLessonType == LessonType.TRAINING) {
            return new TrainingSchedulePresenter(this, LessonType.TRAINING, mLessonId);
        } else {
            return null;
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mCustomView = inflater.inflate(R.layout.fragment_schedule, container, false);
        return mCustomView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        resolveArguments();
        mPresenter = createPresenter();

        findViews();
        subscribeViews();

        mPresenter.init();
    }

    @Override
    public void onDestroy() {
        if (mPresenter != null) {
            mPresenter.onDestroy();
        }
        super.onDestroy();
    }

    private void resolveArguments() {
        mLessonType = getArguments().getLong(EXTRA_LESSON_TYPE);
        mLessonId = getArguments().getLong(EXTRA_LESSON_ID);
    }

    private void findViews() {
        this.mModesContainer = mCustomView.findViewById(R.id.course_schedule_modes_container);
        this.mModeGrid = mCustomView.findViewById(R.id.course_schedule_mode_grid);
        this.mModeLinear = mCustomView.findViewById(R.id.course_schedule_mode_linear);
        this.mTrainingsList = mCustomView.findViewById(R.id.course_schedule_trainings_list);
        this.mNoResult = mCustomView.findViewById(R.id.course_schedule_no_result);
    }

    private void subscribeViews() {
        mModeGrid.setOnClickListener(v -> mPresenter.setScheduleMode(ScheduleMode.GRID));
        mModeLinear.setOnClickListener(v -> mPresenter.setScheduleMode(ScheduleMode.LINEAR));
    }

    @Override
    public void showNoLessons() {
        mNoResult.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideNoLessons() {
        mNoResult.setVisibility(View.GONE);
    }

    @Override
    public void setLayoutManager(RecyclerView.LayoutManager layoutManager) {
        mTrainingsList.setLayoutManager(layoutManager);
    }

    @Override
    public void setAdapter(RecyclerView.Adapter trainingsAdapter) {
        mTrainingsList.setAdapter(trainingsAdapter);
    }

    @Override
    public void showModeOptions() {
        mModesContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideModeOptions() {
        mModesContainer.setVisibility(View.GONE);
    }

    @Override
    public void launchVideo(long videoId) {
        getActivity().startActivity(
                VideoLessonView.createIntent(getContext(), videoId)
        );
    }

    @Override
    public void setGridModeActive() {
        mModeGrid.setImageDrawable(getResources().getDrawable(R.drawable.switch_mode_square_active));
    }

    @Override
    public void setGridModeInactive() {
        mModeGrid.setImageDrawable(getResources().getDrawable(R.drawable.switch_mode_square_inactive));
    }

    @Override
    public void setLinearModeActive() {
        mModeLinear.setImageDrawable(getResources().getDrawable(R.drawable.switch_mode_linear_active));
    }

    @Override
    public void setLinearModeInactive() {
        mModeLinear.setImageDrawable(getResources().getDrawable(R.drawable.switch_mode_linear_inactive));
    }
}
