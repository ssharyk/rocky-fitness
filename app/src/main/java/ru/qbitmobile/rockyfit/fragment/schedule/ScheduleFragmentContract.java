package ru.qbitmobile.rockyfit.fragment.schedule;

import androidx.recyclerview.widget.RecyclerView;

import ru.qbitmobile.rockyfit.basemvp.IBasePresenter;
import ru.qbitmobile.rockyfit.basemvp.IBaseView;

public interface ScheduleFragmentContract {

    interface IScheduleView extends IBaseView {

        void launchVideo(long videoId);

        void setGridModeActive();
        void setGridModeInactive();
        void setLinearModeActive();
        void setLinearModeInactive();

        void showNoLessons();
        void hideNoLessons();

        void setLayoutManager(RecyclerView.LayoutManager layoutManager);
        void setAdapter(RecyclerView.Adapter trainingsAdapter);

        void showModeOptions();
        void hideModeOptions();
    }

    interface ISchedulePresenter extends IBasePresenter {
        void init();

        void setScheduleMode(@ScheduleMode int mode);
    }
}
