package ru.qbitmobile.rockyfit.fragment.description;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager;
import com.beloo.widget.chipslayoutmanager.SpacingItemDecoration;
import ru.qbitmobile.rockyfit.R;
import ru.qbitmobile.rockyfit.activity.videolesson.VideoLessonView;
import ru.qbitmobile.rockyfit.adapter.AdapterImages;
import ru.qbitmobile.rockyfit.adapter.AdapterTags;
import ru.qbitmobile.rockyfit.basemvp.BaseFragmentView;
import ru.qbitmobile.rockyfit.contentmanager.model.Lesson;
import ru.qbitmobile.rockyfit.contentmanager.enumeration.LessonType;
import ru.qbitmobile.rockyfit.helper.SwiperIndicatorDecoration;
import com.yarolegovich.discretescrollview.DiscreteScrollView;

public class DescriptionFragment extends BaseFragmentView implements DescriptionFragmentContract.IDescriptionView {
    private View mCustomView;

    private TextView mInfoCategoryName;
    private TextView mInfoDescription;
    private DiscreteScrollView mImagesList;
    private RecyclerView mTagsList;

    private @LessonType long mContentType;
    private Lesson mContent;

    private DescriptionFragmentContract.IDescriptionPresenter mPresenter;

    private static final String EXTRA_FRAGMENT_TYPE = "ru.qbitmobile.rockyfit.fragment.description.FRAGMENT_TYPE";
    private static final String EXTRA_CONTENT = "ru.qbitmobile.rockyfit.fragment.description.CONTENT";

    public static DescriptionFragment newInstance(@LessonType long type, Lesson lesson) {
        Bundle arguments = new Bundle();
        arguments.putLong(EXTRA_FRAGMENT_TYPE, type);
        arguments.putParcelable(EXTRA_CONTENT, lesson);

        DescriptionFragment fragment = new DescriptionFragment();
        fragment.setArguments(arguments);
        return fragment;
    }

    protected DescriptionFragmentContract.IDescriptionPresenter createPresenter() {
        if (this.mContentType == LessonType.MASTER_CLASS)
            return new CourseDescriptionFragmentPresenter(this, mContent.getId());
        else if (this.mContentType == LessonType.TRAINING)
            return new TrainingDescriptionFragmentPresenter(this, mContent.getId());
        return null;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mCustomView = inflater.inflate(R.layout.fragment_description, container, false);

        resolveBundle();
        mPresenter = createPresenter();

        return mCustomView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        findViews();

        showLessonInfo(mContent);
        mPresenter.loadImages();
        if (mPresenter.shouldShowTags()) {
            mPresenter.loadTags();
        } else {
            hideTagsList();
        }
    }

    private void resolveBundle() {
        mContentType = getArguments().getLong(EXTRA_FRAGMENT_TYPE);
        mContent = getArguments().getParcelable(EXTRA_CONTENT);
    }

    private void findViews() {
        mInfoCategoryName = mCustomView.findViewById(R.id.details_category_name);
        mInfoDescription = mCustomView.findViewById(R.id.details_description);
        mImagesList = mCustomView.findViewById(R.id.details_images_list);
        mTagsList = mCustomView.findViewById(R.id.details_tags_list);
    }

    @Override
    public void showLessonInfo(Lesson lesson) {
        mInfoCategoryName.setText(lesson.getName());
        mInfoDescription.setText(lesson.getDescriptionFull());
    }

    @Override
    public void hideMediaList() {
        mImagesList.setVisibility(View.GONE);
    }

    @Override
    public void showMediaList() {
        mImagesList.setVisibility(View.VISIBLE);
    }

    @Override
    public void setMediaAdapter(AdapterImages adapter) {
        mImagesList.addItemDecoration(new SwiperIndicatorDecoration());
        mImagesList.setAdapter(adapter);
    }

    @Override
    public void hideTagsList() {
        mTagsList.setVisibility(View.GONE);
    }

    @Override
    public void showTagsList() {
        mTagsList.setVisibility(View.VISIBLE);
    }

    @Override
    public void setTagsAdapter(AdapterTags tags) {
        ChipsLayoutManager.Builder builder = ChipsLayoutManager.newBuilder(getContext());
        if (builder != null && builder instanceof ChipsLayoutManager.StrategyBuilder) {
            builder = ((ChipsLayoutManager.StrategyBuilder) builder).withLastRow(true);
        }
        ChipsLayoutManager chipsLayoutManager = builder.build();
        mTagsList.setLayoutManager(chipsLayoutManager);
        mTagsList.addItemDecoration(new SpacingItemDecoration(
                getResources().getDimensionPixelOffset(R.dimen.tag_margin),
                getResources().getDimensionPixelOffset(R.dimen.tag_margin)));
        mTagsList.setAdapter(tags);
    }

    @Override
    public void launchVideo(long videoLessonId) {
        getActivity().startActivity(
                VideoLessonView.createIntent(getContext(), videoLessonId)
        );
    }
}
