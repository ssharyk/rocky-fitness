package ru.qbitmobile.rockyfit.fragment.description;

import android.content.Context;

import ru.qbitmobile.rockyfit.adapter.AdapterImages;
import ru.qbitmobile.rockyfit.adapter.AdapterTags;
import ru.qbitmobile.rockyfit.contentmanager.module.subscription.SubscriptionService;
import ru.qbitmobile.rockyfit.contentmanager.updateobserver.VideoLessonUpdateObserver;
import ru.qbitmobile.rockyfit.commoncomponent.helper.Constant;
import ru.qbitmobile.rockyfit.likescontroller.VideoLessonItemClickListener;
import ru.qbitmobile.rockyfit.basemvp.BasePresenter;
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.model.Tag;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;
import ru.qbitmobile.rockyfit.contentmanager.repository.ITagRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.IVideoLessonsRepository;

import java.util.List;

public class CourseDescriptionFragmentPresenter extends BasePresenter implements DescriptionFragmentContract.IDescriptionPresenter, AdapterImages.Owner {

    private DescriptionFragmentContract.IDescriptionView mView;
    private long mCourseId;
    private AdapterImages mImagesAdapter;
    private List<VideoLesson> mFiles;
    private final int OBSERVER_TYPE = Constant.ObserverTypes.VIDEO_LESSON_PREVIEWS;

    public CourseDescriptionFragmentPresenter(DescriptionFragmentContract.IDescriptionView view, long courseId) {
        this.mView = view;
        this.mCourseId = courseId;
    }

    @Override
    public void init() {
    }

    @Override
    public void loadImages() {
        mFiles = DependencyGraph.get(IVideoLessonsRepository.class).getVideoLessonsByTrainingId(mCourseId);
        mImagesAdapter = new AdapterImages(this, mFiles);
        mImagesAdapter.setItemClickListener(new VideoLessonItemClickListener() {
            @Override
            public void onItemClick(int position, VideoLesson item) {
                if (item.isUnblocked()) {
                    if (mView != null) {
                        mView.launchVideo(item.getId());
                    }
                } else {
                    DependencyGraph.get(SubscriptionService.class).subscriptionShowDialog(() -> {
                        if (mView != null) {
                            mView.runOnUiThread(() -> {
                                mImagesAdapter.notifyDataSetChanged();
                                mView.launchVideo(item.getId());
                            });
                        }
                    });
                }
            }
        });
        if (mFiles.size() > 0) {
            mView.runOnUiThread(() -> {
                mView.showMediaList();
                mView.setMediaAdapter(mImagesAdapter);
            });
        } else {
            mView.runOnUiThread(() -> {
                mView.hideMediaList();
            });
        }
    }

    @Override
    public boolean shouldShowTags() {
        return true;
    }

    @Override
    public void loadTags() {
        if (!shouldShowTags()) {
            mView.runOnUiThread(() -> {
                mView.hideTagsList();
            });
            return;
        }

        List<Tag> tags = DependencyGraph.get(ITagRepository.class).getTagsByCourseId(mCourseId);
        if (tags != null) {
            mView.runOnUiThread(() -> {
                mView.showTagsList();
                mView.setTagsAdapter(new AdapterTags(mView.getContext(), tags, new AdapterTags.UnselectableItemsListener()));
            });
        } else {
            mView.runOnUiThread(() -> {
                mView.hideTagsList();
            });
        }
    }

    @Override
    public void onDestroy() {
        if (mFiles == null) return;
        for (VideoLesson lesson : mFiles) {
            lesson.removeObservers(OBSERVER_TYPE);
        }
    }

    @Override
    public Context getContext() {
        return mView.getContext();
    }

    @Override
    public void subscribeToChanges(VideoLessonUpdateObserver observer) {
        if (mFiles == null) return;
        for (VideoLesson lesson : mFiles) {
            if (lesson.getId() == observer.getVideoLessonId()) {
                lesson.addObserver(observer);
            }
        }
    }

    @Override
    public int getObserverType() {
        return OBSERVER_TYPE;
    }
}
