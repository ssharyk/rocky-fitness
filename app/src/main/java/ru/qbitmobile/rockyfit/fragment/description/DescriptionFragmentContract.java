package ru.qbitmobile.rockyfit.fragment.description;

import ru.qbitmobile.rockyfit.adapter.AdapterImages;
import ru.qbitmobile.rockyfit.adapter.AdapterTags;
import ru.qbitmobile.rockyfit.basemvp.IBasePresenter;
import ru.qbitmobile.rockyfit.basemvp.IBaseView;
import ru.qbitmobile.rockyfit.contentmanager.model.Lesson;

public interface DescriptionFragmentContract {
    interface IDescriptionView extends IBaseView {
        void showLessonInfo(Lesson lesson);

        void hideMediaList();
        void showMediaList();
        void setMediaAdapter(AdapterImages adapter);

        void hideTagsList();
        void showTagsList();
        void setTagsAdapter(AdapterTags tags);

        void launchVideo(long videoLessonId);
    }

    interface IDescriptionPresenter extends IBasePresenter {
        void init();

        void loadImages();

        boolean shouldShowTags();
        void loadTags();
    }
}
