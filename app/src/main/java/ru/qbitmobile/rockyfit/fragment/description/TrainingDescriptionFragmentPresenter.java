package ru.qbitmobile.rockyfit.fragment.description;

import android.content.Context;

import ru.qbitmobile.rockyfit.adapter.AdapterImages;
import ru.qbitmobile.rockyfit.contentmanager.updateobserver.VideoLessonUpdateObserver;
import ru.qbitmobile.rockyfit.commoncomponent.helper.Constant;
import ru.qbitmobile.rockyfit.likescontroller.VideoLessonItemClickListener;
import ru.qbitmobile.rockyfit.basemvp.BasePresenter;
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;
import ru.qbitmobile.rockyfit.contentmanager.repository.IVideoLessonsRepository;

import java.util.List;

public class TrainingDescriptionFragmentPresenter extends BasePresenter implements DescriptionFragmentContract.IDescriptionPresenter, AdapterImages.Owner {

    private DescriptionFragmentContract.IDescriptionView mView;
    private long mTrainingId;
    private AdapterImages mImagesAdapter;
    private List<VideoLesson> mFiles;
    private final int OBSERVER_TYPE = Constant.ObserverTypes.VIDEO_LESSON_PREVIEWS;

    public TrainingDescriptionFragmentPresenter(DescriptionFragmentContract.IDescriptionView view, long courseId) {
        this.mView = view;
        this.mTrainingId = courseId;
    }

    @Override
    public void init() {
        mView.hideTagsList();
    }

    @Override
    public void loadImages() {
        mFiles = DependencyGraph.get(IVideoLessonsRepository.class).getVideoLessonsByTrainingId(mTrainingId);
        mImagesAdapter = new AdapterImages(this, mFiles);
        mImagesAdapter.setItemClickListener(new VideoLessonItemClickListener() {
            @Override
            public void onItemClick(int position, VideoLesson item) {
                mView.launchVideo(item.getId());
            }
        });
        if (mFiles.size() > 0) {
            mView.runOnUiThread(() -> {
                mView.showMediaList();
                mView.setMediaAdapter(mImagesAdapter);
            });
        } else {
            mView.runOnUiThread(() -> {
                mView.hideMediaList();
            });
        }
    }

    @Override
    public boolean shouldShowTags() {
        return false;
    }

    @Override
    public void loadTags() {
        mView.runOnUiThread(() -> {
            mView.hideTagsList();
        });
    }

    @Override
    public void onDestroy() {
        if (mFiles == null) return;
        for (VideoLesson lesson : mFiles) {
            lesson.removeObservers(OBSERVER_TYPE);
        }
    }

    @Override
    public Context getContext() {
        return mView.getContext();
    }

    @Override
    public void subscribeToChanges(VideoLessonUpdateObserver observer) {
        if (mFiles == null) return;
        for (VideoLesson lesson : mFiles) {
            if (lesson.getId() == observer.getVideoLessonId()) {
                lesson.addObserver(observer);
            }
        }
    }

    @Override
    public int getObserverType() {
        return OBSERVER_TYPE;
    }
}
