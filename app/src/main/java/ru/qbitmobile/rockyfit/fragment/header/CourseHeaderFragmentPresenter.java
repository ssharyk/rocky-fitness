package ru.qbitmobile.rockyfit.fragment.header;

import ru.qbitmobile.rockyfit.R;
import ru.qbitmobile.rockyfit.basemvp.BasePresenter;
import ru.qbitmobile.rockyfit.contentmanager.model.Training;

public class CourseHeaderFragmentPresenter extends BasePresenter implements LessonHeaderContract.ILessonHeaderPresenter {

    private LessonHeaderContract.ILessonHeaderView mView;
    private Training mTraining;

    public CourseHeaderFragmentPresenter(LessonHeaderContract.ILessonHeaderView view, Training training) {
        this.mView = view;
        this.mTraining = training;
    }

    @Override
    public void init() {
        mView.hideCourseName();
        mView.hideLessonId();
        mView.setPrimaryButtonText(mView.getContext().getResources().getString(R.string.start_course));
    }

    @Override
    public void onPrimaryButtonClick() {
//        mView.launchTraining(mCourse.getFirstTrainingId());
    }
}
