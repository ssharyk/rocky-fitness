package ru.qbitmobile.rockyfit.fragment.header;

import ru.qbitmobile.rockyfit.basemvp.IBasePresenter;
import ru.qbitmobile.rockyfit.basemvp.IBaseView;
import ru.qbitmobile.rockyfit.contentmanager.model.Lesson;

public interface LessonHeaderContract {
    interface ILessonHeaderView extends IBaseView {

        void showLessonInfo(Lesson lesson);

        void hideCourseName();

        void showShortDescription(String name);

        void hideLessonId();

        void showLessonId(long id);

        void setPrimaryButtonText(String text);

        void launchVideo(long videoId);
    }

    interface ILessonHeaderPresenter extends IBasePresenter {

        void init();

        void onPrimaryButtonClick();
    }
}
