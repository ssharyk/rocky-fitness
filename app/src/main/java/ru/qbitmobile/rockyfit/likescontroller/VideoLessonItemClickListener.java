package ru.qbitmobile.rockyfit.likescontroller;

import ru.qbitmobile.rockyfit.adapter.ItemClickListener;
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;
import ru.qbitmobile.rockyfit.contentmanager.repository.ILikeRepository;

public abstract class VideoLessonItemClickListener implements ItemClickListener<VideoLesson> {
    public void onLikeClick(VideoLesson lesson) {
        DependencyGraph.get(ILikeRepository.class).setLike(lesson, true);
    }

    public void onDislikeClick(VideoLesson lesson) {
        DependencyGraph.get(ILikeRepository.class).setLike(lesson, false);
    }
}
