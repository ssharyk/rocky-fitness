package ru.qbitmobile.rockyfit.likescontroller;

import ru.qbitmobile.rockyfit.R;
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.model.Markable;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;
import ru.qbitmobile.rockyfit.contentmanager.module.contextholder.IContextHolder;

public class LikeController {
    public static void onClick(ILikeViewHolder holder, VideoLesson lesson) {

        boolean isSetNow = !lesson.getIsLike();
        lesson.setIsLike(isSetNow);
        if (holder.getVideosLessonClickListener() != null) {
            if (isSetNow) {
                holder.getVideosLessonClickListener().onLikeClick(lesson);
                lesson.setLikes(lesson.getLikes() + 1);
            } else {
                holder.getVideosLessonClickListener().onDislikeClick(lesson);
                lesson.setLikes(lesson.getLikes() - 1);
            }
        }

        bindLikeIcon(holder, lesson);
    }

    public static void bindLikeIcon(ILikeViewHolder viewHolder, Markable lesson) {
        viewHolder.getLikesCount().setText(String.valueOf(lesson.getLikes()));
        if (lesson.getIsLike()) {
            viewHolder.getLikesCount().setTextColor(DependencyGraph.get(IContextHolder.class).getContext().getResources().getColor(R.color.colorDefaultGolden));
            viewHolder.getLikesIcon().setImageDrawable(DependencyGraph.get(IContextHolder.class).getContext().getResources().getDrawable(R.drawable.like_golden));
        } else {
            viewHolder.getLikesCount().setTextColor(DependencyGraph.get(IContextHolder.class).getContext().getResources().getColor(R.color.content_color));
            viewHolder.getLikesIcon().setImageDrawable(DependencyGraph.get(IContextHolder.class).getContext().getResources().getDrawable(R.drawable.like_white));
        }
    }
}
