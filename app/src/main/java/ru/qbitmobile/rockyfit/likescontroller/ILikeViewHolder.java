package ru.qbitmobile.rockyfit.likescontroller;

import android.widget.ImageView;
import android.widget.TextView;

public interface ILikeViewHolder {
    VideoLessonItemClickListener getVideosLessonClickListener();

    TextView getLikesCount();

    ImageView getLikesIcon();
}
