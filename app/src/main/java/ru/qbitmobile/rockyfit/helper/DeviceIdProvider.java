package ru.qbitmobile.rockyfit.helper;

import android.content.Context;
import android.provider.Settings;

import java.util.UUID;

import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.module.contextholder.IContextHolder;

public final class DeviceIdProvider {
    public static final String retrieveDeviceId() {

        Context context = DependencyGraph.get(IContextHolder.class).getContext();
        try {
            return "android-" + Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (Throwable e) {
            return "android-" + UUID.randomUUID().toString();
        }
    }
}
