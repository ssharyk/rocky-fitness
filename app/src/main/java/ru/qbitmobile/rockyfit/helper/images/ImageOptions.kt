package ru.qbitmobile.rockyfit.helper.images

sealed class ImageOption {
    companion object {
        const val DEFAULT_BLUR = 7
    }
}

data class BlurImageOption(val blurRadius: Int) : ImageOption()

data class SizeImageOption(val width: Int, val height: Int) : ImageOption()

object NoProgressOption: ImageOption()

interface ImageOptionsFactory<T : Any> {
    fun transform(option: ImageOption): T?
}

abstract class BaseImageOptionsFactory<T : Any> : ImageOptionsFactory<T> {
    fun transform(options: List<ImageOption>): List<T> =
            options.mapNotNull { transform(it) }
}