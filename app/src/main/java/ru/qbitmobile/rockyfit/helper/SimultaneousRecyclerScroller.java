package ru.qbitmobile.rockyfit.helper;

import android.view.MotionEvent;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;
import java.util.List;

public class SimultaneousRecyclerScroller {

    private ViewGroup mContainer;
    private int mTouchedRvTag = 0;

    public SimultaneousRecyclerScroller(ViewGroup container) {
        this.mContainer = container;
    }

    private RecyclerView.OnScrollListener mListScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if ((int) recyclerView.getTag() == mTouchedRvTag) {
                for (int noOfRecyclerView = 1; noOfRecyclerView <= 2; noOfRecyclerView++) {
                    if (noOfRecyclerView != (int) recyclerView.getTag()) {
                        RecyclerView tempRecyclerView = (RecyclerView) mContainer.findViewWithTag(noOfRecyclerView);
                        tempRecyclerView.scrollBy(dx, dy);
                    }
                }
            }
        }
    };

    private RecyclerView.OnItemTouchListener mListItemTouchListener = new RecyclerView.OnItemTouchListener() {
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            mTouchedRvTag = (int) rv.getTag();
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        }
    };

    public void install(RecyclerView... recyclerViews) {
        install(Arrays.asList(recyclerViews));
    }

    public void install(List<RecyclerView> recyclerViews) {
        for (int i = 0; i < recyclerViews.size(); i++) {
            RecyclerView rv = recyclerViews.get(i);
            installInternal(rv, i + 1);
        }
    }

    private void installInternal(RecyclerView rv, int tag) {
        rv.setTag(tag);
        rv.addOnScrollListener(mListScrollListener);
        rv.addOnItemTouchListener(mListItemTouchListener);
    }
}
