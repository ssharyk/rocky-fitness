package ru.qbitmobile.rockyfit.helper.images;

import android.content.Context;
import android.widget.ImageView;

import androidx.swiperefreshlayout.widget.CircularProgressDrawable;

import com.bumptech.glide.request.RequestOptions;

import java.util.Arrays;
import java.util.List;

import ru.qbitmobile.rockyfit.R;

/**
 * Адаптер для отображения картинок.
 * Делегирует работу специализированным библиотекам.
 * В настоящее время используется Glide
 */
public final class ImageHelper {

    public static void render(Context context, ImageView imageView, String uri, List<ImageOption> options) {
        int width = 0, height = 0;
        for (int i = 0; i < options.size(); i++) {
            if (options.get(i) instanceof SizeImageOption) {
                SizeImageOption size  =((SizeImageOption) options.get(i));
                width = size.getWidth();
                height = size.getHeight();
                break;
            }
        }

        boolean withProgress = true;
        for (ImageOption option : options) {
            if (option instanceof NoProgressOption) {
                withProgress = false;
                break;
            }
        }
        render(context, imageView, uri, width, height, withProgress, options);
    }

    private static void render(Context context, ImageView imageView, String uri, int width, int height, boolean withProgress, List<ImageOption> options) {
        if (withProgress) {
            // установить индикатор загрузки
            configureProgress(context, imageView);
        } else {
            imageView.setBackground(null);
        }

        // делегировать работу glide
        RequestOptions requestOptions = GlideHelper.createOptions(options);
        GlideHelper.render(context, imageView, uri, width, height, requestOptions);
    }

    public static void render(Context context, ImageView imageView, String uri, ImageOption... options) {
        render(context, imageView, uri, Arrays.asList(options));
    }

    static CircularProgressDrawable configureProgress(Context context, ImageView imageView) {
        CircularProgressDrawable progressDrawable = new CircularProgressDrawable(context);
        progressDrawable.setColorSchemeColors(context.getResources().getColor(R.color.colorDefaultGolden));
        progressDrawable.setStrokeWidth(4.0f);
        progressDrawable.setCenterRadius(30.0f);
        progressDrawable.start();
        imageView.setBackground(progressDrawable);
        return progressDrawable;
    }
}
