package ru.qbitmobile.rockyfit.helper;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yarolegovich.discretescrollview.DiscreteScrollView;

public class SwiperIndicatorDecoration extends RecyclerView.ItemDecoration {

    private static final float BIG_DOT_RADIUS = 5f;
    private static final float SMALL_DOT_RADIUS = 3f;
    private static final float CORNER_RADIUS = 5f;
    private static final float SELECTED_ITEM_WIDTH = 36f;

    private static final float DP = Resources.getSystem().getDisplayMetrics().density;

    /**
     * Height of the space the indicator takes up at the bottom of the view.
     */
    private final int mIndicatorHeight = (int) (DP * 16);

    /**
     * Indicator stroke width.
     */
    private final float mIndicatorStrokeWidth = DP * 2;

    /**
     * Indicator width.
     */
    private final float mIndicatorItemLength = DP * 6;
    /**
     * Padding between indicators.
     */
    private final float mIndicatorItemPadding = DP * 6;

    private final float mAdditionalPadding = SELECTED_ITEM_WIDTH - BIG_DOT_RADIUS;

    /**
     * Some more natural animation interpolation
     */
    private final Interpolator mInterpolator = new AccelerateDecelerateInterpolator();

    private final Paint mPaint = new Paint();

    public SwiperIndicatorDecoration() {
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(mIndicatorStrokeWidth);
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setAntiAlias(true);
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        super.onDrawOver(c, parent, state);

        int itemCount = parent.getAdapter().getItemCount();

        // find active page (which should be highlighted)
        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();
        int activePosition = RecyclerView.NO_POSITION;
        if (layoutManager instanceof LinearLayoutManager) {
            activePosition = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
        } else if (parent instanceof DiscreteScrollView) {
            activePosition = ((DiscreteScrollView) parent).getCurrentItem();
        }
        if (activePosition == RecyclerView.NO_POSITION) {
            return;
        }

        // find offset of active page (if the user is scrolling)
        final View activeChild = layoutManager.findViewByPosition(activePosition);
        if (activeChild == null) {
            return;
        }
        int left = activeChild.getLeft();
        int width = activeChild.getWidth();

        // on swipe the active item will be positioned from [-width, 0]
        // interpolate offset for smooth animation
        float progress = mInterpolator.getInterpolation(left * -1 / (float) width);


        // center vertically in the allotted space
        float indicatorPosY = parent.getHeight() - mIndicatorHeight / 2F;

        // center horizontally, calculate width and subtract half from center
        float totalLength = mIndicatorItemLength * (itemCount - 1) + SELECTED_ITEM_WIDTH;
        float paddingBetweenItems = Math.max(0, itemCount - 1) * mIndicatorItemPadding;
        float indicatorTotalWidth = totalLength + paddingBetweenItems;
        float indicatorStartX = (parent.getWidth() - indicatorTotalWidth) / 2F;

        float startOfActive = drawInactiveIndicators(c, indicatorStartX, indicatorPosY, activePosition, itemCount);
        drawHighlights(c, startOfActive, indicatorPosY, activePosition, progress, itemCount);
    }

    private float drawInactiveIndicators(Canvas c, float indicatorStartX, float indicatorPosY, int activePosition, int itemCount) {
        // width of item indicator including padding
        final float itemWidth = mIndicatorItemLength + mIndicatorItemPadding;

        float startOfActive = 0f;
        float start = indicatorStartX;
        for (int i = 0; i < itemCount; i++) {
            float radius = defineRadiusOfDot(activePosition, i);
            int color = defineColorOfDot(activePosition, i);
            mPaint.setColor(color);
            c.drawCircle(start + mIndicatorItemLength, indicatorPosY, radius, mPaint);
            if (i == activePosition) {
                startOfActive = start + mIndicatorItemPadding;
                start += SELECTED_ITEM_WIDTH + mIndicatorItemPadding;
            } else {
                start += itemWidth;
            }
        }
        return startOfActive;
    }

    private float defineRadiusOfDot(int activePosition, int currentPosition) {
        if (activePosition == currentPosition) return 0.0f;
        if (Math.abs(activePosition - currentPosition) <= 2) return BIG_DOT_RADIUS;
        return SMALL_DOT_RADIUS;
    }

    private int defineColorOfDot(int activePosition, int currentPosition) {
        if (Math.abs(activePosition - currentPosition) <= 1)
            return Color.WHITE;
        else
            return Color.GRAY;
    }

    private void drawHighlights(Canvas c, float highlightStart, float indicatorPosY,
                                int highlightPosition, float progress, int itemCount) {
        mPaint.setColor(Color.WHITE);

        // width of item indicator including padding
        final float itemWidth = mIndicatorItemLength + mIndicatorItemPadding;

//        float highlightStart = indicatorStartX + itemWidth * highlightPosition;
        c.drawRoundRect(new RectF(highlightStart - mIndicatorItemPadding, indicatorPosY - BIG_DOT_RADIUS, highlightStart + SELECTED_ITEM_WIDTH, indicatorPosY + BIG_DOT_RADIUS), CORNER_RADIUS, CORNER_RADIUS, mPaint);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        outRect.bottom = mIndicatorHeight;
    }
}
