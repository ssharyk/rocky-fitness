package ru.qbitmobile.rockyfit.helper.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import jp.wasabeef.glide.transformations.BlurTransformation;
import ru.qbitmobile.rockyfit.R;

/**
 * Утилита для отображения картинок при использовании Glide
 */
public class GlideHelper {

    static class GlideOptionsFactory extends BaseImageOptionsFactory<Transformation<Bitmap>> {
        @Override
        public Transformation<Bitmap> transform(@NotNull ImageOption option) {
            if (option instanceof BlurImageOption) {
                return new BlurTransformation(((BlurImageOption) option).getBlurRadius());
            }
            return null;
        }
    }

    private static RequestOptions sRequestOptions;
    private static List<Transformation<Bitmap>> sTransformations = new ArrayList<>();
    private static GlideOptionsFactory sOptionsFactory = new GlideOptionsFactory();

    static {
        sTransformations.add(new CenterCrop());
        sTransformations.add(new RoundedCorners(16));

        sRequestOptions = new RequestOptions();
        sRequestOptions = sRequestOptions.transform(
                new MultiTransformation<>(sTransformations));
        sRequestOptions = sRequestOptions
                .diskCacheStrategy(DiskCacheStrategy.ALL);
    }

    private static RequestOptions getOptions(Transformation<Bitmap>... additionalTransforms) {
        return getOptions(Arrays.asList(additionalTransforms));
    }

    private static RequestOptions getOptions(List<Transformation<Bitmap>> additionalTransforms) {
        List<Transformation<Bitmap>> updatedTransforms = new ArrayList<>(sTransformations);
        updatedTransforms.addAll(additionalTransforms);
        return sRequestOptions.clone()
                .transform(new MultiTransformation<>(updatedTransforms));
    }

    static void render(Context context, ImageView imageView, String uri, int width, int height, RequestOptions options) {
        try {
            Glide.with(context).clear(imageView);
        } catch (Throwable t) {
        }

        try {
            RequestBuilder<Drawable> requestBuilder = Glide.with(context)
                    .load(uri)
                    .apply(options)
                    .placeholder(R.drawable.screen_placeholder);
            if (width > 0 && height > 0) {
                requestBuilder = requestBuilder.override(width, height);
            }
            requestBuilder
                    .into(imageView);
        } catch (Throwable t) {
        }
    }

    static RequestOptions createOptions(List<ImageOption> options) {
        // преобразовать данные
        List<Transformation<Bitmap>> addedTransformations = sOptionsFactory.transform(options);

        // создать опции
        RequestOptions requestOptions = getOptions(addedTransformations);
        return requestOptions;
    }
}
