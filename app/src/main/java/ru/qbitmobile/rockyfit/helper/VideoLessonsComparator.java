package ru.qbitmobile.rockyfit.helper;

import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;

import java.util.Comparator;

public class VideoLessonsComparator implements Comparator<VideoLesson> {

    @Override
    public int compare(VideoLesson lhs, VideoLesson rhs) {
        if (lhs == null) {
            if (rhs == null) return 0;
            return 1;
        } else {
            if (rhs == null) return -1;
        }

        int byId = signum(lhs.getTrainingId() - rhs.getTrainingId());
        if (byId == 0) {
            return signum(lhs.getStep() - rhs.getStep());
        }
        return byId;
    }

    private int signum(long x) {
        if (x == 0) return 0;
        if (x > 0) return 1;
        return -1;
    }
}
