package ru.qbitmobile.rockyfit.activity.lessondetails;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import ru.qbitmobile.rockyfit.R;
import ru.qbitmobile.rockyfit.adapter.PagerAdapterCourseDetails;
import ru.qbitmobile.rockyfit.basemvp.BaseAppCompatActivity;
import ru.qbitmobile.rockyfit.contentmanager.enumeration.LessonType;
import ru.qbitmobile.rockyfit.contentmanager.model.Lesson;
import ru.qbitmobile.rockyfit.fragment.description.DescriptionFragment;
import ru.qbitmobile.rockyfit.fragment.header.LessonHeaderFragment;
import ru.qbitmobile.rockyfit.fragment.schedule.ScheduleFragment;
import ru.qbitmobile.rockyfit.view.ScrollingViewPager;
import com.google.android.material.tabs.TabLayout;

public class LessonDetailsActivity extends BaseAppCompatActivity implements LessonDetailsContract.ILessonDetailsView {

    private LessonDetailsContract.ILessonDetailsPresenter mPresenter;
    private @LessonType long mLessonType;
    private long mLessonId;

    private View mContainer;
    private View mNoResultsContainer;

    private TabLayout mTabs;
    private ScrollingViewPager mFragmentsPager;
    private PagerAdapterCourseDetails mPagerAdapter;
    private LessonHeaderFragment mHeader;

    private static final String EXTRA_LESSON_TYPE = "ru.qbitmobile.rockyfit.activity.lessondetails.LESSON_TYPE";
    private static final String EXTRA_LESSON_ID = "ru.qbitmobile.rockyfit.activity.lessondetails.LESSON_ID";

    public static Intent createIntent(Context context, @LessonType long lessonType, long lessonId) {
        Intent intent = new Intent(context, LessonDetailsActivity.class);
        intent.putExtra(EXTRA_LESSON_TYPE, lessonType);
        intent.putExtra(EXTRA_LESSON_ID, lessonId);
        return intent;
    }

    protected LessonDetailsContract.ILessonDetailsPresenter createPresenter() {
        if (mLessonType == LessonType.MASTER_CLASS) {
            return new CourseDetailsPresenter(this, mLessonId);
        } else if (mLessonType == LessonType.TRAINING) {
            return new TrainingDetailsPresenter(this, mLessonId);
        } else {
            return null;
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_details);

        mLessonType = getIntent().getLongExtra(EXTRA_LESSON_TYPE, LessonType.TRAINING);
        mLessonId = getIntent().getLongExtra(EXTRA_LESSON_ID, 0L);

        findViews();
        initTabs();

        showLoading();
        mPresenter = createPresenter();
        mPresenter.loadLesson();
    }

    private void findViews() {
        mContainer = findViewById(R.id.course_details_container);
        mNoResultsContainer = findViewById(R.id.course_details_no_result);
        mFragmentsPager = findViewById(R.id.course_details_pager);
        mTabs = findViewById(R.id.course_details_tabs);
    }

    private void initTabs() {
        mTabs.setupWithViewPager(mFragmentsPager);
        mTabs.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mFragmentsPager) {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);

                int newPosition = tab.getPosition();

                // пересчет высоты, так как ScrollingViewPager ипользует WRAP_CONTENT
                mFragmentsPager.setPage(newPosition);
                mFragmentsPager.requestLayout();
                mFragmentsPager.invalidate();
            }
        });
    }

    @Override
    public void onBackPressed() {
        finishThis();
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finishThis();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void finishThis() {
        this.finish();
    }

    @Override
    public void showLoading() {
        mNoResultsContainer.setVisibility(View.VISIBLE);
        mContainer.setVisibility(View.GONE);
    }

    @Override
    public void showLessonInfo(Lesson lesson) {
        mNoResultsContainer.setVisibility(View.GONE);
        mContainer.setVisibility(View.VISIBLE);

        mHeader = LessonHeaderFragment.newInstance(mLessonType, lesson);
        inflateFragment(mHeader, R.id.course_details_header_fragment_container);

        mPagerAdapter = new PagerAdapterCourseDetails(getSupportFragmentManager());
        mPagerAdapter.addFragment(DescriptionFragment.newInstance(mLessonType, lesson), getString(R.string.description));
        mPagerAdapter.addFragment(ScheduleFragment.newInstance(mLessonType, lesson.getId()), getString(R.string.schedule));
        mFragmentsPager.setAdapter(mPagerAdapter);
    }
}