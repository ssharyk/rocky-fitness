package ru.qbitmobile.rockyfit.activity.videolesson;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import ru.qbitmobile.rockyfit.R;
import ru.qbitmobile.rockyfit.basemvp.BaseAppCompatActivity;
import ru.qbitmobile.rockyfit.contentmanager.model.MediaFile;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;
import ru.qbitmobile.rockyfit.helper.images.BlurImageOption;
import ru.qbitmobile.rockyfit.helper.images.ImageHelper;
import ru.qbitmobile.rockyfit.helper.images.ImageOption;
import ru.qbitmobile.rockyfit.helper.images.SizeImageOption;
import ru.qbitmobile.rockyfit.likescontroller.ILikeViewHolder;
import ru.qbitmobile.rockyfit.likescontroller.LikeController;
import ru.qbitmobile.rockyfit.likescontroller.VideoLessonItemClickListener;
import ru.qbitmobile.rockyfit.videocontroller.ExoplayerController;
import ru.qbitmobile.rockyfit.videocontroller.IVideoController;
import ru.qbitmobile.rockyfit.view.LinearProgress;

import com.google.android.exoplayer2.ui.PlayerView;


public class VideoLessonView extends BaseAppCompatActivity
        implements VideoLessonContract.IVideoLessonView, ILikeViewHolder {

    private static final String EXTRA_VIDEO_LESSON_ID = "ru.qbitmobile.rockyfit.activity.videolesson.VIDEO_LESSON_ID";

    public static Intent createIntent(Context context, long lessonId) {
        Intent intent = new Intent(context, VideoLessonView.class);
        intent.putExtra(EXTRA_VIDEO_LESSON_ID, lessonId);
        return intent;
    }

    private long mLessonId;

    private ScrollView mScroller;
    private View mContent;
    private View mContainer;
    private View mNoResultsContainer;
    private View mToolbar;
    private TextView mStepNumber;
    private TextView mDuration;
    private LinearProgress mTotalProgress;
    private View mLikesContainer;
    private TextView mLikesCount;
    private ImageView mLikesIcon;
    private TextView mLoadingMessage;
    private TextView mName;
    private TextView mDescription;
    private ImageView mPreview;
    private PlayerView mVideoView;
    private ProgressBar mProgressBar;
    private ImageView mLockIcon;
    private View mButtonsContainer;
    private Button mNextLessonButton;
    private Button mPreviousLessonButton;

    private VideoLessonContract.IVideoLessonPresenter mPresenter;
    private IVideoController mVideoController;
    private int mBottomPadding = 0;

    protected VideoLessonContract.IVideoLessonPresenter createPresenter() {
        return new VideoLessonPresenter(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_lesson);

        resolveIntent();
        findViews();
        initToolbar();

//        mVideoController = new VideoController(this, mPreview, mVideoView);
        mVideoController = new ExoplayerController(this, mPreview, mVideoView, mProgressBar, new ExoplayerController.Callbacks() {

            private int mVideoWidth = 0;
            private int mVideoHeight = 0;

            @Override
            public void beforeFullscreen() {
                mVideoWidth = mVideoView.getWidth();
                mVideoHeight = mVideoView.getHeight();
            }

            @Override
            public void onEnterFullscreen() {
                // скрыть все, кроме видео
                setViewsVisibility(View.GONE);
                setVideoContentBottomPadding(0);

                mVideoView.getLayoutParams().width = getResources().getDisplayMetrics().heightPixels;
                mVideoView.getLayoutParams().height = getResources().getDisplayMetrics().widthPixels;
            }

            @Override
            public void onExitFullscreen() {
                // показать все, что было ранее скрыто
                setViewsVisibility(View.VISIBLE);
                mPreview.setVisibility(View.GONE);

                setVideoContentBottomPadding(mBottomPadding);

                int width = mVideoWidth;
                int height = mVideoHeight;
                mVideoView.getLayoutParams().width = width;
                mVideoView.getLayoutParams().height = height;
                setSize(width, height);
            }
        });

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        mPresenter = createPresenter();
        mPresenter.load(mLessonId);
    }

    @Override
    protected void onStop() {
        if (mVideoController != null) {
            mVideoController.onStop();
        }
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        if (mPresenter != null) {
            mPresenter.onDestroy();
        }
        super.onDestroy();
    }

    private void resolveIntent() {
        mLessonId = getIntent().getLongExtra(EXTRA_VIDEO_LESSON_ID, -1L);
    }

    private void findViews() {
        this.mScroller = findViewById(R.id.video_lesson_scrolling_container);
        this.mContent = findViewById(R.id.video_lesson_scrolling_content);
        this.mContainer = findViewById(R.id.video_lesson_container);
        this.mNoResultsContainer = findViewById(R.id.video_lesson_no_result);
        this.mLoadingMessage = findViewById(R.id.video_lesson_loading_message);

        this.mToolbar = findViewById(R.id.video_lesson_toolbar_container);
        this.mStepNumber = findViewById(R.id.video_lesson_toolbar_step_number);
        this.mDuration = findViewById(R.id.video_lesson_toolbar_duration);
        this.mTotalProgress = findViewById(R.id.video_lesson_toolbar_progress);
        this.mName = findViewById(R.id.video_lesson_video_name);
        this.mDescription = findViewById(R.id.video_lesson_description);
        this.mLikesContainer = findViewById(R.id.video_lesson_like_container);
        this.mLikesCount = findViewById(R.id.list_item_likes_counter);
        this.mLikesIcon = findViewById(R.id.list_item_like_icon);
        this.mPreview = findViewById(R.id.video_lesson_preview);
        this.mVideoView = findViewById(R.id.video_lesson_video);
        this.mProgressBar = findViewById(R.id.video_lesson_loading_progress);
        this.mLockIcon = findViewById(R.id.video_lesson_block);

        this.mButtonsContainer = findViewById(R.id.video_lesson_buttons_container);
        this.mNextLessonButton = findViewById(R.id.video_lesson_button_next);
        this.mPreviousLessonButton = findViewById(R.id.video_lesson_button_previous);
    }

    private void setViewsVisibility(int visibility) {
        mToolbar.setVisibility(visibility);
        mPreview.setVisibility(visibility);
        mName.setVisibility(visibility);
        mDescription.setVisibility(visibility);
        mLikesContainer.setVisibility(visibility);
        mButtonsContainer.setVisibility(visibility);
    }

    private void setVideoContentBottomPadding(int paddingBottom) {
        mContent.setPadding(
                mContent.getPaddingLeft(), mContent.getPaddingTop(),
                mContent.getPaddingRight(), paddingBottom
        );
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.video_lesson_toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayShowTitleEnabled(false);
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.icon_back);
        }
    }

    @Override
    public void onDataLoaded() {
        mScroller.smoothScrollTo(0, 0);
    }

    @Override
    public void showSingleLesson() {
        this.mNextLessonButton.setVisibility(View.GONE);
        this.mNextLessonButton.setOnClickListener(null);

        this.mPreviousLessonButton.setVisibility(View.GONE);
        this.mPreviousLessonButton.setOnClickListener(null);
    }

    @Override
    public void showNextLessonButton() {
        this.mNextLessonButton.setVisibility(View.VISIBLE);
        this.mNextLessonButton.setOnClickListener(v -> mPresenter.onNextLessonClick());
    }

    @Override
    public void hideNextLessonButton() {
        this.mNextLessonButton.setVisibility(View.GONE);
        this.mNextLessonButton.setOnClickListener(null);
    }

    @Override
    public void showPreviousButton() {
        this.mPreviousLessonButton.setVisibility(View.VISIBLE);
        this.mPreviousLessonButton.setOnClickListener(v -> mPresenter.onPreviousLessonClick());
    }

    @Override
    public void hidePreviousButton() {
        this.mPreviousLessonButton.setVisibility(View.GONE);
        this.mPreviousLessonButton.setOnClickListener(null);
    }

    @Override
    public void setBottomPadding(int buttonsVisible) {
        int padding = getResources().getDimensionPixelSize(R.dimen.command_buttons_container_height);
        switch (buttonsVisible) {
            case 0:
                padding = getResources().getDimensionPixelSize(R.dimen.text_view_small_padding);
                break;

            case 1:
                int buttonHeight = getResources().getDimensionPixelSize(R.dimen.button_height);
                int buttonMargin = getResources().getDimensionPixelSize(R.dimen.button_next_video_vertical_margin);
                padding = padding - (buttonHeight + buttonMargin);
                break;

            case 2:
                padding = getResources().getDimensionPixelSize(R.dimen.command_buttons_container_height);
                break;
        }

        mBottomPadding = padding;
        mContent.setPadding(
                mContent.getPaddingLeft(), mContent.getPaddingTop(),
                mContent.getPaddingRight(), padding
        );
    }

    @Override
    public void setLessonMetadata(int stepNumber, String formattedDuration, int progress) {
        this.mStepNumber.setText(getResources().getString(R.string.step) + " " + stepNumber);
        this.mDuration.setText(formattedDuration);
        this.mTotalProgress.setProgressLevel(progress);
        mTotalProgress.invalidate();
    }

    @Override
    public void setLessonContent(String name, String description) {
        this.mName.setText(name);
        this.mDescription.setText(description);
    }

    @Override
    public void setLessonLikes(VideoLesson lesson) {
        LikeController.bindLikeIcon(this, lesson);
        this.mLikesContainer.setBackground(null);
        this.mLikesCount.setText(String.valueOf(lesson.getLikes()));
        this.mLikesContainer.setOnClickListener(v -> {
            LikeController.onClick(VideoLessonView.this, lesson);
        });
    }

    @Override
    public void setLessonPreview(MediaFile preview, MediaFile video) {
        mVideoController.showPreview(preview, video);
        mLockIcon.setVisibility(View.VISIBLE);
        mPreview.setVisibility(View.VISIBLE);
        ImageHelper.render(this, mPreview, preview.getUri(),
                new SizeImageOption(video.getWidth(), video.getHeight()),
                new BlurImageOption(ImageOption.DEFAULT_BLUR));
    }

    @Override
    public void setLessonVideo(MediaFile preview, MediaFile video) {
        mLockIcon.setVisibility(View.GONE);
        mVideoController.start(preview, video);
    }

    @Override
    public void setClickListener(Runnable action) {
        if (action == null) {
            mPreview.setOnClickListener(null);
        } else {
            mPreview.setOnClickListener(v -> action.run());
        }
    }

    @Override
    public void setSize(int width, int height) {
        ConstraintLayout mLayout = findViewById(R.id.constraints);
        ConstraintSet set = new ConstraintSet();
        set.clone(mLayout);
        String ratio = width + ":" + height;
        set.setDimensionRatio(mPreview.getId(), ratio);
        set.setDimensionRatio(mVideoView.getId(), ratio);
        set.applyTo(mLayout);
    }

    @Override
    public void showLoading() {
        mContainer.setVisibility(View.GONE);
        mToolbar.setVisibility(View.GONE);
        mNoResultsContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        mNoResultsContainer.setVisibility(View.GONE);
        mContainer.setVisibility(View.VISIBLE);
        mToolbar.setVisibility(View.VISIBLE);
        mLoadingMessage.setText(getResources().getString(R.string.loading_in_process));
    }

    @Override
    public void showError() {
        mContainer.setVisibility(View.GONE);
        mToolbar.setVisibility(View.GONE);
        mNoResultsContainer.setVisibility(View.VISIBLE);
        mNextLessonButton.setOnClickListener(null);
        mLoadingMessage.setText(getResources().getString(R.string.error_please_retry));
    }

    @Override
    public void launchVideo(long newId) {
        mVideoController.prepareForNext();
        mPresenter.reset(newId);
    }

    @Override
    public void onBackPressed() {
        if (this.mVideoController != null && mVideoController.isFullScreen()) {
            mVideoController.exitFullScreen();
        } else {
            finishThis();
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finishThis();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void finishThis() {
        this.finish();
    }

    @Override
    public VideoLessonItemClickListener getVideosLessonClickListener() {
        return new VideoLessonItemClickListener() {
            @Override
            public void onItemClick(int position, VideoLesson item) {
            }
        };
    }

    @Override
    public TextView getLikesCount() {
        return mLikesCount;
    }

    @Override
    public ImageView getLikesIcon() {
        return mLikesIcon;
    }
}
