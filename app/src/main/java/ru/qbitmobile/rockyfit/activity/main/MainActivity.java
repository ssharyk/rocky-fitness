package ru.qbitmobile.rockyfit.activity.main;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.beloo.widget.chipslayoutmanager.SpacingItemDecoration;
import ru.qbitmobile.rockyfit.R;
import ru.qbitmobile.rockyfit.activity.lessondetails.LessonDetailsActivity;
import ru.qbitmobile.rockyfit.activity.videolesson.VideoLessonView;
import ru.qbitmobile.rockyfit.adapter.AdapterMainScreen;
import ru.qbitmobile.rockyfit.basemvp.BaseAppCompatActivity;
import ru.qbitmobile.rockyfit.contentmanager.enumeration.LessonType;
import ru.qbitmobile.rockyfit.contentmanager.model.Tag;

import java.util.List;

public class MainActivity extends BaseAppCompatActivity implements MainContract.IMainView {

    private RecyclerView mContent;

    private MainContract.IMainPresenter mPresenter;

    protected MainContract.IMainPresenter createPresenter() {
        return new MainPresenter(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViews();

        mPresenter = createPresenter();
        mPresenter.init();
    }

    private void findViews() {
        mContent = findViewById(R.id.main_screen_all);
    }

    @Override
    public void showContent(AdapterMainScreen adapter) {
        mContent.setLayoutManager(new LinearLayoutManager(this));
        mContent.setAdapter(adapter);
        int verticalSpacing = getResources().getDimensionPixelSize(R.dimen.card_vertical_space) / 3;
        mContent.addItemDecoration(new SpacingItemDecoration(0, verticalSpacing));
    }

    @Override
    public void launchCourse(long courseId) {
        startActivity(LessonDetailsActivity.createIntent(this, LessonType.MASTER_CLASS, courseId));
    }

    @Override
    public void launchTraining(long trainingId) {
        startActivity(LessonDetailsActivity.createIntent(this, LessonType.TRAINING, trainingId));
    }

    @Override
    public void launchVideo(long videoId) {
        startActivity(VideoLessonView.createIntent(this, videoId));
    }
}
