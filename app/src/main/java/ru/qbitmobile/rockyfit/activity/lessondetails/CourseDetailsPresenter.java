package ru.qbitmobile.rockyfit.activity.lessondetails;

import ru.qbitmobile.rockyfit.basemvp.BasePresenter;
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.model.Training;
import ru.qbitmobile.rockyfit.contentmanager.repository.ITrainingRepository;

public class CourseDetailsPresenter extends BasePresenter implements LessonDetailsContract.ILessonDetailsPresenter {
    private LessonDetailsContract.ILessonDetailsView mView;
    private long mTrainingId;

    public CourseDetailsPresenter(LessonDetailsContract.ILessonDetailsView view, long courseId) {
        this.mView = view;
        this.mTrainingId = courseId;
    }

    @Override
    public void loadLesson() {
        Training training = DependencyGraph.get(ITrainingRepository.class).getTrainingById(mTrainingId);
        if (training == null) {
            mView.runOnUiThread(() -> {
                mView.showLoading();
            });
        } else {
            mView.runOnUiThread(() -> {
                mView.showLessonInfo(training);
            });
        }
    }
}
