package ru.qbitmobile.rockyfit.activity.main;

import ru.qbitmobile.rockyfit.adapter.AdapterMainScreen;
import ru.qbitmobile.rockyfit.basemvp.IBasePresenter;
import ru.qbitmobile.rockyfit.basemvp.IBaseView;


public interface MainContract {
    interface IMainView extends IBaseView {

        void showContent(AdapterMainScreen adapter);

        void launchCourse(long courseId);
        void launchTraining(long trainingId);
        void launchVideo(long videoId);
    }

    interface IMainPresenter extends IBasePresenter {
        void init();
    }
}
