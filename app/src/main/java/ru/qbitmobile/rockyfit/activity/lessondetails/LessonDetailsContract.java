package ru.qbitmobile.rockyfit.activity.lessondetails;

import ru.qbitmobile.rockyfit.basemvp.IBasePresenter;
import ru.qbitmobile.rockyfit.basemvp.IBaseView;
import ru.qbitmobile.rockyfit.contentmanager.model.Lesson;

public interface LessonDetailsContract {

    interface ILessonDetailsView extends IBaseView {
        void showLoading();

        void showLessonInfo(Lesson lesson);
    }

    interface ILessonDetailsPresenter extends IBasePresenter {
        void loadLesson();
    }
}
