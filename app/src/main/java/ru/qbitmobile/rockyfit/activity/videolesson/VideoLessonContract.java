package ru.qbitmobile.rockyfit.activity.videolesson;

import ru.qbitmobile.rockyfit.basemvp.IBasePresenter;
import ru.qbitmobile.rockyfit.basemvp.IBaseView;
import ru.qbitmobile.rockyfit.contentmanager.model.MediaFile;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;

public interface VideoLessonContract {

    interface IVideoLessonView extends IBaseView {
        void showLoading();
        void hideLoading();

        void showError();

        void onDataLoaded();

        void showSingleLesson();
        void showNextLessonButton();
        void hideNextLessonButton();
        void showPreviousButton();
        void hidePreviousButton();

        void setBottomPadding(int buttonsVisible);

        void setLessonMetadata(int stepNumber, String formattedDuration, int progress);
        void setLessonContent(String name, String description);
        void setLessonLikes(VideoLesson lesson);

        void setLessonPreview(MediaFile preview, MediaFile video);
        void setLessonVideo(MediaFile preview, MediaFile video);
        void setSize(int width, int height);

        void setClickListener(Runnable action);
        void launchVideo(long newId);
    }

    interface IVideoLessonPresenter extends IBasePresenter {
        void load(long lessonId);

        void reset(long newLessonId);

        void onNextLessonClick();

        void onPreviousLessonClick();
    }
}
