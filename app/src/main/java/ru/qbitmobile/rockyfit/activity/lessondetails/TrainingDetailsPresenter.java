package ru.qbitmobile.rockyfit.activity.lessondetails;

import ru.qbitmobile.rockyfit.basemvp.BasePresenter;
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.model.Training;
import ru.qbitmobile.rockyfit.contentmanager.repository.ITrainingRepository;

public class TrainingDetailsPresenter extends BasePresenter implements LessonDetailsContract.ILessonDetailsPresenter {
    private LessonDetailsContract.ILessonDetailsView mView;
    private long mTrainingId;

    public TrainingDetailsPresenter(LessonDetailsContract.ILessonDetailsView view, long trainingId) {
        this.mView = view;
        this.mTrainingId = trainingId;
    }

    @Override
    public void loadLesson() {
        Training training = DependencyGraph.get(ITrainingRepository.class).getTrainingById(mTrainingId);
        if (training == null) {
            mView.runOnUiThread(() -> {
                mView.showLoading();
            });
        } else {
            mView.runOnUiThread(() -> {
                mView.showLessonInfo(training);
            });
        }
    }
}
