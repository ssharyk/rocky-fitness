package ru.qbitmobile.rockyfit.activity.main;

import androidx.lifecycle.LiveData;

import ru.qbitmobile.rockyfit.adapter.AdapterMainScreen;
import ru.qbitmobile.rockyfit.adapter.AdapterTags;
import ru.qbitmobile.rockyfit.adapter.ItemClickListener;
import ru.qbitmobile.rockyfit.basemvp.BaseAppCompatActivity;
import ru.qbitmobile.rockyfit.contentmanager.module.configurator.ConfigurationService;
import ru.qbitmobile.rockyfit.contentmanager.module.subscription.SubscriptionService;
import ru.qbitmobile.rockyfit.contentmanager.updateobserver.VideoLessonUpdateObserver;
import ru.qbitmobile.rockyfit.likescontroller.VideoLessonItemClickListener;
import ru.qbitmobile.rockyfit.basemvp.BasePresenter;
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.enumeration.LessonType;
import ru.qbitmobile.rockyfit.contentmanager.model.Lesson;
import ru.qbitmobile.rockyfit.contentmanager.model.Tag;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;
import ru.qbitmobile.rockyfit.contentmanager.module.db.DatabaseService;
import ru.qbitmobile.rockyfit.contentmanager.repository.IVideoLessonsRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.livedata.RepositoryData;
import ru.qbitmobile.rockyfit.helper.VideoLessonsComparator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainPresenter extends BasePresenter implements MainContract.IMainPresenter, AdapterMainScreen.Owner {

    private MainContract.IMainView mView;

    private List<Tag> mSelectedTags = new ArrayList<>();
    private AdapterMainScreen mGlobalAdapter;
    private List<VideoLesson> mVideoLessons;

    private AdapterTags.ItemListener mTagItemListener = new AdapterTags.ItemListener() {
        @Override
        public boolean canUserSelectTag(int position) {
            return true;
        }

        @Override
        public void onTagSelected(int adapterPosition, Tag tag) {
            mSelectedTags.add(tag);
            loadTaggedVideos();
        }

        @Override
        public void onTagUnselected(int adapterPosition, Tag tag) {
            mSelectedTags.remove(tag);
            if (mSelectedTags.size() > 0) {
                loadTaggedVideos();
            } else {
                if (mView != null) {
                    loadAllCachedVideos();
                }
            }
        }
    };

    private VideoLessonItemClickListener mVideoLessonItemListener = new VideoLessonItemClickListener() {
        @Override
        public void onItemClick(int position, VideoLesson item) {
            if (item.isUnblocked()) {
                mView.launchVideo(item.getId());
            } else {
                DependencyGraph.get(SubscriptionService.class).subscriptionShowDialog(() -> {
                    if (mView != null) {
                        mView.runOnUiThread(() -> {
                            mGlobalAdapter.notifyDataSetChanged();
                            mView.launchVideo(item.getId());
                        });
                    }
                });
            }
        }
    };

    public MainPresenter(MainContract.IMainView view) {
        mView = view;
    }

    @Override
    public void init() {
        loadAllVideos();

        mGlobalAdapter = new AdapterMainScreen(this, DependencyGraph.get(DatabaseService.class).getFreeVideoLessons());
        mGlobalAdapter.setTrainingClickListener(new ItemClickListener<Lesson>() {
            @Override
            public void onItemClick(int position, Lesson item) {
                if (item.getCatalogId() == LessonType.TRAINING) {
                    mView.launchTraining(item.getId());
                } else {
                    mView.launchCourse(item.getId());
                }
            }
        });
        mGlobalAdapter.setVideoLessonClickListener(mVideoLessonItemListener);
        mGlobalAdapter.setTagsListener(mTagItemListener);

        mView.showContent(mGlobalAdapter);
    }

    private void loadTaggedVideos() {
        List<VideoLesson> videoLessons = DependencyGraph.get(IVideoLessonsRepository.class).getVideoLessons(mSelectedTags, false);
        notifyVideosList(videoLessons);
    }

    /**
     * Обновляет список данных для видео
     */
    private void loadAllVideos() {
        LiveData<RepositoryData<List<VideoLesson>>> videoLessonsLiveData = DependencyGraph.get(IVideoLessonsRepository.class).getVideoLessons(false);
        videoLessonsLiveData.observe(mView.getLifecycleOwner(), videoLessons -> {
            if (videoLessons == null || videoLessons.getData() == null || videoLessons.getData().size() == 0) {
                return;
            }
            mView.runOnUiThread(() -> {
                List<VideoLesson> videos = new ArrayList<>(videoLessons.getData());
                Collections.sort(videos, new VideoLessonsComparator());
                notifyVideosList(videos);
            });
        });
    }

    /**
     * Подготавливает данные видео для отображения в общем списке
     */
    private void loadAllCachedVideos() {
        List<VideoLesson> videoLessons = DependencyGraph.get(DatabaseService.class).getAllVideoLessons();
        if (videoLessons == null || videoLessons.size() == 0) {
            return;
        }
        mView.runOnUiThread(() -> {
            notifyVideosList(videoLessons);
        });
    }

    private void notifyVideosList(List<VideoLesson> videoLessons) {
        mVideoLessons = videoLessons;
        if (mGlobalAdapter != null) {
            videoLessons = DependencyGraph.get(ConfigurationService.class).shuffle(videoLessons);
            mGlobalAdapter.setVideos(videoLessons);
            mGlobalAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public BaseAppCompatActivity getContext() {
        return mView.getMvpActivity();
    }

    @Override
    public void subscribeToChanges(VideoLessonUpdateObserver observer) {
        if (mVideoLessons == null) return;
        for (VideoLesson lesson : mVideoLessons) {
            if (lesson.getId() == observer.getVideoLessonId()) {
                lesson.addObserver(observer);
            }
        }
    }
}
