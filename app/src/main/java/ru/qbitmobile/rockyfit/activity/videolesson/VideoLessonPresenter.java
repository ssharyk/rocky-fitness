package ru.qbitmobile.rockyfit.activity.videolesson;

import org.jetbrains.annotations.NotNull;

import ru.qbitmobile.rockyfit.R;
import ru.qbitmobile.rockyfit.basemvp.BasePresenter;
import ru.qbitmobile.rockyfit.commoncomponent.helper.Time;
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.model.MediaFile;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;
import ru.qbitmobile.rockyfit.contentmanager.module.subscription.SubscriptionService;
import ru.qbitmobile.rockyfit.contentmanager.repository.IVideoLessonsRepository;
import ru.qbitmobile.rockyfit.contentmanager.updateobserver.VideoLessonUpdateObserver;
import ru.qbitmobile.rockyfit.commoncomponent.helper.Constant;

import java.util.List;

public class VideoLessonPresenter extends BasePresenter
        implements VideoLessonContract.IVideoLessonPresenter, VideoLessonUpdateObserver {

    private VideoLessonContract.IVideoLessonView mView;
    private VideoLesson mLesson;
    private List<VideoLesson> mLessonsOfTraining;

    public VideoLessonPresenter(VideoLessonContract.IVideoLessonView view) {
        this.mView = view;
    }

    @Override
    public void load(long lessonId) {
        mView.showLoading();

        // отмена подписки на изменения в видео, которое уже не показывается
        if (mLesson != null) {
            mLesson.removeObserver(this);
        }

        mLesson = DependencyGraph.get(IVideoLessonsRepository.class).getVideoLessonById(lessonId);
        if (mLesson == null) {
            mView.showError();
            return;
        }

        // подписка на изменения в видео, которое должно быть отображено
        mLesson.addObserver(this);

        // все уроки в серии. должны быть отсортированы по возрастанию номера (на уровне БД)
        this.mLessonsOfTraining = DependencyGraph.get(IVideoLessonsRepository.class).getVideoLessonsByTrainingId(mLesson.getTrainingId());

        Time duration = new Time(mLesson.getDuration() * 1000);
        mView.runOnUiThread(() -> {
            mView.onDataLoaded();

            // определить количество кнопок, которые должны быть показаны
            int totalButtonsCount = 0;
            int totalLessonsCount = mLesson.getTraining().getLessonsCount();
            if (totalLessonsCount == 1) {
                mView.showSingleLesson();
            } else {
                // проверить, если это первый урок в серии
                if (mLesson.getStep() == 1) {
                    mView.hidePreviousButton();
                } else {
                    totalButtonsCount++;
                    mView.showPreviousButton();
                }

                // проверить, если это последний урок в серии
                if (mLesson.getStep() == totalLessonsCount) {
                    mView.hideNextLessonButton();
                } else {
                    totalButtonsCount++;
                    mView.showNextLessonButton();
                }
            }
            // в зависимости от количества кнопок, установить поля для текста
            mView.setBottomPadding(totalButtonsCount);
            mView.setSize(mLesson.getWidth(), mLesson.getHeight());

            String header, content;
            MediaFile video = new MediaFile(mLesson.getId(), mLesson.getWidth(), mLesson.getHeight(), mLesson.getVideoUri(), mLesson.getName());
            if (mLesson.isUnblocked()) {
                header = mLesson.getName();
                content = mLesson.getDescription();
                mView.setLessonVideo(mLesson.getPreviewImage(), video);
                mView.setClickListener(null);
            } else {
                header = mView.getString(R.string.warning_subscription_required_header);
                content = mView.getString(R.string.warning_subscription_required);
                mView.setLessonPreview(mLesson.getPreviewImage(), video);
                mView.setClickListener(() -> {
                    DependencyGraph.get(SubscriptionService.class).subscriptionShowDialog(() -> {
                        mView.setLessonContent(mLesson.getName(), mLesson.getDescription());
                        mView.setLessonVideo(mLesson.getPreviewImage(), video);
                        mView.setClickListener(null);
                    });
                });
            }
            mView.setLessonMetadata(mLesson.getStep(), duration.toString(), mLesson.getProgress());
            mView.setLessonContent(header, content);
            mView.setLessonLikes(mLesson);

            mView.hideLoading();
        });
    }

    @Override
    public void reset(long newLessonId) {
        // не требуется сохранять состояние, просто загружаем новое видео
        this.load(newLessonId);
    }

    @Override
    public void onNextLessonClick() {
        if (mLesson == null) {
            return;
        }

        // определить
        if (mLessonsOfTraining == null) {
            return;
        }

        for (int i = 0; i < mLessonsOfTraining.size() - 1; i++) {
            if (mLessonsOfTraining.get(i).getId() == mLesson.getId()) {
                long newId = mLessonsOfTraining.get(i + 1).getId();
                mView.launchVideo(newId);
                break;
            }
        }
    }

    @Override
    public void onPreviousLessonClick() {
        if (mLesson == null) {
            return;
        }

        if (mLessonsOfTraining == null) {
            return;
        }

        for (int i = 1; i < mLessonsOfTraining.size(); i++) {
            if (mLessonsOfTraining.get(i).getId() == mLesson.getId()) {
                // определить ID следущего урока в серии
                long newId = mLessonsOfTraining.get(i - 1).getId();
                mView.launchVideo(newId);
                break;
            }
        }
    }

    @Override
    public long getVideoLessonId() {
        if (mLesson == null) return 0;
        return mLesson.getId();
    }

    @Override
    public void updateLike(@NotNull VideoLesson videoLesson) {
        if (mLesson == null) return;
        if (videoLesson.getId() == mLesson.getId()) {
            mView.setLessonLikes(videoLesson);
        }
    }

    @Override
    public int getObserverType() {
        return Constant.ObserverTypes.VIDEO_LESSON_VIEW;
    }


    @Override
    public void onDestroy() {
        if (mLesson != null) {
            mLesson.removeObserver(this);
        }

        super.onDestroy();
    }
}
