package ru.qbitmobile.rockyfit.adapter;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import ru.qbitmobile.rockyfit.contentmanager.repository.livedata.DataSource;
import ru.qbitmobile.rockyfit.contentmanager.repository.livedata.RepositoryData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Управляет индикаторами загрузки в общем списке
 */
class LoadingIndicatorController {

    private Map<Integer, MutableLiveData<Boolean>> mShouldShowLoaderLiveData = new HashMap<>();

    LoadingIndicatorController() {
        for (int position = 1; position <= 3; position++) {
            mShouldShowLoaderLiveData.put(position, new MutableLiveData<>());
        }
    }

    LiveData<Boolean> get(int position) {
        return mShouldShowLoaderLiveData.get(position);
    }

    private MutableLiveData<Boolean> getInternal(int position) {
        return mShouldShowLoaderLiveData.get(position);
    }

    <T> void onDataLoaded(int adapterPosition, RepositoryData<List<T>> data) {

        if (data.getData() == null) {
            // нет данных - показываем загрузку
            postValue(adapterPosition, false);
        }

        if (data.getData().size() == 0L) {
            if (data.getDataSource() == DataSource.NETWORK || data.getDataSource() == DataSource.CACHE) {
                // нет данных на сервере (после первого и последующих запросов) - показываем загрузку
                postValue(adapterPosition, false);
            } else {
                if (adapterPosition == 3) {
                    // видео - показывать индикатор, если нет данных
                    boolean shouldShow = getInternal(1).getValue() == null || !getInternal(1).getValue();
                    postValue(adapterPosition, shouldShow);
                } else {
                    // есть данные - загрузка завершена
                    postValue(adapterPosition, true);
                }
            }
        } else {
            postValue(adapterPosition, false);
        }

    }

    /**
     * Послать наблюдателям новое значение
     * @param adapterPosition   Указатель на LiveData, для которой должно быть установлено значение
     * @param val               Была ли закончена загрузка?
     */
    private void postValue(int adapterPosition, boolean val) {
        if (getInternal(adapterPosition) != null)
            getInternal(adapterPosition).setValue(val);
    }
}
