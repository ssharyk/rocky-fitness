package ru.qbitmobile.rockyfit.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import ru.qbitmobile.rockyfit.R;
import ru.qbitmobile.rockyfit.contentmanager.updateobserver.VideoLessonUpdateObserver;
import ru.qbitmobile.rockyfit.helper.images.BlurImageOption;
import ru.qbitmobile.rockyfit.helper.images.ImageHelper;
import ru.qbitmobile.rockyfit.helper.images.ImageOption;
import ru.qbitmobile.rockyfit.likescontroller.ILikeViewHolder;
import ru.qbitmobile.rockyfit.likescontroller.LikeController;
import ru.qbitmobile.rockyfit.likescontroller.VideoLessonItemClickListener;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;

import java.util.ArrayList;
import java.util.List;

/**
 * Для вывода списка видео в графике занятий
 */
public class AdapterVideoCard extends RecyclerView.Adapter<AdapterVideoCard.TrainingCardViewHolder> {

    public interface Owner {
        Context getContext();
        void subscribeToChanges(VideoLessonUpdateObserver observer);
        int getObserverType();
    }

    private Owner mOwner;
    private Context mContext;
    private LayoutInflater mInflater;
    private List<VideoLesson> mVideoLessons;
    private VideoLessonItemClickListener mItemClickListener;

    public AdapterVideoCard(Owner owner, List<VideoLesson> videoLessons) {
        this.mOwner = owner;
        this.mContext = owner.getContext();
        this.mInflater = LayoutInflater.from(mContext);
        this.mVideoLessons = new ArrayList<>(videoLessons);
    }

    @NonNull
    @Override
    public TrainingCardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_training_card, parent, false);
        return new TrainingCardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TrainingCardViewHolder holder, int position) {
        holder.bind(mVideoLessons.get(position));
        mOwner.subscribeToChanges(holder);
    }

    @Override
    public int getItemCount() {
        return mVideoLessons.size();
    }

    @Override
    public long getItemId(int position) {
        return mVideoLessons.get(position).getId();
    }

    public void setItemClickListener(VideoLessonItemClickListener clickListener) {
        this.mItemClickListener = clickListener;
    }


    class TrainingCardViewHolder extends RecyclerView.ViewHolder implements ILikeViewHolder, VideoLessonUpdateObserver {

        private VideoLesson mObservingLesson;
        private ImageView mImage;
        private ImageView mLockPlaceholder;
        private TextView mName;
        private TextView mCoachName;
        private TextView mDescription;
        private RecyclerView mTagsList;
        private View mLikeContainer;
        private ImageView mLikesIcon;
        private TextView mLikesCount;

        public TrainingCardViewHolder(@NonNull View itemView) {
            super(itemView);
            mImage = itemView.findViewById(R.id.list_item_training_card_image);
            mLockPlaceholder = itemView.findViewById(R.id.list_item_training_card_block);
            mName = itemView.findViewById(R.id.list_item_training_card_name);
            mCoachName = itemView.findViewById(R.id.list_item_training_card_coach_name);
            mDescription = itemView.findViewById(R.id.list_item_training_card_description);
            mLikeContainer = itemView.findViewById(R.id.list_item_training_card_likes_container);
            mLikesCount = itemView.findViewById(R.id.list_item_likes_counter);
            mLikesIcon = itemView.findViewById(R.id.list_item_like_icon);

            mTagsList = itemView.findViewById(R.id.list_item_training_card_tags_list);
            mTagsList.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));
            mTagsList.setAdapter(new AdapterTags(mContext, new ArrayList<>(), new AdapterTags.UnselectableItemsListener()));

            itemView.setOnClickListener(v -> {
                if (mItemClickListener != null) {
                    int position = getAdapterPosition();
                    if (position == -1) return;
                    mItemClickListener.onItemClick(position, mVideoLessons.get(position));
                }
            });

            mLikeContainer.setOnClickListener(v -> {
                VideoLesson lesson = mVideoLessons.get(getAdapterPosition());
                LikeController.onClick(AdapterVideoCard.TrainingCardViewHolder.this, lesson);
            });
        }

        void bind(VideoLesson lesson) {
            mObservingLesson = lesson;

            mName.setText(lesson.getName());
            if (lesson.getCoach() != null) {
                mCoachName.setText(lesson.getCoach().getFullName());
            }
            mDescription.setText(lesson.getDescription());

            List<ImageOption> previewDisplayOptions = new ArrayList<>();
            if (!lesson.isUnblocked()) {
                previewDisplayOptions.add(new BlurImageOption(ImageOption.DEFAULT_BLUR));
                mLockPlaceholder.setVisibility(View.VISIBLE);
            } else {
                mLockPlaceholder.setVisibility(View.GONE);
            }

            ImageHelper.render(mContext, mImage, lesson.getPreviewImageUri(), previewDisplayOptions);

            RecyclerView.Adapter tagsAdapter = mTagsList.getAdapter();
            if (tagsAdapter != null && tagsAdapter instanceof AdapterTags) {
                ((AdapterTags) tagsAdapter).setItems(lesson.getTags());
                tagsAdapter.notifyDataSetChanged();
            }

            LikeController.bindLikeIcon(this, lesson);
        }

        @Override
        public VideoLessonItemClickListener getVideosLessonClickListener() {
            return mItemClickListener;
        }

        @Override
        public TextView getLikesCount() {
            return mLikesCount;
        }

        @Override
        public ImageView getLikesIcon() {
            return mLikesIcon;
        }

        @Override
        public long getVideoLessonId() {
            return mObservingLesson == null ? 0 : mObservingLesson.getId();
        }

        @Override
        public void updateLike(@NotNull VideoLesson videoLesson) {
            if (mObservingLesson == null || mObservingLesson.getId() != videoLesson.getId()) {
                return;
            }
            LikeController.bindLikeIcon(this, videoLesson);
        }

        @Override
        public int getObserverType() {
            return mOwner.getObserverType();
        }
    }
}
