package ru.qbitmobile.rockyfit.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

class SimpleFragmentPagerAdapter<T extends Fragment> extends FragmentPagerAdapter {
    protected List<T> mFragments;
    protected List<String> mTitles;

    public SimpleFragmentPagerAdapter(FragmentManager fm) {
        super(fm, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);

        this.mFragments = new ArrayList<>();
        this.mTitles = new ArrayList<>();
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles.get(position);
    }

    public void addFragment(T fragment, String title) {
        mFragments.add(fragment);
        mTitles.add(title);
    }

    public void update(int position, T fragment) {
        mFragments.set(position, fragment);
    }
}