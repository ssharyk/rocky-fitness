package ru.qbitmobile.rockyfit.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import ru.qbitmobile.rockyfit.R;
import ru.qbitmobile.rockyfit.contentmanager.updateobserver.VideoLessonUpdateObserver;
import ru.qbitmobile.rockyfit.helper.images.BlurImageOption;
import ru.qbitmobile.rockyfit.helper.images.ImageHelper;
import ru.qbitmobile.rockyfit.helper.images.ImageOption;
import ru.qbitmobile.rockyfit.likescontroller.ILikeViewHolder;
import ru.qbitmobile.rockyfit.likescontroller.LikeController;
import ru.qbitmobile.rockyfit.likescontroller.VideoLessonItemClickListener;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;

import java.util.ArrayList;
import java.util.List;

/**
 * Список превью уроков на странице с описанием тренировки
 */
public class AdapterImages extends RecyclerView.Adapter<AdapterImages.HolderImage> {

    public interface Owner {
        Context getContext();
        void subscribeToChanges(VideoLessonUpdateObserver observer);
        int getObserverType();
    }

    private Owner mOwner;
    private Context mContext;
    private LayoutInflater mInflater;
    private List<VideoLesson> mFiles;
    private VideoLessonItemClickListener mItemClickListener;

    public AdapterImages(Owner owner, List<VideoLesson> files) {
        this.mOwner = owner;
        this.mContext = owner.getContext();
        this.mInflater = LayoutInflater.from(mContext);
        this.mFiles = new ArrayList<>(files);
    }

    @NonNull
    @Override
    public HolderImage onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_media, parent, false);
        return new HolderImage(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HolderImage holder, int position) {
        holder.bind(mFiles.get(position));
        mOwner.subscribeToChanges(holder);
    }

    @Override
    public int getItemCount() {
        return mFiles.size();
    }

    @Override
    public long getItemId(int position) {
        return mFiles.get(position).getId();
    }

    public void setItemClickListener(VideoLessonItemClickListener clickListener) {
        this.mItemClickListener = clickListener;
    }

    class HolderImage extends RecyclerView.ViewHolder implements ILikeViewHolder, VideoLessonUpdateObserver {

        private VideoLesson mObservingLesson;
        private ImageView mImage;
        private ImageView mLockPlaceholder;
        private TextView mName;
        private View mLikeContainer;
        private ImageView mLikesIcon;
        private TextView mLikesCount;

        public HolderImage(@NonNull View itemView) {
            super(itemView);
            mImage = itemView.findViewById(R.id.list_item_media_image);
            mLockPlaceholder = itemView.findViewById(R.id.list_item_media_block);
            mName = itemView.findViewById(R.id.list_item_media_name);
            mLikeContainer = itemView.findViewById(R.id.list_item_media_like_container);
            mLikesCount = itemView.findViewById(R.id.list_item_likes_counter);
            mLikesIcon = itemView.findViewById(R.id.list_item_like_icon);

            itemView.setOnClickListener(v -> {
                if (mItemClickListener != null) {
                    int position = getAdapterPosition();
                    if (position == -1) return;
                    mItemClickListener.onItemClick(position, mFiles.get(position));
                }
            });

            mLikeContainer.setOnClickListener(v -> {
                VideoLesson lesson = mFiles.get(getAdapterPosition());
                LikeController.onClick(AdapterImages.HolderImage.this, lesson);
            });
        }

        void bind(VideoLesson lesson) {
            mObservingLesson = lesson;

            mName.setText(lesson.getName());
            mLikesCount.setText(String.valueOf(lesson.getLikes()));

            List<ImageOption> previewDisplayOptions = new ArrayList<>();
            if (!lesson.isUnblocked()) {
                previewDisplayOptions.add(new BlurImageOption(ImageOption.DEFAULT_BLUR));
                mLockPlaceholder.setVisibility(View.VISIBLE);
            } else {
                mLockPlaceholder.setVisibility(View.GONE);
            }

            ImageHelper.render(mContext, mImage, lesson.getPreviewImageUri(), previewDisplayOptions);

            LikeController.bindLikeIcon(this, lesson);
        }

        @Override
        public VideoLessonItemClickListener getVideosLessonClickListener() {
            return mItemClickListener;
        }

        @Override
        public TextView getLikesCount() {
            return mLikesCount;
        }

        @Override
        public ImageView getLikesIcon() {
            return mLikesIcon;
        }

        @Override
        public long getVideoLessonId() {
            return mObservingLesson == null ? 0 : mObservingLesson.getId();
        }

        @Override
        public void updateLike(@NotNull VideoLesson videoLesson) {
            if (mObservingLesson == null || mObservingLesson.getId() != videoLesson.getId()) {
                return;
            }
            LikeController.bindLikeIcon(this, videoLesson);
        }

        @Override
        public int getObserverType() {
            return mOwner.getObserverType();
        }
    }
}
