package ru.qbitmobile.rockyfit.adapter;

public interface ItemClickListener<T> {
    void onItemClick(int position, T item);
}
