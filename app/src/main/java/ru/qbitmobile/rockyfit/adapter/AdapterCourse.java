package ru.qbitmobile.rockyfit.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ru.qbitmobile.rockyfit.R;
import ru.qbitmobile.rockyfit.contentmanager.model.Lesson;
import ru.qbitmobile.rockyfit.contentmanager.model.Training;
import ru.qbitmobile.rockyfit.helper.images.ImageHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Отображает список тренировок/мастер-классов
 */
public class AdapterCourse extends RecyclerView.Adapter<AdapterCourse.LessonViewHolder> {

    private Context mContext;
    private LayoutInflater mInflater;
    private ItemClickListener<Lesson> mItemClickListener;
    private final List<Lesson> mLessons;

    public AdapterCourse(Context context, List<? extends Lesson> lessons) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mLessons = new ArrayList<>(lessons);
    }

    @NonNull
    @Override
    public LessonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_course, parent, false);
        return new LessonViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LessonViewHolder holder, int position) {
        holder.bind(mLessons.get(position));
    }

    @Override
    public int getItemCount() {
        return mLessons.size();
    }

    public void setItemClickListener(ItemClickListener<Lesson> clickListener) {
        this.mItemClickListener = clickListener;
    }

    public void setItems(List<Training> trainings) {
        synchronized (mLessons) {
            this.mLessons.clear();
            mLessons.addAll(trainings);
        }
    }

    class LessonViewHolder extends RecyclerView.ViewHolder {

        private ImageView mPreview;
        private TextView mLessonsCounter;
        private TextView mName;
        private TextView mCoachName;
        private TextView mDescription;
        private View mLikesContainer;
        private ImageView mLikeIcon;
        private TextView mLikesCount;

        public LessonViewHolder(@NonNull View itemView) {
            super(itemView);

            mPreview = itemView.findViewById(R.id.list_item_course_preview);
            mLessonsCounter = itemView.findViewById(R.id.list_item_course_lessons_counter);
            mName = itemView.findViewById(R.id.list_item_course_course_name);
            mCoachName = itemView.findViewById(R.id.list_item_course_coach_name);
            mDescription = itemView.findViewById(R.id.list_item_course_description);
            mLikesContainer = itemView.findViewById(R.id.list_item_course_like_container);
            mLikesCount = itemView.findViewById(R.id.list_item_likes_counter);
            mLikeIcon = itemView.findViewById(R.id.list_item_like_icon);

            mLikesContainer.setBackground(null);

            itemView.setOnClickListener(v -> {
                if (mItemClickListener != null) {
                    int position = getAdapterPosition();
                    if (position == -1) return;
                    mItemClickListener.onItemClick(position, mLessons.get(position));
                }
            });
        }

        void bind(Lesson lesson) {
            ImageHelper.render(mContext, mPreview, lesson.getPreviewImageUri());

            mLessonsCounter.setText(mContext.getResources().getString(R.string.lessons) + ": " + lesson.getLessonsCount());

            if (lesson.getCoach() != null) {
                mCoachName.setText(lesson.getCoach().getFullName());
            }

            mName.setText(lesson.getName());
            mDescription.setText(lesson.getDescription());

            mLikesCount.setText(String.valueOf(lesson.getLikes()));
        }
    }
}
