package ru.qbitmobile.rockyfit.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import ru.qbitmobile.rockyfit.R;
import ru.qbitmobile.rockyfit.contentmanager.model.Tag;

import java.util.ArrayList;
import java.util.List;

/**
 * Отображает набор тегов
 */
public class AdapterTags extends RecyclerView.Adapter<AdapterTags.TagViewHolder> {

    public interface ItemListener {
        boolean canUserSelectTag(int position);

        void onTagSelected(int adapterPosition, Tag tag);
        void onTagUnselected(int adapterPosition, Tag tag);
    }

    public static class UnselectableItemsListener implements ItemListener {
        @Override
        public boolean canUserSelectTag(int position) {
            return false;
        }

        @Override
        public void onTagSelected(int adapterPosition, Tag tag) {
        }

        @Override
        public void onTagUnselected(int adapterPosition, Tag tag) {
        }
    }

    private LayoutInflater mInflater;
    private List<Tag> mTags;
    private ItemListener mItemListener;

    public AdapterTags(Context context, List<Tag> tags, ItemListener listener) {
        this.mInflater = LayoutInflater.from(context);
        this.mTags = new ArrayList<>(tags);
        this.mItemListener = listener;
    }

    @NonNull
    @Override
    public TagViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_tag_chip, parent, false);
        return new TagViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TagViewHolder holder, int position) {
        holder.bind(mTags.get(position));
    }

    @Override
    public int getItemCount() {
        return mTags.size();
    }

    @Override
    public long getItemId(int position) {
        return mTags.get(position).getName().hashCode();
    }

    public void setItems(List<Tag> newTags) {
        this.mTags.clear();
        this.mTags.addAll(newTags);
    }

    class TagViewHolder extends RecyclerView.ViewHolder {

        private TextView mTagName;
        private ImageView mSelectionIcon;

        public TagViewHolder(@NonNull View itemView) {
            super(itemView);

            mTagName = itemView.findViewById(R.id.tag_chip_text);
            mSelectionIcon = itemView.findViewById(R.id.tag_chip_icon_remove);
        }

        void bind(Tag tag) {
            bindViewsState(tag);

            boolean canSelect = mItemListener != null && mItemListener.canUserSelectTag(getAdapterPosition());
            if (canSelect) {
                itemView.setOnClickListener(v -> {
                    int adapterPosition = getAdapterPosition();
                    if (tag.isSelected()) {
                        tag.setSelected(false);
                        mItemListener.onTagUnselected(adapterPosition, mTags.get(adapterPosition));
                    } else {
                        tag.setSelected(true);
                        mItemListener.onTagSelected(adapterPosition, mTags.get(adapterPosition));
                    }
                    bindViewsState(tag);
                });
            } else {
                itemView.setOnClickListener(null);
            }
        }

        private void bindViewsState(Tag tag) {
            mTagName.setText(tag.getName());

            boolean canSelect = mItemListener != null && mItemListener.canUserSelectTag(getAdapterPosition());
            if (canSelect && tag.isSelected()) {
                itemView.setBackgroundResource(R.drawable.tag_chip_selected);
                mSelectionIcon.setVisibility(View.VISIBLE);
            } else {
                itemView.setBackgroundResource(R.drawable.tag_chip);
                mSelectionIcon.setVisibility(View.GONE);
            }
        }
    }
}
