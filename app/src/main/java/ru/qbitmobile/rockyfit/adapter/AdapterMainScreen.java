package ru.qbitmobile.rockyfit.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.beloo.widget.chipslayoutmanager.ChipsLayoutManager;
import com.beloo.widget.chipslayoutmanager.SpacingItemDecoration;

import ru.qbitmobile.rockyfit.R;
import ru.qbitmobile.rockyfit.contentmanager.updateobserver.VideoLessonUpdateObserver;
import ru.qbitmobile.rockyfit.commoncomponent.helper.Constant;
import ru.qbitmobile.rockyfit.helper.images.BlurImageOption;
import ru.qbitmobile.rockyfit.helper.images.ImageHelper;
import ru.qbitmobile.rockyfit.helper.images.ImageOption;
import ru.qbitmobile.rockyfit.likescontroller.ILikeViewHolder;
import ru.qbitmobile.rockyfit.likescontroller.LikeController;
import ru.qbitmobile.rockyfit.likescontroller.VideoLessonItemClickListener;
import ru.qbitmobile.rockyfit.basemvp.BaseAppCompatActivity;
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.enumeration.LessonType;
import ru.qbitmobile.rockyfit.contentmanager.model.Lesson;
import ru.qbitmobile.rockyfit.contentmanager.model.Tag;
import ru.qbitmobile.rockyfit.contentmanager.model.Training;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;
import ru.qbitmobile.rockyfit.contentmanager.repository.ITagRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.ITrainingRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.livedata.RepositoryData;
import ru.qbitmobile.rockyfit.helper.SwiperIndicatorDecoration;

import com.yarolegovich.discretescrollview.DiscreteScrollView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Представляет главный экран как общий список.
 * Первые четыре элемента - фиксированные: тулбар, список тренировок, список мастер-классов, список всех тегов видео
 * Остальные элементы - отдельные видео
 */
public class AdapterMainScreen extends RecyclerView.Adapter<AdapterMainScreen.MainScreenViewHolder> {

    public interface Owner {
        BaseAppCompatActivity getContext();
        void subscribeToChanges(VideoLessonUpdateObserver observer);
    }

    // region Fields and properties

    private Owner mOwner;
    private BaseAppCompatActivity mContext;
    private LayoutInflater mInflater;
    private LoadingIndicatorController mIndicatorController = new LoadingIndicatorController();
    private List<VideoLesson> mVideos;

    private ItemClickListener<Lesson> mTrainingClickListener;
    private VideoLessonItemClickListener mVideoLessonClickListener;
    private AdapterTags.ItemListener mTagsListener;

    private static final int TYPE_TOOLBAR = 1;
    private static final int TYPE_TRAININGS = 2;
    private static final int TYPE_TAGS = 3;
    private static final int TYPE_VIDEO = 4;

    private static final int MAX_TAGS = 4;

    // endregion

    // region Main methods

    public AdapterMainScreen(Owner owner, List<VideoLesson> videos) {
        this.mOwner = owner;
        this.mContext = owner.getContext();
        this.mInflater = LayoutInflater.from(mContext);
        this.mVideos = new ArrayList<>(videos);
    }

    @NonNull
    @Override
    public MainScreenViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_TOOLBAR:
                View toolbarSubView = mInflater.inflate(R.layout.item_mainscreen_toolbar, parent, false);
                return new HolderToolbar(toolbarSubView);

            case TYPE_TRAININGS:
                View trainingSubView = mInflater.inflate(R.layout.item_mainscreen_trainingslist, parent, false);
                return new HolderTrainingsList(trainingSubView);

            case TYPE_TAGS:
                View tagsSubView = mInflater.inflate(R.layout.item_mainscreen_tagslist, parent, false);
                return new HolderTagsList(tagsSubView);

            default:
                View videoView = mInflater.inflate(R.layout.item_tagged_video, parent, false);
                return new HolderVideo(videoView);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull MainScreenViewHolder holder, int position) {
        holder.bind();
        if (holder instanceof VideoLessonUpdateObserver) {
            mOwner.subscribeToChanges((VideoLessonUpdateObserver) holder);
        }
    }

    @Override
    public int getItemCount() {
        return mVideos.size() + 4;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return TYPE_TOOLBAR;
            case 1:
            case 2:
                return TYPE_TRAININGS;
            case 3:
                return TYPE_TAGS;
            default:
                return TYPE_VIDEO;
        }
    }

    @Override
    public long getItemId(int position) {
        if (position <= 2) {
            return -position;
        }

        return mVideos.get(getResolvedVideoPosition(position)).getId();
    }

    public void setVideos(List<VideoLesson> videos) {
        this.mVideos.clear();
        this.mVideos.addAll(videos);
    }

    /**
     * Определяет по позиции VH индекс элемента в списке видео, от которового следует брать данные для связывания
     */
    private int getResolvedVideoPosition(int position) {
        if (position <= 3) {
            return position;
        }
        return position - 4;
    }

    // endregion

    // region Listeners

    public void setTrainingClickListener(ItemClickListener<Lesson> mTrainingClickListener) {
        this.mTrainingClickListener = mTrainingClickListener;
    }

    public void setVideoLessonClickListener(VideoLessonItemClickListener mVideoLessonClickListener) {
        this.mVideoLessonClickListener = mVideoLessonClickListener;
    }

    public void setTagsListener(AdapterTags.ItemListener mTagsListener) {
        this.mTagsListener = mTagsListener;
    }

    // endregion


    // region VHs

    abstract class MainScreenViewHolder extends RecyclerView.ViewHolder {

        public MainScreenViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        abstract void bind();
    }

    /**
     * Тулбар. Просто тулбар
     */
    class HolderToolbar extends MainScreenViewHolder {

        public HolderToolbar(@NonNull View itemView) {
            super(itemView);
        }

        @Override
        void bind() {
            // do nothinh
        }
    }

    /**
     * Список тренировок и мастер-классов
     */
    class HolderTrainingsList extends MainScreenViewHolder {

        private View mLoader;
        private DiscreteScrollView mList;
        private TextView mTrainingsListHeader;
        private TextView mTrainingsListSubheader;


        public HolderTrainingsList(@NonNull View itemView) {
            super(itemView);

            setIsRecyclable(false);

            mLoader = itemView.findViewById(R.id.main_screen_trainings_loading);
            mList = itemView.findViewById(R.id.main_screen_trainings_list);
            mTrainingsListHeader = itemView.findViewById(R.id.main_screen_trainings_header);
            mTrainingsListSubheader = itemView.findViewById(R.id.main_screen_trainings_subheader);
        }

        @Override
        public void bind() {

            // показ/сокрытие индикатора загрузки и контента путем подписки на изменения в контроллере индикаторов
            mIndicatorController.get(getAdapterPosition()).observe(mContext.getLifecycleOwner(), shouldShowLoader -> {
                if (shouldShowLoader) {
                    hideContent();
                } else {
                    showContent();
                }
            });

            // тип категории
            long catalogId = resolveCatalogId();
            if (catalogId == LessonType.TRAINING) {
                mTrainingsListHeader.setText(R.string.trainings);
                mTrainingsListSubheader.setText(R.string.training_subheader);
            } else {
                mTrainingsListHeader.setText(R.string.courses_with_best_coaches);
                mTrainingsListSubheader.setText(R.string.courses_subheader);
            }

            // подписка на получение данных
            LiveData<RepositoryData<List<Training>>> trainingsLiveData = DependencyGraph.get(ITrainingRepository.class).getTrainings(catalogId);
            trainingsLiveData.observe(mContext, trainings -> {
                mContext.runOnUiThread(() -> {
                    mIndicatorController.onDataLoaded(getAdapterPosition(), trainings);

                    if (trainings == null || trainings.getData() == null || trainings.getData().size() == 0) {
                        return;
                    }

                    long contentCatalog = resolveCatalogId();
                    if (trainings.getData().get(0).getCatalogId() != contentCatalog) {
                        // LiveData получила данные для неверного каталога (т.к. bind)
                        return;
                    }

                    if (mList.getAdapter() == null || (!(mList.getAdapter() instanceof AdapterCourse))) {
                        // получено первый раз - сконфигурировать RV с нуля
                        AdapterCourse trainingAdapter = new AdapterCourse(mContext, trainings.getData());
                        trainingAdapter.setItemClickListener(mTrainingClickListener);
                        mList.setAdapter(trainingAdapter);
                        mList.addItemDecoration(new SwiperIndicatorDecoration());
                    } else {
                        // обновить данные в уже определенном RV
                        if (mList.getAdapter() != null && mList.getAdapter() instanceof AdapterCourse) {
                            ((AdapterCourse) mList.getAdapter()).setItems(trainings.getData());
                            mContext.runOnUiThread(() -> {
                                mList.getAdapter().notifyDataSetChanged();
                            });
                        }
                    }
                });
            });
        }

        void showContent() {
            mContext.runOnUiThread(() -> {
                mList.setVisibility(View.VISIBLE);
                mLoader.setVisibility(View.GONE);
            });
        }

        void hideContent() {
            mContext.runOnUiThread(() -> {
                mList.setVisibility(View.GONE);
                mLoader.setVisibility(View.VISIBLE);
            });
        }

        private long resolveCatalogId() {
            if (getAdapterPosition() == 1) {
                return LessonType.TRAINING;
            }
            return LessonType.MASTER_CLASS;
        }
    }

    /**
     * Список всех тегов всех видео
     */
    class HolderTagsList extends MainScreenViewHolder {

        private View mLoader;
        private RecyclerView mList;

        public HolderTagsList(@NonNull View itemView) {
            super(itemView);

            mLoader = itemView.findViewById(R.id.main_screen_tags_loading);
            mList = itemView.findViewById(R.id.main_screen_tags_list);
        }

        @Override
        public void bind() {
            // показ/сокрытие индикатора загрузки и контента путем подписки на изменения в контроллере индикаторов
            mIndicatorController.get(getAdapterPosition()).observe(mContext.getLifecycleOwner(), shouldShowLoader -> {
                if (shouldShowLoader) {
                    hideContent();
                } else {
                    showContent();
                }
            });

            // подписка на получение данных
            LiveData<RepositoryData<List<Tag>>> tagsLiveData = DependencyGraph.get(ITagRepository.class).getTags();
            tagsLiveData.observe(mContext, tags -> {
                mIndicatorController.onDataLoaded(getAdapterPosition(), tags);

                boolean isNull = tags == null || tags.getData() == null;
                if (isNull) {
                    mContext.runOnUiThread(() -> {
                        mList.setVisibility(View.GONE);
                        mLoader.setVisibility(View.GONE);
                    });
                    return;
                }

                mContext.runOnUiThread(() -> {
                    StaggeredGridLayoutManager gridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.HORIZONTAL);
                    mList.setLayoutManager(gridLayoutManager);
                    mList.setAdapter(new AdapterTags(mContext, tags.getData(), mTagsListener));
                });
            });
        }

        void showContent() {
            mContext.runOnUiThread(() -> {
                mList.setVisibility(View.VISIBLE);
                mLoader.setVisibility(View.GONE);
            });
        }

        void hideContent() {
            mContext.runOnUiThread(() -> {
                mList.setVisibility(View.GONE);
                mLoader.setVisibility(View.VISIBLE);
            });
        }
    }

    /**
     * Каждое видео
     */
    class HolderVideo extends MainScreenViewHolder implements ILikeViewHolder, VideoLessonUpdateObserver {

        private VideoLesson mObservingLesson;
        private ImageView mImage;
        private TextView mName;
        private TextView mCoachName;
        private RecyclerView mTagsList;
        private ImageView mLockPlaceholder;
        private ImageView mLikeIcon;
        private TextView mLikesCount;
        private View mLikeContainer;

        public HolderVideo(@NonNull View itemView) {
            super(itemView);
            mImage = itemView.findViewById(R.id.list_item_tagged_video_image);
            mLockPlaceholder = itemView.findViewById(R.id.list_item_tagged_video_block);
            mName = itemView.findViewById(R.id.list_item_tagged_video_name);
            mCoachName = itemView.findViewById(R.id.list_item_tagged_video_coach_name);

            mLikeContainer = itemView.findViewById(R.id.list_item_tagged_video_likes_counter_container);
            mLikeIcon = itemView.findViewById(R.id.list_item_like_icon);
            mLikesCount = itemView.findViewById(R.id.list_item_likes_counter);

            mTagsList = itemView.findViewById(R.id.list_item_tagged_video_tags_list);
            ChipsLayoutManager chipsLayoutManager = ChipsLayoutManager
                    .newBuilder(mContext)
                    .build();
            mTagsList.setLayoutManager(chipsLayoutManager);
            int verticalSpacing = mContext.getResources().getDimensionPixelSize(R.dimen.card_vertical_space) / 4;
            mTagsList.addItemDecoration(new SpacingItemDecoration(0, verticalSpacing));
            mTagsList.setAdapter(new AdapterTags(mContext, new ArrayList<>(), new AdapterTags.UnselectableItemsListener()));

            itemView.setOnClickListener(v -> {
                if (mVideoLessonClickListener != null) {
                    int position = getResolvedVideoPosition(getAdapterPosition());
                    if (position < 0) return;
                    mVideoLessonClickListener.onItemClick(position, mVideos.get(position));
                }
            });

            mLikeContainer.setOnClickListener(v -> {
                int position = getResolvedVideoPosition(getAdapterPosition());
                VideoLesson lesson = mVideos.get(position);

                LikeController.onClick(HolderVideo.this, lesson);
            });
        }

        @Override
        void bind() {
            this.bind(mVideos.get(getResolvedVideoPosition(getAdapterPosition())));
        }

        private void bind(VideoLesson lesson) {
            mObservingLesson = lesson;

            mName.setText(lesson.getName());

            if (lesson.getCoach() != null) {
                mCoachName.setText(lesson.getCoach().getFullName());
            }

            // превью
            List<ImageOption> previewDisplayOptions = new ArrayList<>();
            if (!lesson.isUnblocked()) {
                previewDisplayOptions.add(new BlurImageOption(ImageOption.DEFAULT_BLUR));
                mLockPlaceholder.setVisibility(View.VISIBLE);
            } else {
                mLockPlaceholder.setVisibility(View.GONE);
            }

            ImageHelper.render(mContext, mImage, lesson.getPreviewImageUri(), previewDisplayOptions);

            // набор тегов к видео
            RecyclerView.Adapter tagsAdapter = mTagsList.getAdapter();
            if (tagsAdapter != null && tagsAdapter instanceof AdapterTags) {
                List<Tag> selectedTags = new ArrayList<>(lesson.getTags());
                if (selectedTags.size() > MAX_TAGS)
                    selectedTags = selectedTags.subList(0, MAX_TAGS);
                ((AdapterTags) tagsAdapter).setItems(selectedTags);
                tagsAdapter.notifyDataSetChanged();
            }

            LikeController.bindLikeIcon(this, lesson);
        }

        @Override
        public VideoLessonItemClickListener getVideosLessonClickListener() {
            return mVideoLessonClickListener;
        }

        @Override
        public TextView getLikesCount() {
            return mLikesCount;
        }

        @Override
        public ImageView getLikesIcon() {
            return mLikeIcon;
        }

        @Override
        public long getVideoLessonId() {
            return mObservingLesson == null ? 0 : mObservingLesson.getId();
        }

        @Override
        public void updateLike(@NotNull VideoLesson videoLesson) {
            if (mObservingLesson == null || mObservingLesson.getId() != videoLesson.getId()) {
                return;
            }
            LikeController.bindLikeIcon(this, videoLesson);
        }

        @Override
        public int getObserverType() {
            return Constant.ObserverTypes.VIDEO_LESSON_MAIN_SCREEN;
        }
    }

    // endregion
}
