package ru.qbitmobile.rockyfit.videocontroller;

import android.graphics.Color;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;

import ru.qbitmobile.rockyfit.R;
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.model.MediaFile;
import ru.qbitmobile.rockyfit.contentmanager.module.contextholder.IContextHolder;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.Format;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.VideoFrameMetadataListener;
import com.google.android.exoplayer2.video.VideoListener;

/**
 * Контроллер видео, основанный на com.google.android.exoplayer2
 */
public class ExoplayerController extends BaseVideoController implements IVideoController {

    public interface Callbacks {
        void beforeFullscreen();

        void onEnterFullscreen();

        void onExitFullscreen();
    }

    private Callbacks mCallbacks;
    private PlayerView mPlayerView;

    private SimpleExoPlayer mPlayer;
    private DefaultHttpDataSourceFactory mMediaDataSourceFactory;

    private long mCurrentVideo;
    private VideoListener mVideoListener;
    private Player.EventListener mEventsListener;

    public ExoplayerController(AppCompatActivity context,
                               ImageView preview, PlayerView exoplayerView, ProgressBar progressBar,
                               Callbacks callbacks) {
        super(context, preview, progressBar);
        this.mPlayerView = exoplayerView;
        this.mCallbacks = callbacks;

        init();
    }

    // region Preparation

    @Override
    protected void init() {
        super.init();

        mPlayer = ExoPlayerFactory.newSimpleInstance(mActivity);
        String appName = mActivity.getResources().getString(R.string.app_name);
        mMediaDataSourceFactory = new DefaultHttpDataSourceFactory(Util.getUserAgent(mActivity, appName));

        mPlayer.setPlayWhenReady(true);

        mPlayerView.setShutterBackgroundColor(Color.TRANSPARENT);
        mPlayerView.setPlayer(mPlayer);
    }

    // endregion

    @Override
    public View getVideoView() {
        return mPlayerView;
    }

    @Override
    public void showPreview(MediaFile preview, MediaFile video) {
        mCurrentVideo = video.getId();
        super.showPreview(preview, video);
    }

    @Override
    public boolean start(MediaFile preview, MediaFile video) {
        if (mPlayerView == null) {
            return false;
        }

        mCurrentVideo = video.getId();
        String videoSource = video.getUri();

        try {
            // показать индикатор на время загрузки
            showPreloader(preview, video.getWidth(), video.getHeight());

            // подготовить плеер
            ProgressiveMediaSource mediaSource = new ProgressiveMediaSource.Factory(mMediaDataSourceFactory)
                    .createMediaSource(Uri.parse(videoSource));
            mPlayer.prepare(mediaSource, true, true);

            if (mVideoListener != null) {
                mPlayer.removeVideoListener(mVideoListener);
            }
            mVideoListener = new VideoListener() {
                @Override
                public void onRenderedFirstFrame() {
                    // готово к изображению - скрывает превью и показываем видео
                    hidePreview();
                }
            };
            mPlayer.addVideoListener(mVideoListener);

            if (mEventsListener != null) {
                mPlayer.removeListener(mEventsListener);
            }
            mEventsListener = new Player.EventListener() {
                @Override
                public void onPlayerError(ExoPlaybackException error) {
                    // ошибка при загрузке/воспроизведении видео
                    error.printStackTrace();

                    @StringRes int errorMessageCode = R.string.error_please_retry;
                    if (error.getCause() != null && error.getCause() instanceof HttpDataSource.HttpDataSourceException) {
                        HttpDataSource.HttpDataSourceException cause = (HttpDataSource.HttpDataSourceException) error.getCause();
                        if (cause.dataSpec.httpBody == null || cause.dataSpec.httpBody.length == 0) {
                            errorMessageCode = R.string.error_video_file_cannot_be_loaded;
                        }
                    } else if (error.getCause() != null && error.getCause() instanceof IllegalStateException) {
                        errorMessageCode = R.string.error_video_file_format;
                    }
                    int finalErrorMessageCode = errorMessageCode;

                    if (!DependencyGraph.get(IContextHolder.class).isValid(mActivity)) {
                        return;
                    }
                    mActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (video.getId() == mCurrentVideo) {
                                showPreview(preview, video);
                                Toast.makeText(mActivity, finalErrorMessageCode, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }

                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    if (!playWhenReady) {
                        return;
                    }

                    switch (playbackState) {
                        case Player.STATE_BUFFERING:
                        case Player.STATE_IDLE:
                        case Player.STATE_ENDED:
                            break;
                        case Player.STATE_READY:
                            hidePreview();
                            break;
                    }
                }
            };
            mPlayer.addListener(mEventsListener);

            mPlayer.setVideoFrameMetadataListener(new VideoFrameMetadataListener() {
                private int lastHeight = 0;
                private int lastWidth = 0;
                private boolean isSet = false;

                @Override
                public void onVideoFrameAboutToBeRendered(long presentationTimeUs, long releaseTimeNs, Format format) {

                    // по загруженному видео определить фактические размеры для отображения во фрейме
                    if (isSet && sameFormat(format)) {
                        return;
                    }

                    isSet = true;
                    lastHeight = format.height;
                    lastWidth = format.width;

                    // не требуется, так как теперь размеры доступны до загрузки видео
                    /*
                    mActivity.runOnUiThread(() -> {
                        mCallbacks.onLoaded(lastWidth, lastHeight);
                    });
                    */

                    System.out.println("VIDEO_VIEW format = " + format);
                }

                private boolean sameFormat(Format format) {
                    return format.height == lastHeight &&
                            format.width == lastWidth;
                }
            });
            mPlayerView.requestFocus();
            mPlayerView.setControllerShowTimeoutMs(3000);
            mPlayer.setPlayWhenReady(true);

            return true;
        } catch (Throwable t) {
            return false;
        }
    }

    @Override
    public void prepareForNext() {
        // позволить плееру самостоятельно обработать смену Uri
        if (mPlayer != null) {
            mPlayer.setPlayWhenReady(false);
        }
    }

    @Override
    public void onStop() {
        // полная остановка
        if (mPlayer != null) {
            mPlayer.setPlayWhenReady(false);

            mPlayer.setVideoListener(null);
        }
    }

    @Override
    public void enterFullScreen() {
        if (mCallbacks != null) {
            mCallbacks.beforeFullscreen();
        }
        super.enterFullScreen();
        if (mCallbacks != null) {
            mCallbacks.onEnterFullscreen();
        }
    }

    @Override
    public void exitFullScreen() {
        super.exitFullScreen();
        if (mCallbacks != null) {
            mCallbacks.onExitFullscreen();
        }
    }
}
