package ru.qbitmobile.rockyfit.videocontroller;

import android.view.View;

import ru.qbitmobile.rockyfit.contentmanager.model.MediaFile;

public interface IVideoController<T extends View> {
    void showPreview(MediaFile preview, MediaFile video);
    boolean start(MediaFile preview, MediaFile video);
    void prepareForNext();

    T getVideoView();

    void onStop();

    boolean isFullScreen();
    void enterFullScreen();
    void exitFullScreen();
}
