package ru.qbitmobile.rockyfit.videocontroller;

import android.content.pm.ActivityInfo;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

import androidx.annotation.CallSuper;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import ru.qbitmobile.rockyfit.R;
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.model.MediaFile;
import ru.qbitmobile.rockyfit.contentmanager.module.contextholder.IContextHolder;
import ru.qbitmobile.rockyfit.helper.images.ImageHelper;
import ru.qbitmobile.rockyfit.helper.images.ImageOption;
import ru.qbitmobile.rockyfit.helper.images.NoProgressOption;
import ru.qbitmobile.rockyfit.helper.images.SizeImageOption;

/**
 * Базовый класс, упрощающий взаимодействие экрана, превью и видео при загрузке видеоурока
 */
public abstract class BaseVideoController implements IVideoController {

    AppCompatActivity mActivity;
    private ImageView mPreview;
    private ProgressBar mProgressBar;

    private boolean mIsFullscreen;
    private View mEnterFullscreen;
    private View mExitFullscreen;

    /**
     * Создание нового объекта контроллера
     *
     * @param activity Окно
     * @param preview  ImageView для отображения превью
     */
    public BaseVideoController(AppCompatActivity activity, ImageView preview, ProgressBar progressBar) {
        this.mActivity = activity;
        this.mPreview = preview;
        this.mProgressBar = progressBar;
    }

    /**
     * Показывает индикатор загрузки на фоне превью вместо видео
     */
    protected void showPreloader(MediaFile preview, int width, int height) {
        if (getVideoView() != null) {
            getVideoView().setVisibility(View.VISIBLE);
        }

//        if (mPreview != null) {
//            mPreview.setVisibility(View.VISIBLE);
//            String uri = preview == null ? null : preview.getUri();
//            ImageHelper.render(mActivity, mPreview, uri, new SizeImageOption(width, height), NoProgressOption.INSTANCE);
//        }

        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Скрывает видео и Отображает превью
     *
     * @param preview Данные об изображение на сервере
     * @param video   Данные о видео на сервере
     */
    @Override
    public void showPreview(MediaFile preview, MediaFile video) {
        if (getVideoView() != null) {
            getVideoView().setVisibility(View.GONE);
        }

        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.GONE);
        }

        String previewSource = preview.getUri();
        if (mPreview != null && previewSource != null) {
            mPreview.setVisibility(View.VISIBLE);

            if (mActivity != null && DependencyGraph.get(IContextHolder.class).getContext() == mActivity &&  // проверка ссылки
                    DependencyGraph.get(IContextHolder.class).isValid(mActivity)) {
                ImageHelper.render(mActivity, mPreview, previewSource, new SizeImageOption(video.getWidth(), video.getHeight()));
            }
        }
    }

    /**
     * Скрывает превью и показывает фрейм видео
     */
    protected void hidePreview() {
        if (getVideoView() != null) {
            getVideoView().setVisibility(View.VISIBLE);
        }

        if (mPreview != null) {
            mPreview.setVisibility(View.GONE);
        }

        if (mProgressBar != null) {
            mProgressBar.setVisibility(View.GONE);
        }
    }


    /**
     * Базования настройка контроллера
     */
    @CallSuper
    protected void init() {
        findViews();
        configureController();
    }

    private void findViews() {
        mEnterFullscreen = mActivity.findViewById(R.id.exo_enter_fullscreen);
        mExitFullscreen = mActivity.findViewById(R.id.exo_exit_fullscreen);
    }

    private void configureController() {
        mEnterFullscreen.setOnClickListener(v -> enterFullScreen());
        mExitFullscreen.setOnClickListener(v -> exitFullScreen());
    }

    @Override
    public boolean isFullScreen() {
        return mIsFullscreen;
    }

    /**
     * Метод обратного вызова, который вызывается, когда видео развертывается в полный экран
     */
    @Override
    public void enterFullScreen() {
        mIsFullscreen = true;
        mEnterFullscreen.setVisibility(View.GONE);
        mExitFullscreen.setVisibility(View.VISIBLE);

        mActivity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        );
        if (mActivity.getSupportActionBar() != null) {
            mActivity.getSupportActionBar().hide();
        }
        mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
    }

    /**
     * Метод обратного вызова, который вызывается, когда видео свертывается
     */
    @Override
    public void exitFullScreen() {
        mIsFullscreen = false;
        mExitFullscreen.setVisibility(View.GONE);
        mEnterFullscreen.setVisibility(View.VISIBLE);

        mActivity.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
        if (mActivity.getSupportActionBar() != null) {
            mActivity.getSupportActionBar().show();
        }
        mActivity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
    }
}
