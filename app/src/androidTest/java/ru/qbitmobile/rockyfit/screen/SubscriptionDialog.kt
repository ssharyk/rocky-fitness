package ru.qbitmobile.rockyfit.screen

import com.agoda.kakao.image.KImageView
import ru.qbitmobile.rockyfit.billing.R

object SubscriptionDialog : KDialog<SubscriptionDialog>() {

    override val layoutId: Int? = R.layout.layout_billing

    val close = KImageView { withId(R.id.billing_close) }
}