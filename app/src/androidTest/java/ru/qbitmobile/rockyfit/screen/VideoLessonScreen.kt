package ru.qbitmobile.rockyfit.screen

import com.agoda.kakao.common.views.KView
import com.agoda.kakao.image.KImageView
import com.agoda.kakao.text.KTextView
import ru.qbitmobile.rockyfit.R
import ru.qbitmobile.rockyfit.activity.videolesson.VideoLessonView

object VideoLessonScreen : KScreen<VideoLessonScreen>() {
    override val layoutId: Int? = R.layout.activity_video_lesson
    override val viewClass: Class<*>? = VideoLessonView::class.java

    val name = KTextView { withId(R.id.video_lesson_video_name) }
    val preview = KImageView { withId(R.id.video_lesson_preview) }
    val videoView = KView { withId(R.id.video_lesson_video) }

    val likeCount: KTextView = KTextView { withId(R.id.list_item_likes_counter) }
    val likeIcon: KImageView = KImageView { withId(R.id.list_item_like_icon) }
}