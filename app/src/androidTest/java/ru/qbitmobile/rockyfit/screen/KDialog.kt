package ru.qbitmobile.rockyfit.screen

import com.agoda.kakao.screen.Screen

abstract class KDialog<out T : KDialog<T>> : Screen<T>() {
    abstract val layoutId: Int?
}