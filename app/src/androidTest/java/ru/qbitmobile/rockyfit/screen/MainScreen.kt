package ru.qbitmobile.rockyfit.screen

import android.view.View
import com.agoda.kakao.common.views.KView
import com.agoda.kakao.image.KImageView
import com.agoda.kakao.recycler.KRecyclerItem
import com.agoda.kakao.recycler.KRecyclerView
import com.agoda.kakao.text.KTextView
import ru.qbitmobile.rockyfit.R
import ru.qbitmobile.rockyfit.activity.main.MainActivity
import org.hamcrest.Matcher

object MainScreen : KScreen<MainScreen>() {
    override val layoutId: Int? = R.layout.activity_main
    override val viewClass: Class<*>? = MainActivity::class.java

    val mainList = KRecyclerView(
            {
                withId(R.id.main_screen_all)
            },
            {
                itemType(MainScreen::AllTrainingsListItemContainer)
                itemType(MainScreen::AllTagsListItemContainer)
                itemType(MainScreen::SingleVideoListItem)
            }
    )

    class AllTrainingsListItemContainer(parent: Matcher<View>) : KRecyclerItem<AllTrainingsListItemContainer>(parent) {
        val trainingsList = KRecyclerView(parent,
                {
                    withId(R.id.main_screen_trainings_list)
                },
                {
                    itemType(MainScreen::SingleTrainingListItem)
                })

        val progress = KView(parent) { withId(R.id.main_screen_trainings_loading) }
    }

    class AllTagsListItemContainer(parent: Matcher<View>) : KRecyclerItem<AllTrainingsListItemContainer>(parent) {
        val tagsList = KRecyclerView(parent,
                {
                    withId(R.id.main_screen_tags_list)
                },
                {
                    itemType(MainScreen::SingleTagListItem)
                })

        val progress = KView(parent) { withId(R.id.main_screen_tags_loading) }
    }

    class SingleTrainingListItem(parent: Matcher<View>) : KRecyclerItem<SingleTrainingListItem>(parent) {
        val likeCount: KTextView = KTextView(parent) { withId(R.id.list_item_likes_counter) }
        val likeIcon: KImageView = KImageView(parent) { withId(R.id.list_item_like_icon) }
    }

    class SingleTagListItem(parent: Matcher<View>) : KRecyclerItem<SingleTagListItem>(parent) {
        val tagName: KTextView = KTextView(parent) { withId(R.id.tag_chip_text) }
    }

    class SingleVideoListItem(parent: Matcher<View>) : KRecyclerItem<SingleVideoListItem>(parent) {
        val videoName: KTextView = KTextView(parent) { withId(R.id.list_item_tagged_video_name) }
        val lockIcon: KImageView = KImageView(parent) { withId(R.id.list_item_tagged_video_block) }

        val likeCount: KTextView = KTextView(parent) { withId(R.id.list_item_likes_counter) }
        val likeIcon: KImageView = KImageView(parent) { withId(R.id.list_item_like_icon) }
    }
}