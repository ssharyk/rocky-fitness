package ru.qbitmobile.rockyfit.screen

import android.view.View
import com.agoda.kakao.image.KImageView
import com.agoda.kakao.recycler.KRecyclerItem
import com.agoda.kakao.recycler.KRecyclerView
import com.agoda.kakao.tabs.KTabLayout
import com.agoda.kakao.text.KTextView
import ru.qbitmobile.rockyfit.R
import org.hamcrest.Matcher
import ru.qbitmobile.rockyfit.activity.lessondetails.LessonDetailsActivity

object DetailsScreen : KScreen<DetailsScreen>() {
    override val layoutId: Int? = R.layout.activity_training_details
    override val viewClass: Class<*>? = LessonDetailsActivity::class.java

    val tabs = KTabLayout { withId(R.id.course_details_tabs)}
    val name: KTextView = KTextView { withId(R.id.details_category_name) }
    val description: KTextView = KTextView { withId(R.id.details_description) }
    val previews = KRecyclerView(
            {
                withId(R.id.details_images_list)
            },
            {
                itemType(DetailsScreen::ImageListItem)
            }
    )
    val schedule = KRecyclerView(
            {
                withId(R.id.course_schedule_trainings_list)
            },
            {
                itemType(DetailsScreen::VideoLessonListItem)
            }
    )

    class ImageListItem(parent: Matcher<View>) : KRecyclerItem<ImageListItem>(parent) {
        val videoName: KTextView = KTextView(parent) { withId(R.id.list_item_media_name) }
        val likeCount: KTextView = KTextView(parent) { withId(R.id.list_item_likes_counter) }
        val likeIcon: KImageView = KImageView(parent) { withId(R.id.list_item_like_icon) }
    }

    class VideoLessonListItem(parent: Matcher<View>) : KRecyclerItem<VideoLessonListItem>(parent) {
        val videoName: KTextView = KTextView(parent) { withId(R.id.list_item_training_card_name) }
        val likeCount: KTextView = KTextView(parent) { withId(R.id.list_item_likes_counter) }
        val likeIcon: KImageView = KImageView(parent) { withId(R.id.list_item_like_icon) }
    }
}