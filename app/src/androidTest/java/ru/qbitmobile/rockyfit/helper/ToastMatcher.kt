package ru.qbitmobile.rockyfit.helper

import androidx.test.espresso.Root
import org.hamcrest.Description
import org.hamcrest.TypeSafeMatcher

// http://www.qaautomated.com/2016/01/how-to-test-toast-message-using-espresso.html

class ToastMatcher : TypeSafeMatcher<Root>() {
    override fun describeTo(description: Description) {
        description.appendText("is toast")
    }

    override fun matchesSafely(root: Root): Boolean {
        val windowToken = root.decorView.windowToken
        val appToken = root.decorView.applicationWindowToken
        if (windowToken === appToken) {
            return true
        }
        return false
    }
}