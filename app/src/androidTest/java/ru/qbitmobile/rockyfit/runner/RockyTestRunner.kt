package ru.qbitmobile.rockyfit.runner

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner
import ru.qbitmobile.rockyfit.mock.MockApplication


class RockyTestRunner : AndroidJUnitRunner() {
    @Throws(InstantiationException::class, IllegalAccessException::class, ClassNotFoundException::class)
    override fun newApplication(cl: ClassLoader, className: String, context: Context): Application {
        return super.newApplication(cl, MockApplication::class.java.name, context)
    }
}