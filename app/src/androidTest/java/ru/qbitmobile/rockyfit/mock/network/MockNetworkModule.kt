package ru.qbitmobile.rockyfit.mock.network

import android.os.Handler
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph
import ru.qbitmobile.rockyfit.contentmanager.model.Training
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson
import ru.qbitmobile.rockyfit.contentmanager.module.network.NetworkCallbackFailure
import ru.qbitmobile.rockyfit.contentmanager.module.network.NetworkCallbackSuccess
import ru.qbitmobile.rockyfit.contentmanager.module.network.NetworkService
import ru.qbitmobile.rockyfit.contentmanager.module.network.parser.IJsonProviderFactory
import org.json.JSONArray
import org.json.JSONObject
import ru.qbitmobile.rockyfit.contentmanager.module.network.model.Like


class MockNetworkModule(private val delayFactory: NetworkDelayFactory) : NetworkService {

    override fun loadTrainings(onSuccess: NetworkCallbackSuccess<MutableList<Training>>?, onError: NetworkCallbackFailure?) {
        Handler().postDelayed( {
            val json = wrapJsonArray(NetworkCategories)
            val categories = DependencyGraph.get(IJsonProviderFactory::class.java)?.categoriesListParser?.parse(json.toString())
            onSuccess?.onSuccess(categories?.data?.toMutableList() ?: ArrayList())
        }, delayFactory.getDelayOfRequest(Apis.LOAD_ALL_TRAININGS))
    }

    override fun loadVideos(onSuccess: NetworkCallbackSuccess<MutableList<VideoLesson>>?, onError: NetworkCallbackFailure?) {
        Handler().postDelayed( {
            val json = wrapJsonArray(NetworkVideos)
            val videos = DependencyGraph.get(IJsonProviderFactory::class.java)?.videosListParser?.parse(json.toString())
            onSuccess?.onSuccess(videos?.data?.toMutableList() ?: ArrayList())
        }, delayFactory.getDelayOfRequest(Apis.LOAD_ALL_VIDEOS))
    }

    override fun postLikeVideo(videoId: Long, isSet: Boolean, onSuccess: NetworkCallbackSuccess<Like>?, onError: NetworkCallbackFailure?) {
        Handler().postDelayed( {
            val json = wrapJsonObject(if (isSet) NetworkLike else NetworkDislike)
            val like = DependencyGraph.get(IJsonProviderFactory::class.java)?.likeParser?.parse(json.toString())
            onSuccess?.onSuccess(like?.data ?: Like(1, now, true))
        }, delayFactory.getDelayOfRequest(Apis.POST_LIKE_VIDEO))
    }

    private fun wrapJsonArray(data: JSONArray): JSONObject {
        return JSONObject(mapOf(
                "status" to 200,
                "message" to "Ok",
                "data" to data,
                "errors" to JSONArray(),
                "total" to data.length()
        ))
    }

    private fun wrapJsonObject(data: JSONObject): JSONObject {
        return JSONObject(mapOf(
                "status" to 200,
                "message" to "Ok",
                "data" to data,
                "errors" to JSONArray()
        ))
    }
}