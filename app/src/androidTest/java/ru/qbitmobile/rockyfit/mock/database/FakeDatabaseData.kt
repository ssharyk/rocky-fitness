package ru.qbitmobile.rockyfit.mock.database

import ru.qbitmobile.rockyfit.contentmanager.model.*
import ru.qbitmobile.rockyfit.contentmanager.model.*

val DatabaseComplexity = listOf(
        Complexity(1L, "Новичок"),
        Complexity(2L, "Сложно"),
        Complexity(3L, "Продвинутый")
)

val DatabaseCatalogs = listOf(
        Catalog(1L, "Тренировки", "trainings"),
        Catalog(2L, "Мастер классы", "masters")
)

val DatabaseCoaches = listOf(
        Coach(4L, "Эдгар", "Колян")
)

val DatabaseTrainings = listOf(
        Training(1L, 1, 1L, 4L, "Training1", "DESC 1", null, 1, 11L, 2L, 1L, 11, false, 0),
        Training(2L, 2, 1L, 4L, "Training2", "DESC 2", null, 1, 11L, 1L, 2L, 22, true, System.currentTimeMillis()),
        Training(3L, 1, 2L, 4L, "Course1", "DESC 3", null, 2, 11L, 3L, 3L, 33, false, 0L)
)

val DatabaseVideos = listOf(
        VideoLesson(1L, 3L, 1, 111L, "Video1", "Video1 - DESC", 1L, 1280, 720, "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", 11, false, 0L, false),
        VideoLesson(2L, 1L, 1, 222L, "Video2", "Video2 - DESC", 2L, 1280, 720, "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", 22, false, 0L, true),
        VideoLesson(3L, 3L, 2, 333L, "Video3", "Video3 - DESC", 3L, 1280, 720, "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4", 33, false, 0L, false)
)