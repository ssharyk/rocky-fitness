package ru.qbitmobile.rockyfit.mock.database

import ru.qbitmobile.rockyfit.contentmanager.model.DaoSession
import ru.qbitmobile.rockyfit.contentmanager.module.db.DBModule
import ru.qbitmobile.rockyfit.contentmanager.module.db.DaoOpenHelper

class MockDatabaseModule : DBModule {

    /**
     * Работа с пустыми таблицами, если не переопределены отдельные методы для использования фейковых данных
     */
    constructor(daoOpenHelper: DaoOpenHelper) : super(daoOpenHelper)
    constructor(daoSession: DaoSession) : super(daoSession)
}