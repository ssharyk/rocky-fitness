package ru.qbitmobile.rockyfit.mock

import ru.qbitmobile.rockyfit.contentmanager.module.network.connection.ConnectionService

class MockConnectionModule : ConnectionService {
    override val connectionStatus: Int
        get() = ConnectionService.CONNECTED
}