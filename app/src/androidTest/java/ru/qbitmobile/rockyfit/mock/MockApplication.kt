package ru.qbitmobile.rockyfit.mock

import android.app.Application
import android.content.Context
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph
import ru.qbitmobile.rockyfit.contentmanager.module.configurator.ConfigurationService
import ru.qbitmobile.rockyfit.contentmanager.module.contextholder.IContextHolder
import ru.qbitmobile.rockyfit.contentmanager.module.db.DBModule
import ru.qbitmobile.rockyfit.contentmanager.module.db.DaoOpenHelper
import ru.qbitmobile.rockyfit.contentmanager.module.db.DatabaseService
import ru.qbitmobile.rockyfit.contentmanager.module.network.NetworkService
import ru.qbitmobile.rockyfit.contentmanager.module.network.connection.ConnectionService
import ru.qbitmobile.rockyfit.contentmanager.module.network.networktask.NetworkTaskModule
import ru.qbitmobile.rockyfit.contentmanager.module.network.networktask.NetworkTaskService
import ru.qbitmobile.rockyfit.contentmanager.module.sharedpreference.SharedPreferenceService
import ru.qbitmobile.rockyfit.helper.DeviceIdProvider
import ru.qbitmobile.rockyfit.mock.network.Apis
import ru.qbitmobile.rockyfit.mock.network.MockNetworkModule
import ru.qbitmobile.rockyfit.mock.network.NetworkDelayFactory

class MockApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        DependencyGraph.init()
        DependencyGraph.get(IContextHolder::class.java).context = this
        DependencyGraph.install(ConfigurationService::class.java, TestConfigurationModule())
        DependencyGraph.install(DatabaseService::class.java, getDatabaseModule(this))
        DependencyGraph.install(ConnectionService::class.java, MockConnectionModule())
        DependencyGraph.install(NetworkTaskService::class.java, NetworkTaskModule())

        DependencyGraph.install(NetworkService::class.java, MockNetworkModule(object : NetworkDelayFactory {
            override fun getDelayOfRequest(apiName: Apis): Long {
                return NETWORK_DELAY
            }
        }))

        if (DependencyGraph.get(SharedPreferenceService::class.java).sharedGetDeviceId() == null) {
            val uuid = DeviceIdProvider.retrieveDeviceId()
            DependencyGraph.get(SharedPreferenceService::class.java).sharedSetDeviceId(uuid)
        }
    }

    companion object {
        fun getDatabaseModule(context: Context): DatabaseService {
            return DBModule(DaoOpenHelper(context, "rocky-db-test"))
        }

        const val NETWORK_DELAY = 400L
    }
}