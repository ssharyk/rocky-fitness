package ru.qbitmobile.rockyfit.mock.network

enum class Apis {
    LOAD_ALL_TRAININGS,
    LOAD_ALL_VIDEOS,
    POST_LIKE_VIDEO,
}