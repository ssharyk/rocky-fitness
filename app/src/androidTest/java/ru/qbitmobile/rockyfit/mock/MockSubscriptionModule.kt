package ru.qbitmobile.rockyfit.mock

import ru.qbitmobile.rockyfit.contentmanager.module.subscription.SubscriptionService

class MockSubscriptionModule : SubscriptionService {
    override fun subscriptionInit() {
    }

    override fun subscriptionPresent(): Boolean = true

    override fun subscriptionShowDialog(onSuccess: Runnable?) {
    }
}