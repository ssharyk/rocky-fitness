package ru.qbitmobile.rockyfit.mock.network

interface NetworkDelayFactory {
    fun getDelayOfRequest(apiName: Apis): Long
}