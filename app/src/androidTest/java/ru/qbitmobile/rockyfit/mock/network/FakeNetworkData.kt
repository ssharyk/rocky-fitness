package ru.qbitmobile.rockyfit.mock.network

import org.json.JSONArray
import org.json.JSONObject

val now = System.currentTimeMillis()

val NetworkCatalogTraining = JSONObject(mapOf(
        "_id" to 1,
        "name" to "Тренировки",
        "alias" to "training"
))
val NetworkCatalogMasterClass = JSONObject(mapOf(
        "_id" to 2,
        "name" to "Мастер классы",
        "alias" to "masters"
))
val NetworkCatalogs: JSONArray = JSONArray(listOf(
        NetworkCatalogTraining,
        NetworkCatalogMasterClass
))

val NetworkCoach = JSONObject(mapOf(
        "_id" to 4L,
        "firstName" to "Эдгар",
        "lastName" to "Колян"
))

val NetworkComplexityEasy = JSONObject(mapOf(
        "_id" to 1L,
        "name" to "Новичок"
))
val NetworkComplexityHard = JSONObject(mapOf(
        "_id" to 2L,
        "name" to "Сложно"
))
val NetworkComplexityAdvanced = JSONObject(mapOf(
        "_id" to 3L,
        "name" to "Продвинутый"
))
val NetworkComplexity = JSONArray(listOf(
        NetworkComplexityEasy,
        NetworkComplexityHard,
        NetworkComplexityAdvanced
))


fun createImage(id: Long): JSONObject =
        JSONObject(mapOf(
                "_id" to id,
                "dateCreate" to now,
                "dateUpdate" to now,
                "mimetype" to "image/jpeg",
                "width" to 300,
                "height" to 300,
                "size" to 123456,
                "url" to "https://picsum.photos/id/${100 * id + 5}/300/300"
        ))


val NetworkTraining1 = JSONObject(mapOf(
        "_id" to 1L,
        "dateCreate" to now,
        "dateUpdate" to now,
        "catalog" to NetworkCatalogTraining,
        "number" to 0,
        "name" to "Training1",
        "coach" to NetworkCoach,
        "complexity" to NetworkComplexityHard,
        "description" to "DESC 1",
        "tags" to JSONArray(listOf<String>(
                "tag1", "tag2"
        )),
        "duration" to 0,
        "totalVideos" to 1,
        "likes" to 11,
        "isLike" to false,
        "image" to createImage(1L)
))
val NetworkTraining2 = JSONObject(mapOf(
        "_id" to 2L,
        "dateCreate" to now,
        "dateUpdate" to now,
        "catalog" to NetworkCatalogTraining,
        "name" to "Training2",
        "number" to 0,
        "coach" to NetworkCoach,
        "complexity" to NetworkComplexityEasy,
        "description" to "DESC 2",
        "tags" to JSONArray(listOf<String>(
        )),
        "duration" to 0,
        "totalVideos" to 0,
        "likes" to 22,
        "isLike" to false,
        "image" to createImage(2L)
))

val NetworkMasterClass1 = JSONObject(mapOf(
        "_id" to 3L,
        "dateCreate" to now,
        "dateUpdate" to now,
        "catalog" to NetworkCatalogMasterClass,
        "name" to "Course1",
        "number" to 0,
        "coach" to NetworkCoach,
        "complexity" to NetworkComplexityAdvanced,
        "description" to "DESC 3",
        "tags" to JSONArray(listOf<String>(
                "tag3"
        )),
        "duration" to 0,
        "totalVideos" to 2,
        "likes" to 33,
        "isLike" to true,
        "image" to createImage(3L)
))

val NetworkCategories: JSONArray = JSONArray(listOf(
        NetworkTraining1,
        NetworkTraining2,

        NetworkMasterClass1
))


val NetworkVideo1 = JSONObject(mapOf(
        "_id" to 1L,
        "dateCreate" to now,
        "dateUpdate" to now,
        "categoryId" to 3L,
        "name" to "Video1",
        "description" to "Video1 - DESC",
        "number" to 1,
        "duration" to 111,
        "tags" to JSONArray(listOf<String>(
                "VideoTag11", "VideoTag12"
        )),
        "likes" to 11,
        "isLike" to false,
        "image" to createImage(1L),
        "mimetype" to "video/mp4",
        "video" to "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
        "stream" to null,
        "width" to 1280,
        "height" to 720,
        "free" to false
))
val NetworkVideo2 = JSONObject(mapOf(
        "_id" to 2L,
        "dateCreate" to now,
        "dateUpdate" to now,
        "categoryId" to 1L,
        "name" to "Video2",
        "description" to "Video2 - DESC",
        "number" to 1,
        "duration" to 222,
        "tags" to JSONArray(listOf<String>(
                "VideoTag21", "VideoTag22"
        )),
        "likes" to 22,
        "isLike" to false,
        "image" to createImage(2L),
        "mimetype" to "video/mp4",
        "video" to "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
        "stream" to null,
        "width" to 1280,
        "height" to 720,
        "free" to true
))
val NetworkVideo3 = JSONObject(mapOf(
        "_id" to 3L,
        "dateCreate" to now,
        "dateUpdate" to now,
        "categoryId" to 3L,
        "name" to "Video3",
        "description" to "Video3 - DESC",
        "number" to 2,
        "duration" to 333,
        "tags" to JSONArray(listOf<String>(
        )),
        "likes" to 33,
        "isLike" to false,
        "image" to createImage(3L),
        "mimetype" to "video/mp4",
        "video" to "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
        "stream" to null,
        "width" to 1280,
        "height" to 720,
        "free" to false
))
val NetworkVideos = JSONArray(listOf(
        NetworkVideo1,
        NetworkVideo2,
        NetworkVideo3
))

val NetworkLike = JSONObject(mapOf(
        "isLike" to true,
        "likes" to 12,
        "dateLike" to now
))
val NetworkDislike = JSONObject(mapOf(
        "isLike" to false,
        "likes" to 11,
        "dateLike" to now
))