package ru.qbitmobile.rockyfit.mock

import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson
import ru.qbitmobile.rockyfit.contentmanager.module.configurator.ConfigurationService

class TestConfigurationModule : ConfigurationService {
    override fun getServerDomain(): String {
        return ""
    }

    override fun shuffle(origin: MutableList<VideoLesson>?): List<VideoLesson> {
        return origin?.sortedWith(compareBy({ it.trainingId }, { it.step })) ?: emptyList()
    }
}