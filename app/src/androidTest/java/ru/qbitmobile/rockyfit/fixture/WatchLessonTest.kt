package ru.qbitmobile.rockyfit.fixture

import android.content.Intent
import androidx.test.core.app.ApplicationProvider
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import ru.qbitmobile.rockyfit.activity.main.MainActivity
import ru.qbitmobile.rockyfit.helper.ElapsedTimeIdlingResource
import ru.qbitmobile.rockyfit.screen.DetailsScreen
import ru.qbitmobile.rockyfit.screen.MainScreen
import ru.qbitmobile.rockyfit.screen.VideoLessonScreen
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
/**
 * Сценарий:
 * 1. Предусловие: Открыть главное окно
 * 2. Действие: Перейти в описание тренировки
 * 3. Проверить: Показано превью урока
 * 4. Действие: Перейти во вкладку "График"
 * 5. Проверить: Показано превью урока
 * 6. Действие: Перейти в видео
 * 7. Проверить: показано превью и описание
 * 8. Проверить: показано видео
 */
class WatchLessonTest : BaseTestCase() {

    companion object {
        private const val VIDEO_NAME = "Video2"
        private const val VIDEO_LOADING_TIMEOUT = 2000L
    }

    // region Tests

    override fun prepare() {
        super.prepare()
        resetDb()

        val intent = Intent(ApplicationProvider.getApplicationContext(), MainActivity::class.java)
        launchActivity<MainActivity>(intent)
    }

    @Test
    fun whenNavigateScreens_thenValidContentRendered() {
        before("Дойти до просмотра видео") {
            prepare()
        }.after {
        }.run {
            step("Перейти на детали тренировки") {
                MainScreen {
                    mainList {
                        childAt<MainScreen.AllTrainingsListItemContainer>(1) {
                            trainingsList {
                                isVisible()

                                childAt<MainScreen.SingleTrainingListItem>(0) {
                                    click()
                                }
                            }
                        }

                    }
                }
            }

            step("Проверить, что детали отображаются корректно") {
                DetailsScreen {
                    name {
                        isVisible()
                        hasText("Training1")
                    }

                    description {
                        isVisible()
                        hasText("DESC 1")
                    }

                    previews {
                        hasSize(1)
                        childAt<DetailsScreen.ImageListItem>(0) {
                            videoName {
                                isVisible()
                                hasText(VIDEO_NAME)
                            }
                        }
                    }

                    tabs {
                        selectTab(1)
                    }

                    schedule {
                        isVisible()
                        hasSize(1)
                        childAt<DetailsScreen.VideoLessonListItem>(0) {
                            videoName {
                                isVisible()
                                hasText(VIDEO_NAME)
                            }
                        }
                    }
                }
            }

            step("Перейти к просмотру видео") {
                DetailsScreen.schedule {
                    childAt<DetailsScreen.VideoLessonListItem>(0) {
                        click()
                    }
                }

                VideoLessonScreen {
                    name {
                        isVisible()
                        hasText(VIDEO_NAME)
                    }

                    val videoWait = ElapsedTimeIdlingResource(VIDEO_LOADING_TIMEOUT)
                    Espresso.registerIdlingResources(videoWait)

                    preview.isGone()
                    videoView.isVisible()

                    Espresso.unregisterIdlingResources(videoWait)
                }
            }
        }
    }

    // endregion

}