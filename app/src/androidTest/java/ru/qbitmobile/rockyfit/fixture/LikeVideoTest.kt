package ru.qbitmobile.rockyfit.fixture

import android.content.Intent
import androidx.test.core.app.ApplicationProvider
import androidx.test.core.app.launchActivity
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.agoda.kakao.image.KImageView
import com.agoda.kakao.text.KTextView
import org.junit.Test
import org.junit.runner.RunWith
import ru.qbitmobile.rockyfit.R
import ru.qbitmobile.rockyfit.activity.main.MainActivity
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph
import ru.qbitmobile.rockyfit.contentmanager.module.subscription.SubscriptionService
import ru.qbitmobile.rockyfit.mock.MockSubscriptionModule
import ru.qbitmobile.rockyfit.screen.DetailsScreen
import ru.qbitmobile.rockyfit.screen.MainScreen
import ru.qbitmobile.rockyfit.screen.VideoLessonScreen

@RunWith(AndroidJUnit4::class)
@LargeTest
class LikeVideoTest : BaseTestCase() {

    companion object {
        private const val ORIGIN_VIDEO_LIKES = 11
        private const val ORIGIN_TRAINING_LIKES = 33
    }

    // region Tests

    override fun prepare() {
        super.prepare()

        resetDb()
        DependencyGraph.install(SubscriptionService::class.java, MockSubscriptionModule())

        val intent = Intent(ApplicationProvider.getApplicationContext(), MainActivity::class.java)
        launchActivity<MainActivity>(intent)
    }

    @Test
    fun whenLikeVideo_thenAllAccordingItemsUpdated() {
        before("Обновить все записи о лайках при постановке лайка на видео") {
            prepare()
        }.after {
        }.run {
            step("Кликнуть на записи в списке видео") {

                MainScreen {
                    mainList {
                        childAt<MainScreen.SingleVideoListItem>(5) {
                            validateHasNotLike(likeIcon, likeCount, ORIGIN_VIDEO_LIKES)

                            likeIcon {
                                click()
                            }

                            validateHasLike(likeIcon, likeCount, ORIGIN_VIDEO_LIKES + 1)
                        }
                    }
                }

            }

            step("Проверить изменение в тренировке") {
                MainScreen {
                    mainList {
                        childAt<MainScreen.AllTrainingsListItemContainer>(2) {
                            trainingsList.childAt<MainScreen.SingleTrainingListItem>(0) {
                                validateHasNotLike(likeIcon, likeCount, ORIGIN_TRAINING_LIKES + 1)
                            }
                        }
                    }
                }
            }

            step("Перейти на окно с деталями тренировки и убедиться, что все лайки обновлены") {
                // переход
                MainScreen {
                    mainList {
                        childAt<MainScreen.AllTrainingsListItemContainer>(2) {
                            trainingsList.childAt<MainScreen.SingleTrainingListItem>(0) {
                                click()
                            }
                        }
                    }
                }

                // проверка
                validateDetails(true)
            }

            step("Перейти на окно с видео и убедиться, что все лайки обновлены") {
                DetailsScreen.schedule {
                    childAt<DetailsScreen.VideoLessonListItem>(0) {
                        click()
                    }
                }

                VideoLessonScreen {
                    validateHasLike(likeIcon, likeCount, ORIGIN_VIDEO_LIKES + 1)
                }
            }

            step("Снять лайк и проверить, что все лайки обновлены на всех экранах") {
                // экран видео
                VideoLessonScreen {
                    likeIcon.click()
                    validateHasNotLike(likeIcon, likeCount, ORIGIN_VIDEO_LIKES)

                    pressBack()
                }

                // график и описание
                validateDetails(false)

                DetailsScreen {
                    pressBack()
                }

                // главный экран

                MainScreen {
                    mainList {
                        childAt<MainScreen.SingleVideoListItem>(5) {
                            validateHasNotLike(likeIcon, likeCount, ORIGIN_VIDEO_LIKES)
                        }

                        childAt<MainScreen.AllTrainingsListItemContainer>(2) {
                            trainingsList.childAt<MainScreen.SingleTrainingListItem>(0) {
                                validateHasNotLike(likeIcon, likeCount, ORIGIN_TRAINING_LIKES)
                            }
                        }
                    }
                }

            }
        }
    }

    // endregion

    // region Validation helpers

    private fun validateHasNotLike(likeIcon: KImageView, likeCount: KTextView, expectedCount: Int) {
        likeIcon {
            isVisible()
            hasDrawable(R.drawable.like_white)
        }

        likeCount {
            isVisible()
            hasTextColor(R.color.content_color)
            hasText(expectedCount.toString())
        }
    }

    private fun validateHasLike(likeIcon: KImageView, likeCount: KTextView, expectedCount: Int) {
        likeIcon {
            isVisible()
            hasDrawable(R.drawable.like_golden)
        }

        likeCount {
            isVisible()
            hasTextColor(R.color.colorDefaultGolden)
            hasText(expectedCount.toString())
        }
    }

    private fun validateDetails(hasLike: Boolean) {
        DetailsScreen {
            // на вкладке с описанием

            tabs {
                selectTab(0)
            }

            previews {
                childAt<DetailsScreen.ImageListItem>(0) {
                    if (hasLike) {
                        validateHasLike(likeIcon, likeCount, ORIGIN_VIDEO_LIKES + 1)
                    } else {
                        validateHasNotLike(likeIcon, likeCount, ORIGIN_VIDEO_LIKES)
                    }
                }
            }

            tabs {
                selectTab(1)
            }

            // на вкладке с графиком
            schedule {
                childAt<DetailsScreen.VideoLessonListItem>(0) {
                    if (hasLike) {
                        validateHasLike(likeIcon, likeCount, ORIGIN_VIDEO_LIKES + 1)
                    } else {
                        validateHasNotLike(likeIcon, likeCount, ORIGIN_VIDEO_LIKES)
                    }
                }
            }
        }
    }

    // endregion
}