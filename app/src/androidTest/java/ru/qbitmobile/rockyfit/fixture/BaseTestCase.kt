package ru.qbitmobile.rockyfit.fixture

import androidx.annotation.CallSuper
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph
import ru.qbitmobile.rockyfit.contentmanager.module.db.DatabaseService
import ru.qbitmobile.rockyfit.mock.database.*
import com.kaspersky.kaspresso.testcases.api.testcase.TestCase
import org.junit.Before
import ru.qbitmobile.rockyfit.contentmanager.module.sharedpreference.SharedPreferenceService
import ru.qbitmobile.rockyfit.contentmanager.module.subscription.SubscriptionModule
import ru.qbitmobile.rockyfit.contentmanager.module.subscription.SubscriptionService
import ru.qbitmobile.rockyfit.mock.database.*
import java.util.*

open class BaseTestCase : TestCase() {

    @Before
    fun setUp() {
        clearDb()
    }

    @CallSuper
    protected open fun prepare() {
        DependencyGraph.install(SubscriptionService::class.java, SubscriptionModule())
        clearSharedPreferences()
    }

    private fun clearDb() {
        (DependencyGraph.get(DatabaseService::class.java))?.apply {
            dropAllTables()
        }
    }

    private fun clearSharedPreferences() {
        (DependencyGraph.get(SharedPreferenceService::class.java))?.apply {
            dropAllRecords()
        }
    }

    protected fun resetDb() {
        (DependencyGraph.get(DatabaseService::class.java))?.apply {
            insertCatalogs(DatabaseCatalogs)
            insertComplexity(DatabaseComplexity)
            insertCoaches(DatabaseCoaches)

            insertTrainings(DatabaseTrainings)
            insertVideoLessons(DatabaseVideos)
        }
    }
}