package ru.qbitmobile.rockyfit.fixture

import android.content.Intent
import androidx.test.core.app.ApplicationProvider
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.registerIdlingResources
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import com.agoda.kakao.dialog.KAlertDialog
import ru.qbitmobile.rockyfit.R
import ru.qbitmobile.rockyfit.activity.main.MainActivity
import ru.qbitmobile.rockyfit.helper.ElapsedTimeIdlingResource
import ru.qbitmobile.rockyfit.helper.ToastMatcher
import ru.qbitmobile.rockyfit.mock.MockApplication
import ru.qbitmobile.rockyfit.screen.MainScreen
import org.junit.Test
import org.junit.runner.RunWith
import ru.qbitmobile.rockyfit.screen.SubscriptionDialog

@RunWith(AndroidJUnit4::class)
@LargeTest
class LoadDataTest : BaseTestCase() {

    // region Test

    override fun prepare() {
        super.prepare()
        val intent = Intent(ApplicationProvider.getApplicationContext(), MainActivity::class.java)
        launchActivity<MainActivity>(intent)
    }

    @Test
    fun whenOpenScreen_thenCategoriesLoaded() {
        before("Данные отображатся на главном экране") {
            prepare()
        }.after {
        }.run {
            val idlingResource = ElapsedTimeIdlingResource(MockApplication.NETWORK_DELAY)
            registerIdlingResources(idlingResource)

            step(" Проверить, что все данные загружены") {
                MainScreen {
                    mainList {
                        hasSize(7) // toolbar + trainings + masterclasses + tags + 3 video
                    }
                }
            }

            step("Проверить, что тренировки и мастер-классы содержат верные данные") {
                validateTrainingsPresent()
            }

            step("Проверить, что список видеоуроков содержит верные данные") {
                validateVideosPresent()
            }

            Espresso.unregisterIdlingResources(idlingResource)
        }
    }

    // endregion

    // region Validation helpers

    private fun validateTrainingsPresent() {
        MainScreen {
            mainList {

                // 2 тренировки
                childAt<MainScreen.AllTrainingsListItemContainer>(1) {
                    trainingsList {
                        isVisible()
                        hasSize(2)
                        /// TODO: validate content itself
                    }

                    progress.isGone()
                }

                // 1 мастер класс
                childAt<MainScreen.AllTrainingsListItemContainer>(2) {
                    trainingsList {
                        isVisible()
                        hasSize(1)
                    }

                    progress.isGone()
                }

            }
        }
    }

    private fun validateVideosPresent() {
        MainScreen {
            mainList {

                // теги из свободных видео
                childAt<MainScreen.AllTagsListItemContainer>(3) {
                    tagsList {
                        isVisible()
                        hasSize(4)
                        childAt<MainScreen.SingleTagListItem>(0) { tagName.hasText("VideoTag11") }
                        childAt<MainScreen.SingleTagListItem>(1) { tagName.hasText("VideoTag12") }
                        childAt<MainScreen.SingleTagListItem>(2) { tagName.hasText("VideoTag21") }
                        childAt<MainScreen.SingleTagListItem>(3) { tagName.hasText("VideoTag22") }
                    }

                    progress.isGone()
                }

                // сами видео
                childAt<MainScreen.SingleVideoListItem>(4) {
                    videoName.hasText("Video2")
                    lockIcon.isGone()
                }

                childAt<MainScreen.SingleVideoListItem>(5) {
                    videoName.hasText("Video1")
                    lockIcon.isVisible()
                    click()

                    SubscriptionDialog {
                        close.isVisible()
                    }
                }
            }
        }
    }

    // endregion
}