package ru.qbitmobile.rockyfit.billing;

public interface BillingCallback {
    /**
     * Вызывается при первой инициализации клиента и всякий раз, когда изменяются данные о наличии подписки
     *
     * @param hasSubscription Индикатор наличия подписки
     */
    void onPurchasePresentChanged(boolean hasSubscription);

    /**
     * Вызывается, когда процесс покупки завершен успешно
     */
    void onPurchaseCompleted();
}
