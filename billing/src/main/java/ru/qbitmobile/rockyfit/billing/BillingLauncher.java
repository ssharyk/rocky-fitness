package ru.qbitmobile.rockyfit.billing;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ContextThemeWrapper;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.qbitmobile.rockyfit.commoncomponent.helper.Constant;

public class BillingLauncher {

    private BillingClient mBillingClient;
    private Map<String, SkuDetails> mSkuDetailsMap = new HashMap<>();
    private BillingCallback mCallback;

    public BillingLauncher(BillingCallback callback) {
        this.mCallback = callback;
    }

    public void initBilling(final Context context) {
        mBillingClient = BillingClient.newBuilder(context)
                .enablePendingPurchases()
                .setListener(new PurchasesUpdatedListener() {
                    @Override
                    public void onPurchasesUpdated(BillingResult billingResult, @Nullable List<Purchase> list) {
                        if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                            if (list.isEmpty()) {
                                // ничего не изменилось
                                return;
                            }

                            boolean done = billingCheckSubscriptionPresent(list);
                            billingRegisterSubscription(done);
                            if (done) {
                                // осуществлена покупка - сообщить об этом клиентскому коду
                                mCallback.onPurchaseCompleted();
                            }
                        }
                    }
                })
                .build();

        mBillingClient.startConnection(new BillingClientStateListener() {

            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    // получить данные о доступных покупках
                    queryAvailableSkuDetails();
                }

                // определить, если ли уже у пользователя подписка
                billingRegisterSubscription(billingCheckSubscriptionPresent(queryPurchasesMadeByUser()));
            }

            @Override
            public void onBillingServiceDisconnected() {
                // сюда мы попадем если что-то пойдет не так
                billingShowErrorMessage(context);
            }
        });

    }

    // region Проверки подписок

    private void billingRegisterSubscription(boolean hasSubscription) {
        if (mCallback != null) {
            mCallback.onPurchasePresentChanged(hasSubscription);
        }
    }

    private boolean billingCheckSubscriptionPresent(List<Purchase> readyPurchases) {

        if (readyPurchases.isEmpty()) {
            return false;
        }

        for (Purchase p : readyPurchases) {
            if (!billingIsOrderValid(p.getOrderId()))
                continue;

            String sku = p.getSku();
            switch (sku) {
                case Constant.Subscription.SUBSCRIPTION_MONTH:
                    return true;
                default:
                    break;
            }
        }

        return false;
    }

    private List<Purchase> queryPurchasesMadeByUser() {
        Purchase.PurchasesResult purchasesResultSubs = mBillingClient.queryPurchases(BillingClient.SkuType.SUBS);
        List<Purchase> list = new ArrayList<>();
        List<Purchase> listSubs = purchasesResultSubs.getPurchasesList();
        if (listSubs != null && !listSubs.isEmpty()) {
            list.addAll(purchasesResultSubs.getPurchasesList());
        }
        return list;
    }

    private boolean billingIsOrderValid(String orderId) {
        return orderId.contains("GPA");
    }

    private void queryAvailableSkuDetails() {
        SkuDetailsParams.Builder skuDetailsParamsBuilder = SkuDetailsParams.newBuilder();
        List<String> skuList = new ArrayList<>();
        skuList.add(Constant.Subscription.SUBSCRIPTION_MONTH);
        skuDetailsParamsBuilder.setSkusList(skuList).setType(BillingClient.SkuType.SUBS);
        mBillingClient.querySkuDetailsAsync(skuDetailsParamsBuilder.build(), new SkuDetailsResponseListener() {
            @Override
            public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> list) {
                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
                    for (SkuDetails skuDetails : list) {
                        mSkuDetailsMap.put(skuDetails.getSku(), skuDetails);
                    }
                }
            }
        });
    }

    // endregion

    // region Взаимодействие с основным приложением

    public void showDialog(final Context context) {
        if (!(context instanceof Activity)) {
            return;
        }

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.Rocky_Dialog_Billing));
        LayoutInflater inflater = LayoutInflater.from(context);
        View dialogView = inflater.inflate(R.layout.layout_billing, null, false);
        dialogBuilder.setView(dialogView);
        final AlertDialog dialog = dialogBuilder.create();

        dialogView.findViewById(R.id.billing_buy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchBilling((Activity) context, Constant.Subscription.SUBSCRIPTION_MONTH);
                dialog.cancel();
            }
        });
        dialogView.findViewById(R.id.billing_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);

        dialog.show();
    }

    private void launchBilling(Activity context, String skuId) {
        BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                .setSkuDetails(mSkuDetailsMap.get(skuId))
                .build();
        mBillingClient.launchBillingFlow(context, billingFlowParams);
    }

    private void billingShowErrorMessage(Context context) {
        Toast.makeText(
                context,
                R.string.error_billing,
                Toast.LENGTH_SHORT
        ).show();
    }

    // endregion
}
