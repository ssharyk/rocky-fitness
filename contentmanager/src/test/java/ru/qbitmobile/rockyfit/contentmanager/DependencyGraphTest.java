package ru.qbitmobile.rockyfit.contentmanager;

import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.repository.ITrainingRepository;

import org.junit.Assert;
import org.junit.Test;

public class DependencyGraphTest {

    interface FakeService extends Service {
    }

    @Test
    public void testCourseRepository_thenReturnInstalled() {
        DependencyGraph.init();

        ITrainingRepository repository = DependencyGraph.get(ITrainingRepository.class);
        Assert.assertNotNull(repository);
    }

    @Test
    public void testInvalidDependency_thenReturnNull() {
        DependencyGraph.init();

        FakeService repository = DependencyGraph.get(FakeService.class);
        Assert.assertNull(repository);
    }
}
