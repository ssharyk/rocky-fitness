package ru.qbitmobile.rockyfit.contentmanager

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.ArgumentsProvider
import org.junit.jupiter.params.provider.ArgumentsSource
import ru.qbitmobile.rockyfit.contentmanager.repository.fake.words
import java.util.stream.Stream

class StringExtensionTest {
    @ParameterizedTest
    @ArgumentsSource(WordsInStringProvider::class)
    fun whenTakeWords_thenReturnSubstring(arg: WordsInStringArgument) {
        val actualWords = arg.string.words(arg.count)
        Assertions.assertEquals(arg.firstWords, actualWords)
    }
}


class WordsInStringProvider : ArgumentsProvider {
    override fun provideArguments(context: ExtensionContext?): Stream<out Arguments> = Stream.of(
            WordsInStringArgument("", 0, ""),
            WordsInStringArgument("Hello", 0, ""),
            WordsInStringArgument("Hello world", 0, ""),
            WordsInStringArgument("Hello world", 1, "Hello"),
            WordsInStringArgument("Hello world", 2, "Hello world"),
            WordsInStringArgument("Hello world", 3, "Hello world"),
            WordsInStringArgument("Hello world", -1, "")
    ).map { Arguments.of(it) }
}

data class WordsInStringArgument(val string: String, val count: Int, val firstWords: String)
