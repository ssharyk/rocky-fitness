package ru.qbitmobile.rockyfit.contentmanager.enumeration;

import androidx.annotation.LongDef;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target({ElementType.PARAMETER, ElementType.FIELD, ElementType.TYPE_USE})
@LongDef({LessonType.TRAINING, LessonType.MASTER_CLASS})
@Retention(RetentionPolicy.SOURCE)
public @interface LessonType {
    long TRAINING = 1;
    long MASTER_CLASS = 2;
}