package ru.qbitmobile.rockyfit.contentmanager.model;

public interface Markable {
    long getId();

    int getLikes();

    void setLikes(int likes);

    boolean getIsLike();

    void setIsLike(boolean isLike);

    long getLikeUpdateTimestamp();

    void setLikeUpdateTimestamp(long likeUpdateTimestamp);
}
