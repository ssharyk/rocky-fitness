package ru.qbitmobile.rockyfit.contentmanager.module.network;

public interface NetworkCallbackFailure {

    int NO_DATA = -1000;
    int PARSE_ERROR = -2000;

    void onFailure(int httpResponseCode, String... customErrorCodes);
}
