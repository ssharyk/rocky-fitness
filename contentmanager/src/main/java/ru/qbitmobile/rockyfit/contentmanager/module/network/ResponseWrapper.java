package ru.qbitmobile.rockyfit.contentmanager.module.network;

public class ResponseWrapper<T> {
    NetworkCallbackSuccess<T> successCallback;
    NetworkCallbackFailure errorCallback;
    JsonDataProvider<T> parser;

    ResponseWrapper(NetworkCallbackSuccess<T> successCallback, NetworkCallbackFailure errorCallback, JsonDataProvider<T> parser) {
        this.successCallback = successCallback;
        this.errorCallback = errorCallback;
        this.parser = parser;
    }
}
