package ru.qbitmobile.rockyfit.contentmanager.module.network.parser;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.model.Training;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;
import ru.qbitmobile.rockyfit.contentmanager.module.network.JsonDataProvider;

import java.util.List;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.model.Training;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;
import ru.qbitmobile.rockyfit.contentmanager.module.network.model.Like;

/**
 * Обеспечивает парсеры для JSON-данных с сервера
 */
public interface IJsonProviderFactory extends Service {
    JsonDataProvider<List<Training>> getCategoriesListParser();

    JsonDataProvider<List<VideoLesson>> getVideosListParser();

    JsonDataProvider<Like> getLikeParser();
}
