package ru.qbitmobile.rockyfit.contentmanager.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.Generated;


@Entity(
        indexes = {
                @Index(value = "videoLessonId,tag", unique = true)
        }
)
public class TaggedVideo {
    @Id(autoincrement = true)
    Long id;

    Long videoLessonId;
    String tag;


    @Generated(hash = 1646864593)
    public TaggedVideo() {
    }

    @Generated(hash = 423623588)
    public TaggedVideo(Long id, Long videoLessonId, String tag) {
        this.id = id;
        this.videoLessonId = videoLessonId;
        this.tag = tag;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVideoLessonId() {
        return this.videoLessonId;
    }

    public void setVideoLessonId(Long videoLessonId) {
        this.videoLessonId = videoLessonId;
    }

    public String getTag() {
        return this.tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

}
