package ru.qbitmobile.rockyfit.contentmanager.module.network.parser

import org.json.JSONObject
import ru.qbitmobile.rockyfit.contentmanager.module.network.model.Like

object LikeFactory {
    fun parse(jLike: JSONObject): Like {
        return Like(
                jLike.optInt("likes"),
                jLike.optLong("dateLike"),
                jLike.optBoolean("isLike")
        )
    }
}