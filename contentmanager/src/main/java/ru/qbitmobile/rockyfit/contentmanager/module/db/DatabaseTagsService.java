package ru.qbitmobile.rockyfit.contentmanager.module.db;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.model.Tag;
import ru.qbitmobile.rockyfit.contentmanager.model.TaggedVideo;

import java.util.List;

public interface DatabaseTagsService extends Service {
    List<Tag> getAllTags();

    List<Tag> findTagsOfFreeVideos();
    List<Tag> findTagsByVideoId(long videoId);

    void insertTags(List<Tag> tags);
    void insertTags(Tag... tags);

    void insertTaggedVideos(TaggedVideo... taggedVideos);
    void insertTaggedVideos(List<TaggedVideo> taggedVideos);

    void clearTags();
}
