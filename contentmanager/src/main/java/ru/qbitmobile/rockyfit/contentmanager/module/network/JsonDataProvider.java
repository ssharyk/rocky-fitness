package ru.qbitmobile.rockyfit.contentmanager.module.network;

public interface JsonDataProvider<T> {
    ServerResponse<T> parse(String data);
}
