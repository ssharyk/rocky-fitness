package ru.qbitmobile.rockyfit.contentmanager.module.network;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.model.Training;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;

import java.util.List;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.model.Training;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;
import ru.qbitmobile.rockyfit.contentmanager.module.network.model.Like;

/**
 * Точка входа для сетевых операций
 */
public interface NetworkService extends Service {
    void loadTrainings(NetworkCallbackSuccess<List<Training>> onSuccess, NetworkCallbackFailure onError);

    void loadVideos(NetworkCallbackSuccess<List<VideoLesson>> onSuccess, NetworkCallbackFailure onError);

    void postLikeVideo(long videoId, boolean isSet, NetworkCallbackSuccess<Like> onSuccess, NetworkCallbackFailure onError);
}
