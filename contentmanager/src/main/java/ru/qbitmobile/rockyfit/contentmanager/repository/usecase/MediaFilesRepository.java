package ru.qbitmobile.rockyfit.contentmanager.repository.usecase;

import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.model.MediaFile;
import ru.qbitmobile.rockyfit.contentmanager.module.db.DatabaseService;
import ru.qbitmobile.rockyfit.contentmanager.repository.IMediaFilesRepository;

import java.util.List;

import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.model.MediaFile;

public class MediaFilesRepository implements IMediaFilesRepository {

    @Override
    public List<MediaFile> getFilesByTrainingId(long trainingId) {
        return DependencyGraph.get(DatabaseService.class).findMediaFilesByTrainingId(trainingId);
    }
}
