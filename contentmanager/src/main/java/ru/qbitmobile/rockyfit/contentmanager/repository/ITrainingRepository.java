package ru.qbitmobile.rockyfit.contentmanager.repository;

import androidx.lifecycle.LiveData;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.model.Training;
import ru.qbitmobile.rockyfit.contentmanager.repository.livedata.RepositoryData;

import java.util.List;

public interface ITrainingRepository extends Service {
    LiveData<RepositoryData<List<Training>>> getTrainings(long catalogId);

    Training getTrainingById(long trainingId);

    void updateTraining(Training training);
}
