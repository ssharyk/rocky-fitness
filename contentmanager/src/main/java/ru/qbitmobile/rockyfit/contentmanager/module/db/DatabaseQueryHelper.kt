package ru.qbitmobile.rockyfit.contentmanager.module.db

import org.greenrobot.greendao.Property
import org.greenrobot.greendao.query.WhereCondition

fun buildInConditionStrings(column: Property, ids: List<String>): WhereCondition {
    val wrappedIds = ids.map {
        var result = it
        if (!result.startsWith('\'')) result = "'$result"
        if (!result.endsWith('\'')) result = "$result'"
        result
    }
    return WhereCondition.StringCondition(column.columnName + " IN (" + wrappedIds.joinToString() + ")")
}


fun buildInCondition(column: Property, ids: List<Long>): WhereCondition {
    return WhereCondition.StringCondition(column.columnName + " IN (" + ids.joinToString() + ")")
}

fun <T> buildInCondition(column: Property, ids: Array<T>): WhereCondition {
    val tmp = StringBuilder()
    if (ids.isNotEmpty()) {
        val arrLen = ids.size
        for (i in 0 until arrLen - 1) {
            tmp.append(ids[i]).append(", ")
        }
        tmp.append(ids[arrLen - 1])
    }

    return WhereCondition.StringCondition(column.columnName + " IN (" + tmp.toString() + ")")
}


fun buildInCondition(column: Property, ids: Array<String>): WhereCondition {
    val tmp = StringBuilder()
    if (ids.isNotEmpty()) {
        val arrLen = ids.size
        for (i in 0 until arrLen - 1) {
            tmp.append(ids[i]).append(", ")
        }
        tmp.append(ids[arrLen - 1])
    }

    return WhereCondition.StringCondition(column.columnName + " IN (" + tmp.toString() + ")")
}