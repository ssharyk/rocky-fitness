package ru.qbitmobile.rockyfit.contentmanager.model;

import android.os.Parcelable;

public interface Lesson extends Parcelable {

    long getId();

    void setId(long id);

    long getCatalogId();

    void setCatalogId(long catalogId);

    String getName();

    void setName(String name);

    String getDescription();

    void setDescription(String description);

    String getDescriptionFull();

    void setDescriptionFull(String description);

    int getLessonsCount();

    void setLessonsCount(int lessonsCount);

    long getLessonsTotalDuration();

    void setLessonsTotalDuration(long lessonsTotalDuration);

    String getComplexityLevelTitle();

    Coach getCoach();

    void setPreviewImageId(long previewImageId);

    long getPreviewImageId();

    String getPreviewImageUri();

    int getLikes();

    void setLikes(int likes);

    boolean getIsLike();

    void setIsLike(boolean isLike);
}
