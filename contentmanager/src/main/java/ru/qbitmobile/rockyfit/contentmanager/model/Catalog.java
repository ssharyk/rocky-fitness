package ru.qbitmobile.rockyfit.contentmanager.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

@Entity
public class Catalog {

    @Id
    long id;

    @Property
    String name;

    @Property
    String alias;

    @Generated(hash = 1251128286)
    public Catalog(long id, String name, String alias) {
        this.id = id;
        this.name = name;
        this.alias = alias;
    }

    @Generated(hash = 861357461)
    public Catalog() {
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlias() {
        return this.alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
}
