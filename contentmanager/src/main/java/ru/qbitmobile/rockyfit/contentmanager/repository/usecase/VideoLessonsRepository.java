package ru.qbitmobile.rockyfit.contentmanager.repository.usecase;

import androidx.lifecycle.MutableLiveData;

import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.model.MediaFile;
import ru.qbitmobile.rockyfit.contentmanager.model.Tag;
import ru.qbitmobile.rockyfit.contentmanager.model.TaggedVideo;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;
import ru.qbitmobile.rockyfit.contentmanager.module.db.DatabaseService;
import ru.qbitmobile.rockyfit.contentmanager.module.network.NetworkService;
import ru.qbitmobile.rockyfit.contentmanager.module.sharedpreference.SharedPreferenceService;
import ru.qbitmobile.rockyfit.contentmanager.repository.ILikeRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.ITagRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.IVideoLessonsRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.livedata.DataSource;
import ru.qbitmobile.rockyfit.contentmanager.repository.livedata.RepositoryData;

import java.util.ArrayList;
import java.util.List;

public class VideoLessonsRepository implements IVideoLessonsRepository {
    private MutableLiveData<RepositoryData<List<VideoLesson>>> mAllVideoLessonsLiveData = new MutableLiveData<>();
    private MutableLiveData<RepositoryData<List<VideoLesson>>> mFreeVideoLessonsLiveData = new MutableLiveData<>();

    private boolean mCached;

    @Override
    public MutableLiveData<RepositoryData<List<VideoLesson>>> getVideoLessons(boolean freeOnly) {
        // разослать изменения наблюдателям (загрузка из БД)
        updateAndPost(freeOnly, DataSource.DATABASE);

        DependencyGraph.get(NetworkService.class).loadVideos(
                videoLessons -> {
                    // сохранить теги
                    // установить поля и свойства для записи в БД на основе распаршенных вложенных объектов
                    List<TaggedVideo> taggedVideos = new ArrayList<>();
                    List<MediaFile> lessonPreviews = new ArrayList<>();
                    List<Tag> tags = new ArrayList<>();
                    for (VideoLesson lesson : videoLessons) {
                        List<String> tagsAsString = lesson.getStringTags();
                        for (String tg : tagsAsString) {
                            Tag tag = new Tag(tg);
                            tags.add(tag);
                            taggedVideos.add(new TaggedVideo(null, lesson.getId(), tg));
                        }

                        MediaFile preview = lesson.getPreviewImage();
                        if (preview != null) {
                            lessonPreviews.add(preview);
                            lesson.setPreviewImageId(preview.getId());
                        }

                        // проверить и синхронизировать состояние лайка
                        long lastUpdatedLocal = DependencyGraph.get(SharedPreferenceService.class).sharedGetVideoLikeMarkTimestamp(lesson.getId());
                        if (lesson.getLikeUpdateTimestamp() < lastUpdatedLocal) {
                            boolean isSetLocal = DependencyGraph.get(SharedPreferenceService.class).sharedGetVideoLikeMark(lesson.getId());
                            if (isSetLocal != lesson.getIsLike()) {
                                // на сервере устарел - отправить
                                DependencyGraph.get(ILikeRepository.class).setLike(lesson, isSetLocal);
                                // пока не пришел ответ от установки/снятия лайка, пользуемся собственными данными
                                lesson.setIsLike(isSetLocal);
                            }
                        } else {
                            // локально устарел - будет обновлен при записи в БД, больше ничего делать не надо
                        }
                    }

                    DependencyGraph.get(DatabaseService.class).clearVideoLessons();
                    DependencyGraph.get(DatabaseService.class).insertVideoLessons(videoLessons);

                    DependencyGraph.get(ITagRepository.class).updateTagsWithNetwork(tags);
                    DependencyGraph.get(DatabaseService.class).insertTaggedVideos(taggedVideos);
                    DependencyGraph.get(DatabaseService.class).insertMediaFiles(lessonPreviews);

                    // обновить данные в БД и отправить актуальные значения подписчикам
                    updateAndPost(freeOnly, DataSource.NETWORK);
                    mCached = true;
                },
                null);

        return freeOnly ? mFreeVideoLessonsLiveData : mAllVideoLessonsLiveData;
    }

    private void updateAndPost(boolean freeOnly, DataSource originSource) {
        DataSource dataSource = (originSource == DataSource.DATABASE && mCached) ? DataSource.CACHE : originSource;
        if (freeOnly) {
            List<VideoLesson> cached = DependencyGraph.get(DatabaseService.class).getFreeVideoLessons();
            /// TODO: find items were bought
            mFreeVideoLessonsLiveData.setValue(new RepositoryData<>(cached, dataSource));
        } else {
            List<VideoLesson> cached = DependencyGraph.get(DatabaseService.class).getAllVideoLessons();
            mAllVideoLessonsLiveData.setValue(new RepositoryData<>(cached, dataSource));
        }
    }

    @Override
    public List<VideoLesson> getVideoLessons(List<Tag> tags, boolean freeOnly) {
        List<String> tagIds = new ArrayList<>();
        for (Tag tag : tags) tagIds.add(tag.getName());
        return DependencyGraph.get(DatabaseService.class).findVideoLessonByTags(tagIds, freeOnly);
    }

    @Override
    public VideoLesson getVideoLessonById(long lessonId) {
        return DependencyGraph.get(DatabaseService.class).findVideoLessonById(lessonId);
    }

    @Override
    public List<VideoLesson> getVideoLessonsByTrainingId(long trainingId) {
        return DependencyGraph.get(DatabaseService.class).findVideoLessonByTrainingId(trainingId);
    }

    @Override
    public VideoLesson getFirstVideoOfTraining(long trainingId) {
        List<VideoLesson> allLessonsInTraining = DependencyGraph.get(DatabaseService.class).findVideoLessonByTrainingId(trainingId);
        if (allLessonsInTraining == null || allLessonsInTraining.size() == 0) {
            return null;
        }
        return allLessonsInTraining.get(0);
    }

    @Override
    public void update(VideoLesson videoLesson) {
        DependencyGraph.get(DatabaseService.class).insertVideoLessons(videoLesson);
    }

    @Override
    public void updateAfterTrainings() {
        updateAndPost(false, DataSource.DATABASE);
        updateAndPost(true, DataSource.DATABASE);
    }
}
