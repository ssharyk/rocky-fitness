package ru.qbitmobile.rockyfit.contentmanager.module.sharedpreference;

import android.content.Context;
import android.content.SharedPreferences;

import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.module.contextholder.IContextHolder;

public class SharedPreferenceModule implements SharedPreferenceService {

    private static final String PREFERENCES_NAME = "rocky_sp";

    private SharedPreferences getPreferences() {
        return DependencyGraph.get(IContextHolder.class).getContext().getSharedPreferences(
                PREFERENCES_NAME, Context.MODE_PRIVATE
        );
    }

    @Override
    public void dropAllRecords() {
        getPreferences().edit().clear().apply();
    }

    @Override
    public void sharedSetDeviceId(String uniqueId) {
        getPreferences().edit().putString(AppStorage.DEVICE_ID.name(), uniqueId).apply();
    }

    @Override
    public String sharedGetDeviceId() {
        return getPreferences().getString(AppStorage.DEVICE_ID.name(), null);
    }

    @Override
    public void sharedSetVideoLikeMark(long videoId, boolean isLike) {
        getPreferences().edit().putBoolean(AppStorage.VIDEO_LIKE_IS_SET.name() + "_" + videoId, isLike).apply();
    }

    @Override
    public boolean sharedGetVideoLikeMark(long videoId) {
        return getPreferences().getBoolean(AppStorage.VIDEO_LIKE_IS_SET.name() + "_" + videoId, false);
    }

    @Override
    public void sharedSetVideoLikeMarkTimestamp(long videoId, long timestamp) {
        getPreferences().edit().putLong(AppStorage.VIDEO_LIKE_TIMESTAMP.name() + "_" + videoId, timestamp).apply();
    }

    @Override
    public long sharedGetVideoLikeMarkTimestamp(long videoId) {
        return getPreferences().getLong(AppStorage.VIDEO_LIKE_TIMESTAMP.name() + "_" + videoId, 0L);
    }

    @Override
    public void sharedSetHasSubscription(boolean hasSubscription) {
        getPreferences().edit().putBoolean(AppStorage.HAS_SUBSCRIPTION.name(), hasSubscription).apply();
    }

    @Override
    public boolean sharedGetHasSubscription() {
        return getPreferences().getBoolean(AppStorage.HAS_SUBSCRIPTION.name(), false);
    }
}
