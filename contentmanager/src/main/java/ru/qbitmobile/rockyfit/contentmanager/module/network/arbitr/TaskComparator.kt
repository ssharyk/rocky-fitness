package ru.qbitmobile.rockyfit.contentmanager.module.network.arbitr

/**
 * Упорядочивание задач в приоритетной очереди по их приоритету, затем - по времени создания
 */
internal class TaskComparator : Comparator<Task> {
    override fun compare(lhs: Task?, rhs: Task?): Int {
        return if (lhs != null && rhs != null)
            if (lhs.priority == rhs.priority) {
                lhs.createTimestamp.compareTo(rhs.createTimestamp)
            } else {
                rhs.priority.compareTo(lhs.priority)
            }
        else 1
    }
}