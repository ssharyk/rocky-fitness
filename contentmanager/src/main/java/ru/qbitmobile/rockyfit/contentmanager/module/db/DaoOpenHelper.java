package ru.qbitmobile.rockyfit.contentmanager.module.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.greenrobot.greendao.database.Database;

import ru.qbitmobile.rockyfit.contentmanager.model.DaoMaster;

import static ru.qbitmobile.rockyfit.contentmanager.model.DaoMaster.dropAllTables;

/**
 * Создает БД и управляет миграциями
 */
public class DaoOpenHelper extends DaoMaster.OpenHelper {

    public DaoOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory) {
        super(context, name, factory);
    }

    public DaoOpenHelper(Context context, String name) {
        super(context, name);
    }

    @Override
    public void onUpgrade(Database db, int oldVersion, int newVersion) {
        Log.i("greenDAO", "Upgrading schema from version " + oldVersion + " to " + newVersion + " by dropping all tables");

        migration(db, oldVersion, newVersion);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }

    private void migration(Database db, int oldVersion, int newVersion) {
        Log.d("greenDAO", "migrate from old ver. - " + oldVersion  +" to new ver. - " + newVersion);
        switch (oldVersion) {
            default:
                dropAllTables(db, true);
                onCreate(db);
                break;
        }
    }
}
