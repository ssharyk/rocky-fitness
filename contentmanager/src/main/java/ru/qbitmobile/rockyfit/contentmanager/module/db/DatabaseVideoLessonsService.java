package ru.qbitmobile.rockyfit.contentmanager.module.db;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;

import java.util.List;

interface DatabaseVideoLessonsService extends Service {

    List<VideoLesson> getAllVideoLessons();

    List<VideoLesson> getFreeVideoLessons();

    void insertVideoLessons(List<VideoLesson> lessons);

    void insertVideoLessons(VideoLesson... lessons);

    VideoLesson findVideoLessonById(long lessonId);

    List<VideoLesson> findVideoLessonByTags(List<String> tags, boolean freeOnly);

    List<VideoLesson> findVideoLessonByTrainingId(long trainingId);

    void clearVideoLessons();
}
