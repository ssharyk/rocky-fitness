package ru.qbitmobile.rockyfit.contentmanager.module.configurator;

import java.util.List;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;

/**
 * Задает параметры, зависящие от окружения, в котором выполняется код
 */
public interface ConfigurationService extends Service {
    String getServerDomain();

    List<VideoLesson> shuffle(List<VideoLesson> origin);
}
