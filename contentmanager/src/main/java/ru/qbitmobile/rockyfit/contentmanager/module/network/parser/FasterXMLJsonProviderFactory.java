/*
package ru.qbitmobile.rockyfit.contentmanager.module.network.parser;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import ru.qbitmobile.rockyfit.contentmanager.model.Training;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;
import ru.qbitmobile.rockyfit.contentmanager.module.network.JsonDataProvider;
import ru.qbitmobile.rockyfit.contentmanager.module.network.ServerResponse;

import java.io.IOException;
import java.util.List;

public class FasterXMLJsonProviderFactory implements IJsonProviderFactory {
    private static ObjectMapper sCategoriesMapper;

    static synchronized ObjectMapper getMapper() {
        if (sCategoriesMapper == null) {
            sCategoriesMapper = new ObjectMapper();
        }
        return sCategoriesMapper;
    }


    @Override
    public JsonDataProvider<List<Training>> getCategoriesListParser() {
        return new JsonDataProvider<List<Training>>() {
            @Override
            public ServerResponse<List<Training>> parse(String data) {
                try {
                    return getMapper().readValue(data, new TypeReference<ServerResponse<List<Training>>>() {
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
    }

    @Override
    public JsonDataProvider<List<VideoLesson>> getVideosListParser() {
        return new JsonDataProvider<List<VideoLesson>>() {
            @Override
            public ServerResponse<List<VideoLesson>> parse(String data) {
                try {
                    return getMapper().readValue(data, new TypeReference<ServerResponse<List<VideoLesson>>>() {
                    });
                } catch (IOException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        };
    }
}

*/