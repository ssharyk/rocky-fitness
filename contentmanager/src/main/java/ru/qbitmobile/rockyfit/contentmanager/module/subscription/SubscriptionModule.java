package ru.qbitmobile.rockyfit.contentmanager.module.subscription;

import android.content.Context;

import ru.qbitmobile.rockyfit.billing.BillingCallback;
import ru.qbitmobile.rockyfit.billing.BillingLauncher;
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.module.contextholder.IContextHolder;
import ru.qbitmobile.rockyfit.contentmanager.module.sharedpreference.SharedPreferenceService;

public class SubscriptionModule implements SubscriptionService, BillingCallback {

    private BillingLauncher mLauncher = new BillingLauncher(this);
    private boolean mSubscriptionPresent;
    private Runnable mOnSuccess;

    @Override
    public void subscriptionInit() {
        mSubscriptionPresent = DependencyGraph.get(SharedPreferenceService.class).sharedGetHasSubscription();
        mLauncher.initBilling(DependencyGraph.get(IContextHolder.class).getContext());
    }

    @Override
    public boolean subscriptionPresent() {
        return mSubscriptionPresent;
    }

    @Override
    public void subscriptionShowDialog(Runnable onSuccess) {
        mOnSuccess = onSuccess;
        Context context = DependencyGraph.get(IContextHolder.class).getContext();
        mLauncher.showDialog(context);
    }

    @Override
    public void onPurchaseCompleted() {
        mSubscriptionPresent = true;
        if (mOnSuccess != null) {
            mOnSuccess.run();
        }
    }

    @Override
    public void onPurchasePresentChanged(boolean hasSubscription) {
        mSubscriptionPresent = hasSubscription;
        DependencyGraph.get(SharedPreferenceService.class).sharedSetHasSubscription(hasSubscription);
    }
}
