package ru.qbitmobile.rockyfit.contentmanager.module.network.connection;

public interface InternetConnectivityListener {

    void onInternetConnectivityChanged(boolean isConnected);
}