package ru.qbitmobile.rockyfit.contentmanager.module.network;

import ru.qbitmobile.rockyfit.commoncomponent.helper.Hasher;
import ru.qbitmobile.rockyfit.commoncomponent.helper.Now;
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.model.Training;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;
import ru.qbitmobile.rockyfit.contentmanager.module.configurator.ConfigurationService;
import ru.qbitmobile.rockyfit.contentmanager.module.network.arbitr.Task;
import ru.qbitmobile.rockyfit.contentmanager.module.network.arbitr.TaskTag;
import ru.qbitmobile.rockyfit.contentmanager.module.network.model.Like;
import ru.qbitmobile.rockyfit.contentmanager.module.network.networktask.NetworkTaskService;
import ru.qbitmobile.rockyfit.contentmanager.module.network.parser.IJsonProviderFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Реализует создание сетевых операций.
 * Выполняет создание задачи, передачу ее в арбитр (если это не было сделано ранее)
 * Реагирует на результат выполнения задачи
 */
public class NetworkModule implements NetworkService {

    private static final String API_LOAD_ALL_CATEGORIES = "/api/categories";
    private static final String API_LOAD_ALL_VIDEOS = "/api/videos";
    private static final String API_LOAD_SET_VIDEO_LIKE = "/api/likes/";

    private static final long LIKE_PRIORITY = 1;
    private static final long LOAD_DATA_PRIORITY = 2;

    @Override
    public void loadTrainings(NetworkCallbackSuccess<List<Training>> onSuccess, NetworkCallbackFailure onError) {

        int taskAddress = Hasher.hash(API_LOAD_ALL_CATEGORIES);

        JsonDataProvider<List<Training>> parser = DependencyGraph.get(IJsonProviderFactory.class).getCategoriesListParser();
        ResponseWrapper<List<Training>> wrapper = new ResponseWrapper<>(
                DependencyGraph.get(NetworkTaskService.class).taskCreateSuccessCallback(taskAddress, onSuccess),
                DependencyGraph.get(NetworkTaskService.class).taskCreateErrorCallback(taskAddress, onError),
                parser);

        DependencyGraph.get(NetworkTaskService.class).wrapActiveTask(new Task(() -> {
            VolleyRequestController.addToRequestQueue(
                    VolleyRequestController.buildGetRequest(buildUri(API_LOAD_ALL_CATEGORIES), wrapper)
            );
            return null;
        }, LOAD_DATA_PRIORITY, taskAddress, API_LOAD_ALL_CATEGORIES, new TaskTag.GetDataTaskTag()));
    }

    @Override
    public void loadVideos(NetworkCallbackSuccess<List<VideoLesson>> onSuccess, NetworkCallbackFailure onError) {

        int taskAddress = Hasher.hash(API_LOAD_ALL_VIDEOS);
        JsonDataProvider<List<VideoLesson>> parser = DependencyGraph.get(IJsonProviderFactory.class).getVideosListParser();
        ResponseWrapper<List<VideoLesson>> wrapper = new ResponseWrapper<>(
                DependencyGraph.get(NetworkTaskService.class).taskCreateSuccessCallback(taskAddress, onSuccess),
                DependencyGraph.get(NetworkTaskService.class).taskCreateErrorCallback(taskAddress, onError),
                parser);

        DependencyGraph.get(NetworkTaskService.class).wrapActiveTask(new Task(() -> {
            VolleyRequestController.addToRequestQueue(
                    VolleyRequestController.buildGetRequest(buildUri(API_LOAD_ALL_VIDEOS), wrapper)
            );
            return null;
        }, LOAD_DATA_PRIORITY, taskAddress, API_LOAD_ALL_VIDEOS, new TaskTag.GetDataTaskTag()));
    }

    @Override
    public void postLikeVideo(long videoId, boolean isSet, NetworkCallbackSuccess<Like> onSuccess, NetworkCallbackFailure onError) {
        List<Object> args = new ArrayList<>();
        args.add(API_LOAD_SET_VIDEO_LIKE);
        args.add(videoId);
        args.add(isSet);
        int taskAddress = Hasher.hash(args);

        JsonDataProvider<Like> parser = DependencyGraph.get(IJsonProviderFactory.class).getLikeParser();
        ResponseWrapper<Like> wrapper = new ResponseWrapper<>(
                DependencyGraph.get(NetworkTaskService.class).taskCreateSuccessCallback(taskAddress, onSuccess),
                DependencyGraph.get(NetworkTaskService.class).taskCreateErrorCallback(taskAddress, onError),
                parser);

        DependencyGraph.get(NetworkTaskService.class).wrapActiveTask(new Task(() -> {
            VolleyRequestController.addToRequestQueue(
                    VolleyRequestController.buildPostRequest(buildUri(API_LOAD_SET_VIDEO_LIKE + videoId), wrapper)
            );
            return null;
        }, LOAD_DATA_PRIORITY, taskAddress, API_LOAD_SET_VIDEO_LIKE, new TaskTag.LikeTaskTag(videoId)));
    }

    private String buildUri(String apiName) {
        return DependencyGraph.get(ConfigurationService.class).getServerDomain() + apiName;
    }
}
