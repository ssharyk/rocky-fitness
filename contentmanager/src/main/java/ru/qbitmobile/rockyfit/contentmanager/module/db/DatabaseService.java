package ru.qbitmobile.rockyfit.contentmanager.module.db;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;

public interface DatabaseService extends
        DatabaseTrainingsService,
        DatabaseVideoLessonsService,
        DatabaseCoachesService,
        DatabaseTagsService,
        DatabaseComplexityService,
        DatabaseMediaFilesService,
        DatabaseCatalogsService,
        Service {

    void dropAllTables();

}
