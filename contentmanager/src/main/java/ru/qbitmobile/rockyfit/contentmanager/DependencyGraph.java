package ru.qbitmobile.rockyfit.contentmanager;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.module.contextholder.ContextHolder;
import ru.qbitmobile.rockyfit.contentmanager.module.contextholder.IContextHolder;
import ru.qbitmobile.rockyfit.contentmanager.module.network.parser.IJsonProviderFactory;
import ru.qbitmobile.rockyfit.contentmanager.module.network.parser.PlainJsonProviderFactory;
import ru.qbitmobile.rockyfit.contentmanager.module.sharedpreference.SharedPreferenceModule;
import ru.qbitmobile.rockyfit.contentmanager.module.sharedpreference.SharedPreferenceService;
import ru.qbitmobile.rockyfit.contentmanager.module.subscription.SubscriptionModule;
import ru.qbitmobile.rockyfit.contentmanager.module.subscription.SubscriptionService;
import ru.qbitmobile.rockyfit.contentmanager.repository.ICoachRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.ILikeRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.IMediaFilesRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.ITagRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.ITrainingRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.IVideoLessonsRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.usecase.CoachRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.usecase.LikeRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.usecase.MediaFilesRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.usecase.TagRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.usecase.TrainingRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.usecase.VideoLessonsRepository;

import java.util.HashMap;
import java.util.Map;

/**
 * DI-контейнер
 */
public final class DependencyGraph {
    private static boolean installed = false;

    private static Map<Class<? extends Service>, Service> installedServices = new HashMap<>();

    public static void init() {
        if (installed) return;

        synchronized (DependencyGraph.class) {
            if (installed) {
                return;
            }

            install(ITrainingRepository.class, new TrainingRepository());
            install(IMediaFilesRepository.class, new MediaFilesRepository());
            install(ICoachRepository.class, new CoachRepository());
            install(ITagRepository.class, new TagRepository());
            install(IVideoLessonsRepository.class, new VideoLessonsRepository());
            install(ILikeRepository.class, new LikeRepository());

            install(IContextHolder.class, new ContextHolder());
            install(SharedPreferenceService.class, new SharedPreferenceModule());
            install(SubscriptionService.class, new SubscriptionModule());
            install(IJsonProviderFactory.class, new PlainJsonProviderFactory());

            installed = true;
        }
    }

    /**
     * Добавляет зависимость в граф
     *
     * @param klass   Класс зависимости
     * @param service Реализация
     */
    public static void install(Class<? extends Service> klass, Service service) {
        Class[] interfaces = service.getClass().getInterfaces();
        boolean hasInterface = false;
        for (int i = 0; i < interfaces.length; i++) {
            if (interfaces[i].equals(klass)) {
                hasInterface = true;
                break;
            }
        }
        if (hasInterface) {
            installedServices.put(klass, service);
        } else {
            throw new IllegalStateException("Service " + service.getClass().getName() + " does not implement expecting " + klass.getName());
        }
    }

    /**
     * Получает реализацию зависимости
     *
     * @param klass Класс зависимости
     */
    public static <T extends Service> T get(Class<T> klass) {
        if (!installed) init();

        if (installedServices.containsKey(klass)) {
            return (T) installedServices.get(klass);
        }
        return null;
    }
}
