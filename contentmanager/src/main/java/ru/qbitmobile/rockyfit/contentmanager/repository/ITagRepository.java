package ru.qbitmobile.rockyfit.contentmanager.repository;

import androidx.lifecycle.LiveData;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.model.Tag;

import java.util.List;

import ru.qbitmobile.rockyfit.contentmanager.repository.livedata.RepositoryData;

public interface ITagRepository extends Service {
    LiveData<RepositoryData<List<Tag>>> getTags();

    List<Tag> getTagsByCourseId(long courseId);

    List<Tag> getTagsByTrainingId(long trainingId);

    List<Tag> getTagsByVideoLessonId(long lessonId);

    void updateTagsWithNetwork(List<Tag> tags);
}
