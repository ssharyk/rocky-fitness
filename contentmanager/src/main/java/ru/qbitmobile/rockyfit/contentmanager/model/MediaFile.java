package ru.qbitmobile.rockyfit.contentmanager.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

@Entity
public class MediaFile {

    // region Properties

    @Id
    long id;

    @Property
    int width;

    @Property
    int height;

    @Property
    String uri;

    @Property
    String name;

    // endregion

    // region Constructors

    public MediaFile() {
    }

    @Generated(hash = 791329704)
    public MediaFile(long id, int width, int height, String uri, String name) {
        this.id = id;
        this.width = width;
        this.height = height;
        this.uri = uri;
        this.name = name;
    }

    public static MediaFile loremIpsum(long id, int width, int height, long trainingId) {
        String uri = "https://picsum.photos/id/" + id + "/" + width + "/" + height;
        return new MediaFile(id, width, height, uri, String.valueOf(id));
    }

    // endregion

    // region Get/set

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // endregion
}
