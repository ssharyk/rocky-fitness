package ru.qbitmobile.rockyfit.contentmanager.repository.fake

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.qbitmobile.rockyfit.contentmanager.model.Tag
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson
import ru.qbitmobile.rockyfit.contentmanager.repository.IVideoLessonsRepository
import ru.qbitmobile.rockyfit.contentmanager.repository.livedata.DataSource
import ru.qbitmobile.rockyfit.contentmanager.repository.livedata.RepositoryData

class FakeVideoLessonsRepository : IVideoLessonsRepository {
    companion object {
        private val VIDEO_LESSONS = mutableListOf<VideoLesson>(
                VideoLesson(1L, 1L, 1, 100_000L, "Video#1", FULL_DESCRIPTION.random(200), buildPreviewId(1L), 200, 300, buildVideoUri(1L), 1, false, 0L, true),
                VideoLesson(2L, 1L, 2, 100_000L, "Video#2", FULL_DESCRIPTION.random(200), buildPreviewId(2L), 200, 300, buildVideoUri(2L), 2, false, 0L, true),
                VideoLesson(3L, 1L, 3, 100_000L, "Video#3", FULL_DESCRIPTION.random(200), buildPreviewId(3L), 200, 300, buildVideoUri(3L), 3, false, 0L, true),
                VideoLesson(4L, 1L, 4, 100_000L, "Video#4", FULL_DESCRIPTION.random(200), buildPreviewId(4L), 200, 300, buildVideoUri(4L), 4, false, 0L, true),
                VideoLesson(5L, 1L, 5, 100_000L, "Video#5", FULL_DESCRIPTION.random(200), buildPreviewId(5L), 200, 300, buildVideoUri(5L), 5, false, 0L, true),
                VideoLesson(6L, 1L, 6, 100_000L, "Video#6", FULL_DESCRIPTION.random(200), buildPreviewId(6L), 200, 300, buildVideoUri(6L), 6, false, 0L, true),
                VideoLesson(7L, 1L, 7, 100_000L, "Video#7", FULL_DESCRIPTION.random(200), buildPreviewId(7L), 200, 300, buildVideoUri(7L), 7, false, 0L, true),
                VideoLesson(8L, 1L, 8, 100_000L, "Video#8", FULL_DESCRIPTION.random(200), buildPreviewId(8L), 200, 300, buildVideoUri(8L), 8, false, 0L, true),
                VideoLesson(9L, 1L, 9, 100_000L, "Video#9", FULL_DESCRIPTION.random(200), buildPreviewId(9L), 200, 300, buildVideoUri(9L), 9, false, 0L, true),
                VideoLesson(10L, 1L, 10, 100_000L, "Video#10", FULL_DESCRIPTION.random(200), buildPreviewId(10L), 200, 300, buildVideoUri(10L), 10, false, 0L, true)
        )
    }

    override fun getVideoLessons(freeOnly: Boolean): LiveData<RepositoryData<MutableList<VideoLesson>>>? {
        val liveData = MutableLiveData<RepositoryData<MutableList<VideoLesson>>>()
        liveData.value = RepositoryData(VIDEO_LESSONS, DataSource.DATABASE)
        return liveData
    }

    override fun getVideoLessons(tags: List<Tag>, freeOnly: Boolean): List<VideoLesson> {
        return VIDEO_LESSONS.filter {
            it.tags.containsAll(tags) && (!freeOnly || it.isUnblocked)
        }
    }

    override fun getVideoLessonById(lessonId: Long): VideoLesson? {
        return VIDEO_LESSONS.firstOrNull { it.id == lessonId }
    }

    override fun getFirstVideoOfTraining(trainingId: Long): VideoLesson? {
        return VIDEO_LESSONS.firstOrNull { it.trainingId == trainingId && it.step == 1 }
    }

    override fun getVideoLessonsByTrainingId(trainingId: Long): MutableList<VideoLesson> {
        return VIDEO_LESSONS.filter { it.trainingId == trainingId }.toMutableList()
    }

    override fun update(videoLesson: VideoLesson) {
        val index = VIDEO_LESSONS.indexOfFirst { it.id == videoLesson.id }
        if (index == -1) {
            VIDEO_LESSONS.add(videoLesson)
        } else {
            VIDEO_LESSONS[index] = videoLesson
        }
    }

    override fun updateAfterTrainings() {
    }
}