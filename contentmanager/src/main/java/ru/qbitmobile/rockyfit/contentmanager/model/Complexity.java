package ru.qbitmobile.rockyfit.contentmanager.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class Complexity {

    @Id
    long id;

    @Property
    String name;

    @Generated(hash = 1725096176)
    public Complexity(long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Generated(hash = 1883358080)
    public Complexity() {
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
