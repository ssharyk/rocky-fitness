package ru.qbitmobile.rockyfit.contentmanager.repository.fake

import java.lang.Exception
import kotlin.random.Random


const val FULL_DESCRIPTION = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec nisl ipsum, aliquet vel accumsan ac, hendrerit a massa. Donec nec neque convallis, tincidunt metus nec, bibendum nisi. Nam ut lectus ut urna sodales fringilla vel in lectus. Nullam nec odio at erat egestas efficitur. Quisque auctor justo ut pretium placerat. Aenean mauris magna, sagittis ac dapibus ac, hendrerit a nisl. Suspendisse potenti. Donec sed porttitor elit, id laoreet ex. Mauris velit ipsum, feugiat at semper ac, blandit eget velit.\n" +
        "Nunc consequat semper mauris sed faucibus. Suspendisse convallis dictum rhoncus. Vestibulum metus velit, egestas sed lectus ac, tempus aliquet diam. Aliquam sit amet fringilla lorem. Nam diam tellus, blandit id justo non, malesuada viverra est. Praesent mollis posuere arcu, sit amet auctor lectus luctus ac. Ut eu felis vel mauris lobortis cursus mattis ut lorem. Mauris vulputate sapien et tincidunt sodales. Donec auctor iaculis ligula. Vivamus convallis velit vel dolor porttitor laoreet. Sed quis neque magna."


fun buildPreviewId(id: Long): Long {
    return 100 + 3 * id
}

fun buildPreviewUri(id: Long, width: Int = 200, height: Int = 200): String? {
    return "https://picsum.photos/id/${buildPreviewId(id)}/$width/$height"
}

fun buildVideoUri(id: Long): String? {
    return when (id) {
        0L -> "https://developers.google.com/training/images/tacoma_narrows.mp4";
        2L -> "https://download.videvo.net/videvo_files/video/free/2019-04/small_watermarked/190312_28_StadioOlimpicoAndFans_Drone_002_preview.webm?v=1097539"
        3L -> "https://www.videvo.net/videvo_files/converted/2015_04/preview/Ocean_Waves_slow_motion_videvo.mov44965.webm?v=1097539"
        else -> "https://www.videvo.net/videvo_files/converted/2013_09/preview/VUMetersGreenVidevo.mov18561.webm?v=1097539"
    }
}

fun String.random(count: Int): String {
    if (this.length < count) {
        return this
    }

    return try {
        val start = Random(0L).nextInt(length - count)
        substring(start, count)
    } catch (e: Exception) {
        this
    }
}

fun String.words(count: Int): String {
    if (count <= 0) return ""
    val words = this.split(' ')
    return words.take(count).joinToString(separator = " ")
}

fun <T> Collection<T>.containsAny(vararg items: T): Boolean {
    for (x in this) {
        if (items.contains(x)) return true;
    }
    return false
}