package ru.qbitmobile.rockyfit.contentmanager.module.db;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.model.Training;

import java.util.List;

interface DatabaseTrainingsService extends Service {

    List<Training> getAllTrainings();
    List<Training> getAllTrainingsByCatalogId(long catalogId);

    void insertTrainings(List<Training> trainings);

    void insertTrainings(Training... trainings);

    Training findTrainingById(long trainingId);

    void clearTrainings();
}
