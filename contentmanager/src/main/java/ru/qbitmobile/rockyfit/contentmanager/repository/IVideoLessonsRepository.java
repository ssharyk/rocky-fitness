package ru.qbitmobile.rockyfit.contentmanager.repository;

import androidx.lifecycle.LiveData;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.model.Tag;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;

import java.util.List;

import ru.qbitmobile.rockyfit.contentmanager.repository.livedata.RepositoryData;

public interface IVideoLessonsRepository extends Service {
    LiveData<RepositoryData<List<VideoLesson>>> getVideoLessons(boolean freeOnly);

    List<VideoLesson> getVideoLessons(List<Tag> tags, boolean freeOnly);

    List<VideoLesson> getVideoLessonsByTrainingId(long trainingId);

    VideoLesson getVideoLessonById(long lessonId);

    VideoLesson getFirstVideoOfTraining(long trainingId);

    void update(VideoLesson videoLesson);

    void updateAfterTrainings();
}
