package ru.qbitmobile.rockyfit.contentmanager.module.network.arbitr

import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph
import ru.qbitmobile.rockyfit.contentmanager.module.network.connection.ConnectionService
import ru.qbitmobile.rockyfit.contentmanager.module.network.connection.InternetConnectivityListener
import java.util.*
import java.util.concurrent.PriorityBlockingQueue
import kotlin.collections.ArrayList

/**
 * Арбитр сетевых операций
 */
object NetworkArbitr : InternetConnectivityListener {

    private val activeCoreAmount: Int

    init {
        val cores = Runtime.getRuntime().availableProcessors() * 3 / 4
        activeCoreAmount = if (cores == 0) 1 else cores
    }

    val activeTasks: MutableList<Task> = Collections.synchronizedList(ArrayList(activeCoreAmount))

    @Volatile
    private var networkActiveQueue = PriorityBlockingQueue(10, TaskComparator())


    /**
     * Добавление задачи в очерель сетевых задач
     */
    fun addActiveTask(networkRequest: Task) {
        if (networkActiveQueue.size == 0) {
            networkRequest.onTaskCompleteListener = createActiveTaskCompleteListener(networkRequest)
            networkActiveQueue.offer(networkRequest)
            onActiveTaskComplete()
        } else {
            networkRequest.onTaskCompleteListener = createActiveTaskCompleteListener(networkRequest)
            networkActiveQueue.offer(networkRequest)
            onActiveTaskComplete()
        }
    }


    /**
     * Абстракция уровнем повыше, чтобы ловить успех/неудачу в арбитре
     */
    private fun createActiveTaskCompleteListener(networkRequest: Task): OnTaskCompleteListener {
        return object : OnTaskCompleteListener {
            override fun onComplete() {
                activeTasks.remove(networkRequest)
                onActiveTaskComplete()
            }

            override fun onFailed(isUserError: Boolean) {
                activeTasks.remove(networkRequest)
                if (!isUserError) {
                    networkActiveQueue.offer(networkRequest)
                }
                onActiveTaskComplete()
            }
        }
    }

    /**
     * Должен быть вызван когда закончился таск и/или необходимо взять новый
     */
    fun onActiveTaskComplete() {
        while (activeUnlocked() &&
                networkActiveQueue.size != 0 &&
                DependencyGraph.get(ConnectionService::class.java).connectionStatus == ConnectionService.CONNECTED) {
            val task = networkActiveQueue.poll()
            task?.let {
                activeTasks.add(it)
                it.run()
            }
        }
    }


    /**
     * Проверяет, выполнены ли условия для взятия следующей задачи
     */
    private fun activeUnlocked(): Boolean {
        return activeTasks.size <= activeCoreAmount
    }

    /**
     * Обработчик ответа по умолчанию, если произошла внутренняя ошибка
     */
    fun nullActiveCallback(taskAddress: Int) {
        networkActiveQueue.removeAll { it.taskAddress == taskAddress }
        activeTasks.removeAll { it.taskAddress == taskAddress }
    }


    override fun onInternetConnectivityChanged(isConnected: Boolean) {
        if (isConnected) {
            onActiveTaskComplete()
        }
    }
}