package ru.qbitmobile.rockyfit.contentmanager.repository;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;

public interface ILikeRepository extends Service {

    void setLike(VideoLesson lesson, boolean isSet);
}
