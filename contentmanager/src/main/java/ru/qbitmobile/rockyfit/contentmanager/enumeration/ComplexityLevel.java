package ru.qbitmobile.rockyfit.contentmanager.enumeration;

import androidx.annotation.IntDef;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Target({ElementType.PARAMETER, ElementType.FIELD, ElementType.TYPE_USE})
@IntDef({ComplexityLevel.EASY, ComplexityLevel.MEDIUM, ComplexityLevel.HARD})
@Retention(RetentionPolicy.SOURCE)
public @interface ComplexityLevel {
    int EASY = 1;
    int MEDIUM = 2;
    int HARD = 3;
}
