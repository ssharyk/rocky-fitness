package ru.qbitmobile.rockyfit.contentmanager.module.network.parser

import org.json.JSONArray
import org.json.JSONObject

fun JSONObject.optNestedJSONObject(name: String, fallback: JSONObject = JSONObject()): JSONObject {
    return this.optJSONObject(name) ?: fallback
}

fun JSONObject.optNestedJSONArray(name: String, fallback: JSONArray = JSONArray()): JSONArray {
    return this.optJSONArray(name) ?: fallback
}