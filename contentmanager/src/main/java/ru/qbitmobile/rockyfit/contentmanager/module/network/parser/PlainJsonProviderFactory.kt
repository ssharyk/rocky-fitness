package ru.qbitmobile.rockyfit.contentmanager.module.network.parser

import ru.qbitmobile.rockyfit.contentmanager.model.Training
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson
import ru.qbitmobile.rockyfit.contentmanager.module.network.JsonDataProvider
import ru.qbitmobile.rockyfit.contentmanager.module.network.ServerResponse
import org.json.JSONObject
import ru.qbitmobile.rockyfit.contentmanager.module.network.model.Like

/**
 * Самостоятельный парс JSON
 */
class PlainJsonProviderFactory : IJsonProviderFactory {
    override fun getCategoriesListParser(): JsonDataProvider<List<Training>> {
        return object : JsonDataProvider<List<Training>> {
            override fun parse(data: String?): ServerResponse<List<Training>> {
                if (data == null) return ServerResponse<List<Training>>(200, "Ok", null, emptyList(), 0)

                val jData = JSONObject(data)
                val parsedCategories = CategoriesFactory.parse(jData.optJSONArray("data"))
                return ServerResponseFactory.parse(jData, parsedCategories)
            }
        }
    }

    override fun getVideosListParser(): JsonDataProvider<List<VideoLesson>> {
        return object : JsonDataProvider<List<VideoLesson>> {
            override fun parse(data: String?): ServerResponse<List<VideoLesson>> {
                if (data == null) return ServerResponse<List<VideoLesson>>(200, "Ok", null, emptyList(), 0)

                val jData = JSONObject(data)
                val parsedVideos = VideoLessonFactory.parse(jData.optJSONArray("data"))
                return ServerResponseFactory.parse(jData, parsedVideos)
            }
        }
    }

    override fun getLikeParser(): JsonDataProvider<Like> {
        return object : JsonDataProvider<Like> {
            override fun parse(data: String?): ServerResponse<Like> {
                if (data == null) return ServerResponse<Like>(200, "Ok", null, emptyList(), 0)

                val jData = JSONObject(data)
                val parsedLike = LikeFactory.parse(jData.optJSONObject("data"))
                return ServerResponseFactory.parse(jData, parsedLike)
            }
        }
    }
}