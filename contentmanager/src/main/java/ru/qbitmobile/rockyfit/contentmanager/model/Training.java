package ru.qbitmobile.rockyfit.contentmanager.model;

import android.os.Parcel;

import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.module.db.DatabaseService;
import ru.qbitmobile.rockyfit.contentmanager.repository.ICoachRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.ITagRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.IVideoLessonsRepository;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Transient;

import java.util.ArrayList;
import java.util.List;

import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;

@Entity
public class Training implements Lesson {

    // region Properties

    @Id
    long id;

    @Property
    int number;

    @Property
    long catalogId;
    @Transient
    Catalog catalog;

    @Property
    long coachId;
    @Transient
    Coach coach;

    @Property
    String name;
    @Property
    String description;
    @Property
    String descriptionFull;

    @Property
    int lessonsCount;
    @Property
    long lessonsTotalDuration;

    @Property
    long complexityLevel;
    @Transient
    Complexity complexity;

    @Property
    long previewImageId;
    @Transient
    MediaFile previewImage;

    @Transient
    List<Tag> tags;

    @Property
    int likes;
    @Property
    boolean isLike;
    @Property
    long likeUpdateTimestamp;

    // endregion

    // region Constructors

    public Training() {
    }

    @Generated(hash = 4325073)
    public Training(long id, int number, long catalogId, long coachId, String name, String description, String descriptionFull, int lessonsCount, long lessonsTotalDuration, long complexityLevel,
            long previewImageId, int likes, boolean isLike, long likeUpdateTimestamp) {
        this.id = id;
        this.number = number;
        this.catalogId = catalogId;
        this.coachId = coachId;
        this.name = name;
        this.description = description;
        this.descriptionFull = descriptionFull;
        this.lessonsCount = lessonsCount;
        this.lessonsTotalDuration = lessonsTotalDuration;
        this.complexityLevel = complexityLevel;
        this.previewImageId = previewImageId;
        this.likes = likes;
        this.isLike = isLike;
        this.likeUpdateTimestamp = likeUpdateTimestamp;
    }

    protected Training(Parcel parcel) {
        this(
                parcel.readLong(),
                parcel.readInt(),
                parcel.readLong(),
                parcel.readLong(),
                parcel.readString(),
                parcel.readString(),
                parcel.readString(),
                parcel.readInt(),
                parcel.readLong(),
                parcel.readLong(),
                parcel.readLong(),
                parcel.readInt(),
                parcel.readByte() == 1,
                parcel.readLong()
        );
    }

    // endregion

    // region Get/set

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getNumber() {
        return this.number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public long getCatalogId() {
        return this.catalogId;
    }

    public void setCatalogId(long catalogId) {
        this.catalogId = catalogId;
    }

    public long getCoachId() {
        return coachId;
    }

    public void setCoachId(long coachId) {
        this.coachId = coachId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionFull() {
        return description;
    }

    public void setDescriptionFull(String descriptionFull) {
        this.descriptionFull = description;
    }

    public int getLessonsCount() {
        if (lessonsCount == 0) {
            return DependencyGraph.get(DatabaseService.class).findVideoLessonByTrainingId(id).size();
        }
        return lessonsCount;
    }

    public void setLessonsCount(int lessonsCount) {
        this.lessonsCount = lessonsCount;
    }

    public long getLessonsTotalDuration() {
        if (lessonsTotalDuration == 0L) {
            List<VideoLesson> lessons = DependencyGraph.get(DatabaseService.class).findVideoLessonByTrainingId(id);
            long sum = 0L;
            for (VideoLesson les : lessons)
                sum += les.duration;
            lessonsTotalDuration = sum;
        }
        return lessonsTotalDuration;
    }

    public void setLessonsTotalDuration(long lessonsTotalDuration) {
        this.lessonsTotalDuration = lessonsTotalDuration;
    }

    public long getComplexityLevel() {
        return complexityLevel;
    }

    public void setComplexityLevel(long complexityLevel) {
        this.complexityLevel = complexityLevel;
    }

    @Override
    public void setPreviewImageId(long previewImageId) {
        this.previewImageId = previewImageId;
    }

    @Override
    public long getPreviewImageId() {
        return previewImageId;
    }

    public int getLikes() {
        return this.likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public boolean getIsLike() {
        return this.isLike;
    }

    public void setIsLike(boolean isLike) {
        this.isLike = isLike;
    }

    public long getLikeUpdateTimestamp() {
        return this.likeUpdateTimestamp;
    }

    public void setLikeUpdateTimestamp(long likeUpdateTimestamp) {
        this.likeUpdateTimestamp = likeUpdateTimestamp;
    }

    // endregion

    // region Advanced get/set

    public Coach getCoach() {
        if (this.coach == null) {
            this.coach = DependencyGraph.get(ICoachRepository.class).getCoach(coachId);
        }
        return this.coach;
    }

    public List<Tag> getTags() {
        if (tags == null) {
            this.tags = DependencyGraph.get(ITagRepository.class).getTagsByTrainingId(id);
        }

        if (this.tags == null) {
            return new ArrayList<>();
        }
        return tags;
    }

    public long getFirstVideoId() {
        VideoLesson firstLesson = DependencyGraph.get(IVideoLessonsRepository.class).getFirstVideoOfTraining(this.id);
        if (firstLesson == null) {
            return -1L;
        }
        return firstLesson.getId();
    }

    @Override
    public String getComplexityLevelTitle() {
        if (getComplexity() == null) {
            return "";
        }
        return getComplexity().getName();
    }

    public Catalog getCatalog() {
        if (this.catalog == null) {
            this.catalog = DependencyGraph.get(DatabaseService.class).findCatalogById(catalogId);
        }
        return catalog;
    }

    public Complexity getComplexity() {
        if (this.complexity == null) {
            this.complexity = DependencyGraph.get(DatabaseService.class).findComplexityById(complexityLevel);
        }
        return complexity;
    }

    public MediaFile getPreviewImage() {
        if (this.previewImage == null) {
            this.previewImage = DependencyGraph.get(DatabaseService.class).findMediaFileById(previewImageId);
        }
        return previewImage;
    }

    public String getPreviewImageUri() {
        if (this.getPreviewImage() == null) {
            return null;
        }
        return previewImage.getUri();
    }

    // endregion

    // region Parsing get/set

    public void setCatalog(Catalog catalog) {
        this.catalog = catalog;
    }

    public void setCoach(Coach coach) {
        this.coach = coach;
    }

    public void setComplexity(Complexity complexity) {
        this.complexity = complexity;
    }

    public void setPreviewImage(MediaFile previewImage) {
        this.previewImage = previewImage;
    }

    // endregion

    // region Parcelable

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeInt(number);
        dest.writeLong(catalogId);
        dest.writeLong(coachId);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(descriptionFull);
        dest.writeInt(lessonsCount);
        dest.writeLong(lessonsTotalDuration);
        dest.writeLong(complexityLevel);
        dest.writeLong(previewImageId);
        dest.writeInt(likes);
        dest.writeByte((byte) (isLike ? 1 : 0));
        dest.writeLong(likeUpdateTimestamp);
    }

    public static final Creator<Training> CREATOR = new Creator<Training>() {
        @Override
        public Training createFromParcel(Parcel in) {
            return new Training(in);
        }

        @Override
        public Training[] newArray(int size) {
            return new Training[size];
        }
    };

    // endregion


    @Override
    public String toString() {
        return "Training{" +
                "id=" + id +
                ", number=" + number +
                ", catalogId=" + catalogId +
                ", coachId=" + coachId +
                ", name='" + name + '\'' +
                ", lessonsCount=" + lessonsCount +
                ", lessonsTotalDuration=" + lessonsTotalDuration +
                ", complexityLevel='" + complexityLevel + '\'' +
                ", previewImageUri='" + getPreviewImageUri() + '\'' +
                ", likes=" + likes +
                ", isLike=" + isLike +
                '}';
    }
}
