package ru.qbitmobile.rockyfit.contentmanager.module.network.arbitr

/**
 * Слушатель выполнения задачи
 */
interface OnTaskCompleteListener {
    fun onComplete()
    fun onFailed(isUserError: Boolean)
}