package ru.qbitmobile.rockyfit.contentmanager.module.db;

import ru.qbitmobile.rockyfit.commoncomponent.helper.CollectionConverter;
import ru.qbitmobile.rockyfit.contentmanager.enumeration.LessonType;
import ru.qbitmobile.rockyfit.contentmanager.model.Catalog;
import ru.qbitmobile.rockyfit.contentmanager.model.CatalogDao;
import ru.qbitmobile.rockyfit.contentmanager.model.Coach;
import ru.qbitmobile.rockyfit.contentmanager.model.CoachDao;
import ru.qbitmobile.rockyfit.contentmanager.model.Complexity;
import ru.qbitmobile.rockyfit.contentmanager.model.ComplexityDao;
import ru.qbitmobile.rockyfit.contentmanager.model.DaoMaster;
import ru.qbitmobile.rockyfit.contentmanager.model.DaoSession;
import ru.qbitmobile.rockyfit.contentmanager.model.MediaFile;
import ru.qbitmobile.rockyfit.contentmanager.model.MediaFileDao;
import ru.qbitmobile.rockyfit.contentmanager.model.Tag;
import ru.qbitmobile.rockyfit.contentmanager.model.TagDao;
import ru.qbitmobile.rockyfit.contentmanager.model.TaggedVideo;
import ru.qbitmobile.rockyfit.contentmanager.model.TaggedVideoDao;
import ru.qbitmobile.rockyfit.contentmanager.model.Training;
import ru.qbitmobile.rockyfit.contentmanager.model.TrainingDao;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLessonDao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static ru.qbitmobile.rockyfit.contentmanager.module.db.DatabaseQueryHelperKt.buildInCondition;
import static ru.qbitmobile.rockyfit.contentmanager.module.db.DatabaseQueryHelperKt.buildInConditionStrings;

/**
 * Основной класс работы с БД.
 * Использует GreenDAO в качестве ORM
 */
public class DBModule implements DatabaseService {
    private final DaoSession mDaoSession;

    public DBModule(DaoOpenHelper daoOpenHelper) {
        this.mDaoSession = new DaoMaster(daoOpenHelper.getWritableDb()).newSession();
    }

    public DBModule(DaoSession daoSession) {
        this.mDaoSession = daoSession;
    }

    @Override
    public void dropAllTables() {
        mDaoSession.getDatabase().beginTransaction();
        DaoMaster.dropAllTables(mDaoSession.getDatabase(), true);
        DaoMaster.createAllTables(mDaoSession.getDatabase(), true);
        mDaoSession.getDatabase().setTransactionSuccessful();
        mDaoSession.getDatabase().endTransaction();
    }

    // region Catalog

    @Override
    public Catalog findCatalogById(long catalogId) {
        return mDaoSession.getCatalogDao().queryBuilder()
                .where(
                        CatalogDao.Properties.Id.eq(catalogId)
                )
                .unique();
    }

    @Override
    public void insertCatalogs(List<Catalog> catalogs) {
        mDaoSession.getCatalogDao().insertOrReplaceInTx(catalogs);
    }

    @Override
    public void insertCatalogs(Catalog... catalogs) {
        mDaoSession.getCatalogDao().insertOrReplaceInTx(catalogs);
    }

    // endregion

    // region Trainings

    @Override
    public List<Training> getAllTrainings() {
        return mDaoSession.getTrainingDao().loadAll();
    }

    @Override
    public List<Training> getAllTrainingsByCatalogId(long catalogId) {
        return mDaoSession.getTrainingDao().queryBuilder()
                .where(
                        TrainingDao.Properties.CatalogId.eq(catalogId)
                )
                .orderAsc(
                        TrainingDao.Properties.Number
                )
                .list();
    }

    @Override
    public void insertTrainings(List<Training> trainings) {
        mDaoSession.getTrainingDao().insertOrReplaceInTx(trainings);
    }

    @Override
    public void insertTrainings(Training... trainings) {
        mDaoSession.getTrainingDao().insertOrReplaceInTx(trainings);
    }

    @Override
    public Training findTrainingById(long trainingId) {
        return mDaoSession.getTrainingDao().queryBuilder()
                .where(
                        TrainingDao.Properties.Id.eq(trainingId)
                )
                .unique();
    }

    @Override
    public void clearTrainings() {
        mDaoSession.getTrainingDao().deleteAll();
    }

    // endregion

    // region VideoLessons


    @Override
    public List<VideoLesson> getAllVideoLessons() {
        return mDaoSession.getVideoLessonDao().loadAll();
    }

    @Override
    public List<VideoLesson> getFreeVideoLessons() {

        List<Long> freeTrainingIds = filterFreeTrainingIds();
        return mDaoSession.getVideoLessonDao()
                .queryBuilder()
                .where(
                        buildInCondition(VideoLessonDao.Properties.TrainingId, freeTrainingIds)
                )
                .orderAsc(
                        VideoLessonDao.Properties.TrainingId,
                        VideoLessonDao.Properties.Step
                )
                .list();
    }

    @Override
    public void insertVideoLessons(List<VideoLesson> lessons) {
        mDaoSession.getVideoLessonDao().insertOrReplaceInTx(lessons);
    }

    @Override
    public void insertVideoLessons(VideoLesson... lessons) {
        mDaoSession.getVideoLessonDao().insertOrReplaceInTx(lessons);
    }

    @Override
    public VideoLesson findVideoLessonById(long lessonId) {
        return mDaoSession.getVideoLessonDao().queryBuilder()
                .where(
                        VideoLessonDao.Properties.Id.eq(lessonId)
                )
                .unique();
    }

    @Override
    public List<VideoLesson> findVideoLessonByTrainingId(long trainingId) {
        return mDaoSession.getVideoLessonDao().queryBuilder()
                .where(
                        VideoLessonDao.Properties.TrainingId.eq(trainingId)
                )
                .orderAsc(
                        VideoLessonDao.Properties.Step,
                        VideoLessonDao.Properties.Id
                )
                .list();
    }

    @Override
    public List<VideoLesson> findVideoLessonByTags(List<String> tags, boolean freeOnly) {
        // определение ID видео, которые содержат ВСЕ теги
        List<TaggedVideo> taggedVideos = mDaoSession.getTaggedVideoDao().queryBuilder()
                .where(
                        buildInConditionStrings(TaggedVideoDao.Properties.Tag, tags)
                )
                .list();
        Map<Long, Integer> videoIds = new HashMap<>();  // ID урока --> количество тегов, удовлетворяющих запросу
        for (TaggedVideo taggedVideo : taggedVideos) {
            long key = taggedVideo.getVideoLessonId();
            if (videoIds.containsKey(key)) {
                videoIds.put(key, videoIds.get(key) + 1);
            } else {
                videoIds.put(key, 1);
            }
        }
        List<Long> selectedVideos = new ArrayList<>();
        for (long key : videoIds.keySet()) {
            if (videoIds.get(key) == tags.size()) {
                selectedVideos.add(key);
            }
        }

        if (freeOnly) {
            List<Long> freeTrainingIds = filterFreeTrainingIds();
            return mDaoSession.getVideoLessonDao().queryBuilder()
                    .where(
                            buildInCondition(VideoLessonDao.Properties.TrainingId, freeTrainingIds),
                            buildInCondition(VideoLessonDao.Properties.Id, selectedVideos)
                    )
                    .orderAsc(
                            VideoLessonDao.Properties.Step,
                            VideoLessonDao.Properties.Id
                    )
                    .list();
        } else {
            return mDaoSession.getVideoLessonDao().queryBuilder()
                    .where(
                            buildInCondition(VideoLessonDao.Properties.Id, selectedVideos)
                    )
                    .orderAsc(
                            VideoLessonDao.Properties.Step,
                            VideoLessonDao.Properties.Id
                    )
                    .list();
        }
    }

    @Override
    public void clearVideoLessons() {
        mDaoSession.getVideoLessonDao().deleteAll();
    }

    // endregion

    // region Coaches

    @Override
    public List<Coach> getAllCoaches() {
        return mDaoSession.getCoachDao().loadAll();
    }

    @Override
    public Coach findCoachById(long id) {
        return mDaoSession.getCoachDao().queryBuilder()
                .where(
                        CoachDao.Properties.Id.eq(id)
                )
                .unique();
    }

    @Override
    public void insertCoaches(List<Coach> coaches) {
        mDaoSession.getCoachDao().insertOrReplaceInTx(coaches);
    }

    @Override
    public void insertCoaches(Coach... coaches) {
        mDaoSession.getCoachDao().insertOrReplaceInTx(coaches);
    }

    // endregion

    // region Tags

    @Override
    public List<Tag> getAllTags() {
        return mDaoSession.getTagDao().loadAll();
    }

    @Override
    public List<Tag> findTagsOfFreeVideos() {
        List<VideoLesson> freeVideos = this.getFreeVideoLessons();
        List<Long> freeVideosIds = new ArrayList<>();
        for (VideoLesson les : freeVideos) {
            freeVideosIds.add(les.getId());
        }

        List<TaggedVideo> taggedVideos = mDaoSession.getTaggedVideoDao()
                .queryBuilder()
                .where(buildInCondition(TaggedVideoDao.Properties.VideoLessonId, freeVideosIds))
                .list();

        Set<String> tags = new HashSet<>();
        for (TaggedVideo taggedVideo : taggedVideos) {
            tags.add(taggedVideo.getTag());
        }

        return mDaoSession.getTagDao().queryBuilder()
                .where(
                        buildInConditionStrings(TagDao.Properties.Name, CollectionConverter.toList(tags))
                )
                .list();

    }

    @Override
    public void insertTags(List<Tag> tags) {
        mDaoSession.getTagDao().insertOrReplaceInTx(tags);
    }

    @Override
    public void insertTags(Tag... tags) {
        mDaoSession.getTagDao().insertOrReplaceInTx(tags);
    }

    @Override
    public void insertTaggedVideos(List<TaggedVideo> taggedVideos) {
        mDaoSession.getTaggedVideoDao().insertOrReplaceInTx(taggedVideos);
    }

    @Override
    public void insertTaggedVideos(TaggedVideo... taggedVideos) {
        mDaoSession.getTaggedVideoDao().insertOrReplaceInTx(taggedVideos);
    }

    @Override
    public List<Tag> findTagsByVideoId(long videoId) {
        List<TaggedVideo> taggedVideos = mDaoSession.getTaggedVideoDao().queryBuilder()
                .where(
                        TaggedVideoDao.Properties.VideoLessonId.eq(videoId)
                )
                .list();
        Set<String> tags = new HashSet<>();
        for (TaggedVideo taggedVideo : taggedVideos) {
            tags.add(taggedVideo.getTag());
        }

        return mDaoSession.getTagDao().queryBuilder()
                .where(
                        buildInConditionStrings(TagDao.Properties.Name, CollectionConverter.toList(tags))
                )
                .list();
    }

    @Override
    public void clearTags() {
        mDaoSession.getTagDao().deleteAll();
    }

    // endregion

    // region Complexity

    @Override
    public Complexity findComplexityById(long id) {
        return mDaoSession.getComplexityDao().queryBuilder()
                .where(
                        ComplexityDao.Properties.Id.eq(id)
                )
                .unique();
    }

    @Override
    public void insertComplexity(List<Complexity> complexities) {
        this.mDaoSession.getComplexityDao().insertOrReplaceInTx(complexities);
    }

    @Override
    public void insertComplexity(Complexity... complexities) {
        this.mDaoSession.getComplexityDao().insertOrReplaceInTx(complexities);
    }

    // endregion

    // region Media files

    @Override
    public MediaFile findMediaFileById(long id) {
        return mDaoSession.getMediaFileDao().queryBuilder()
                .where(
                        MediaFileDao.Properties.Id.eq(id)
                )
                .unique();
    }

    @Override
    public List<MediaFile> findMediaFilesByTrainingId(long trainingId) {
        // самостоятельное построение списка, т.к. для некоторых уроков превью - нулевое,
        // и в общем случае вернется запросом из БД единственный раз

        List<MediaFile> files = new ArrayList<>();
        List<VideoLesson> lessons = findVideoLessonByTrainingId(trainingId);
        for (VideoLesson lesson : lessons) {
            MediaFile file = lesson.getPreviewImage();
            if (file != null) {
                files.add(file);
            }
        }
        return files;
    }

    @Override
    public void insertMediaFiles(List<MediaFile> files) {
        this.mDaoSession.getMediaFileDao().insertOrReplaceInTx(files);
    }

    @Override
    public void insertMediaFiles(MediaFile... files) {
        this.mDaoSession.getMediaFileDao().insertOrReplaceInTx(files);
    }

    // endregion

    // region Filtering helper

    private List<Training> filterFreeTrainings() {
        /// TODO: add master classes were bought
        return mDaoSession.getTrainingDao().queryBuilder()
                .where(TrainingDao.Properties.CatalogId.eq(LessonType.TRAINING))
                .list();
    }

    private List<Long> filterFreeTrainingIds() {
        List<Training> freeTrainings = filterFreeTrainings();
        List<Long> freeTrainingIds = new ArrayList<>();
        for (Training t : freeTrainings) {
            freeTrainingIds.add(t.getId());
        }
        return freeTrainingIds;
    }

    // endregion
}
