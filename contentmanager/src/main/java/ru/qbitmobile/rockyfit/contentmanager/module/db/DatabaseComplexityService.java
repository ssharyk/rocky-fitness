package ru.qbitmobile.rockyfit.contentmanager.module.db;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.model.Complexity;

import java.util.List;

public interface DatabaseComplexityService extends Service {
    Complexity findComplexityById(long id);

    void insertComplexity(List<Complexity> complexities);
    void insertComplexity(Complexity... complexities);

}
