package ru.qbitmobile.rockyfit.contentmanager.module.db;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.model.MediaFile;

import java.util.List;

public interface DatabaseMediaFilesService extends Service {

    MediaFile findMediaFileById(long id);
    List<MediaFile> findMediaFilesByTrainingId(long trainingId);

    void insertMediaFiles(List<MediaFile> files);
    void insertMediaFiles(MediaFile... files);
}
