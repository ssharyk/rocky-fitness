package ru.qbitmobile.rockyfit.contentmanager.repository;

import androidx.annotation.Nullable;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.model.Coach;
import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.model.Coach;

public interface ICoachRepository extends Service {
    @Nullable
    Coach getCoach(long id);
}
