package ru.qbitmobile.rockyfit.contentmanager.model;

import android.os.Parcel;
import android.os.Parcelable;

import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.module.db.DatabaseService;
import ru.qbitmobile.rockyfit.contentmanager.module.subscription.SubscriptionService;
import ru.qbitmobile.rockyfit.contentmanager.repository.ITagRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.ITrainingRepository;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;
import org.greenrobot.greendao.annotation.Transient;

import java.util.ArrayList;
import java.util.List;

import ru.qbitmobile.rockyfit.contentmanager.updateobserver.UpdateObservable;
import ru.qbitmobile.rockyfit.contentmanager.updateobserver.VideoLessonUpdateObserver;

@Entity
public class VideoLesson extends UpdateObservable<VideoLessonUpdateObserver> implements Parcelable, Markable {

    // region Properties

    @Id
    long id;

    @Property
    long trainingId;
    @Transient
    Training training;

    @Transient
    Coach coach;

    @Property
    int step;
    @Property
    long duration;

    @Property
    String name;
    @Property
    String description;

    @Property
    long previewImageId;
    @Transient
    MediaFile previewImage;

    @Property
    int width;
    @Property
    int height;
    @Property
    String videoUri;

    @Transient
    List<Tag> tags;
    @Transient
    List<String> stringTags;

    @Property
    int likes;
    @Property
    boolean isLike;
    @Property
    long likeUpdateTimestamp;

    @Property
    boolean isFree;

    // endregion

    // region Constructor

    public VideoLesson() {
    }

    @Generated(hash = 80954556)
    public VideoLesson(long id, long trainingId, int step, long duration, String name, String description, long previewImageId, int width, int height,
                       String videoUri, int likes, boolean isLike, long likeUpdateTimestamp, boolean isFree) {
        this.id = id;
        this.trainingId = trainingId;
        this.step = step;
        this.duration = duration;
        this.name = name;
        this.description = description;
        this.previewImageId = previewImageId;
        this.width = width;
        this.height = height;
        this.videoUri = videoUri;
        this.likes = likes;
        this.isLike = isLike;
        this.likeUpdateTimestamp = likeUpdateTimestamp;
        this.isFree = isFree;
    }

    protected VideoLesson(Parcel in) {
        this(
                in.readLong(),
                in.readLong(),
                in.readInt(),
                in.readLong(),
                in.readString(),
                in.readString(),
                in.readLong(),
                in.readInt(),
                in.readInt(),
                in.readString(),
                in.readInt(),
                in.readByte() == 1,
                in.readLong(),
                in.readByte() == 1
        );
    }

    // endregion

    // region Get/set

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTrainingId() {
        return trainingId;
    }

    public void setTrainingId(long trainingId) {
        this.trainingId = trainingId;
    }

    public int getStep() {
        return step;
    }

    public void setStep(int step) {
        this.step = step;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getPreviewImageId() {
        return previewImageId;
    }

    public void setPreviewImageId(long previewImageId) {
        this.previewImageId = previewImageId;
    }

    public String getVideoUri() {
        return videoUri;
    }

    public void setVideoUri(String videoUri) {
        this.videoUri = videoUri;
    }

    public int getLikes() {
        return this.likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public boolean getIsLike() {
        return this.isLike;
    }

    public void setIsLike(boolean isLike) {
        this.isLike = isLike;
    }

    public List<String> getStringTags() {
        return stringTags;
    }

    public void setStringTags(List<String> stringTags) {
        this.stringTags = stringTags;
    }

    @Override
    public long getLikeUpdateTimestamp() {
        return likeUpdateTimestamp;
    }

    @Override
    public void setLikeUpdateTimestamp(long likeUpdateTimestamp) {
        this.likeUpdateTimestamp = likeUpdateTimestamp;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public boolean getIsFree() {
        return this.isFree;
    }

    public void setIsFree(boolean isFree) {
        this.isFree = isFree;
    }

    // endregion

    // region Advanced get/set

    public Coach getCoach() {
        if (this.coach == null) {
            if (this.getTraining() != null) {
                this.coach = this.getTraining().getCoach();
            }
        }
        return this.coach;
    }

    public List<Tag> getTags() {
        if (tags == null) {
            this.tags = DependencyGraph.get(ITagRepository.class).getTagsByVideoLessonId(id);
        }

        if (this.tags == null) {
            return new ArrayList<>();
        }
        return tags;
    }

    public int getProgress() {
        if (getTraining() == null || getTraining().getLessonsCount() == 0) {
            return 100;
        }

        return (100 * this.step) / getTraining().getLessonsCount();
    }

    public Training getTraining() {
        if (this.training == null || this.training.getId() != trainingId) {
            this.training = DependencyGraph.get(ITrainingRepository.class).getTrainingById(trainingId);
        }
        return training;
    }

    public MediaFile getPreviewImage() {
        if (this.previewImage == null) {
            this.previewImage = DependencyGraph.get(DatabaseService.class).findMediaFileById(previewImageId);
        }
        return previewImage;
    }

    public String getPreviewImageUri() {
        if (this.getPreviewImage() == null) {
            return null;
        }
        return previewImage.getUri();
    }

    public boolean isUnblocked() {
        // отмечено как свободное на сервере ИЛИ доступно, т.к. тренировка ИЛИ куплено
        return isFree ||
                DependencyGraph.get(SubscriptionService.class).subscriptionPresent();
    }

    // endregion

    // region Parse get/set

    public void setPreviewImage(MediaFile previewImage) {
        this.previewImage = previewImage;
    }

    // endregion

    // region Parcelable

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeLong(trainingId);
        dest.writeInt(step);
        dest.writeLong(duration);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeLong(previewImageId);
        dest.writeInt(width);
        dest.writeInt(height);
        dest.writeString(videoUri);
        dest.writeInt(likes);
        dest.writeByte((byte) (isLike ? 1 : 0));
        dest.writeLong(likeUpdateTimestamp);
        dest.writeByte((byte) (isFree ? 1 : 0));
    }

    public static final Creator<VideoLesson> CREATOR = new Creator<VideoLesson>() {
        @Override
        public VideoLesson createFromParcel(Parcel in) {
            return new VideoLesson(in);
        }

        @Override
        public VideoLesson[] newArray(int size) {
            return new VideoLesson[size];
        }
    };

    // endregion

    // region Updates

    public void notifyUpdateLikes() {
        for (int i = 0; i < updateObservers.size(); i++) {
            updateObservers.get(i).updateLike(this);
        }
    }

    // endregion

    @Override
    public String toString() {
        return "VideoLesson{" +
                "id=" + id +
                ", trainingId=" + trainingId +
                ", step=" + step +
                ", duration=" + duration +
                ", name='" + name + '\'' +
                ", previewImageId='" + previewImageId + '\'' +
                ", videoUri='" + videoUri + '\'' +
                ", stringTags=" + stringTags +
                ", likes=" + likes +
                ", isLike=" + isLike +
                ", likeUpdateTimestamp=" + likeUpdateTimestamp +
                '}';
    }
}
