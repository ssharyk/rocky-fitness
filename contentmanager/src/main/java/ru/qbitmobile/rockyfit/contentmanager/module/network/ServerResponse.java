package ru.qbitmobile.rockyfit.contentmanager.module.network;

import java.util.List;

public class ServerResponse<T> {

    int status;
    String message;

    T data;
    List<ServerErrorResponse> errors;
    int total;

    public ServerResponse() {
    }

    public ServerResponse(int status, String message, T data, List<ServerErrorResponse> errors, int total) {
        this.status = status;
        this.message = message;
        this.data = data;
        this.errors = errors;
        this.total = total;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public List<ServerErrorResponse> getErrors() {
        return errors;
    }

    public void setErrors(List<ServerErrorResponse> errors) {
        this.errors = errors;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
