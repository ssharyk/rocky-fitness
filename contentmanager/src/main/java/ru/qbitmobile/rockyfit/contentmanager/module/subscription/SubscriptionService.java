package ru.qbitmobile.rockyfit.contentmanager.module.subscription;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;

/**
 * Контроллер наличия подписки на мастер-класс/видео
 */
public interface SubscriptionService extends Service {

    void subscriptionInit();

    boolean subscriptionPresent();

    void subscriptionShowDialog(Runnable onSuccess);

}
