package ru.qbitmobile.rockyfit.contentmanager.repository.fake

import androidx.lifecycle.MutableLiveData
import ru.qbitmobile.rockyfit.contentmanager.model.Tag
import ru.qbitmobile.rockyfit.contentmanager.repository.ITagRepository
import ru.qbitmobile.rockyfit.contentmanager.repository.livedata.DataSource
import ru.qbitmobile.rockyfit.contentmanager.repository.livedata.RepositoryData
import kotlin.random.Random

class FakeTagRepository : ITagRepository {
    companion object {

        val TAGS = mutableListOf<Tag>(
                Tag("Lorem ipsum"),
                Tag("dolor sit amet"),
                Tag("consectetur adipiscing elit"),
                Tag("Donec nisl ipsum"),
                Tag("aliquet vel accumsan ac"),
                Tag("hendrerit a massa"),
                Tag("Donec nec neque convallis"),
                Tag("tincidunt metus nec"),
                Tag("bibendum nisi"),
                Tag("Nam ut"),
                Tag("lectus ut"),
                Tag("urna sodales"),
                Tag("fringilla vel in lectus"),
                Tag("Nullam nec odio"),
                Tag("at erat egestas efficitur")
        )
    }

    override fun getTags(): MutableLiveData<RepositoryData<List<Tag>>> {
        val tagsLiveData = MutableLiveData<RepositoryData<List<Tag>>>()
        tagsLiveData.value = RepositoryData(TAGS, DataSource.DATABASE)
        return tagsLiveData
    }

    override fun getTagsByCourseId(courseId: Long): List<Tag> {
        val count = Random(0L).nextInt(5)
        return TAGS.shuffled().take(count)
    }

    override fun getTagsByTrainingId(trainingId: Long): List<Tag> {
        val count = 3 + Random(0L).nextInt(5)
        return TAGS.shuffled().take(count)
    }

    override fun getTagsByVideoLessonId(lessonId: Long): List<Tag> {
        val count = 2 + Random(0L).nextInt(5)
        return TAGS.shuffled().take(count)
    }

    override fun updateTagsWithNetwork(tags: MutableList<Tag>) {
        TAGS.addAll(tags)
    }
}