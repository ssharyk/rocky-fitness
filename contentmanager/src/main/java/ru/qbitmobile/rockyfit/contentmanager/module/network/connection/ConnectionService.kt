package ru.qbitmobile.rockyfit.contentmanager.module.network.connection

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service

/**
 * Управляет статусом Интернет-соединения
 */
interface ConnectionService : Service {

    val connectionStatus: Int

    companion object {

        const val NO_CONNECTION = 0
        const val CONNECTED = 1
        const val CONNECTING = 2
    }
}
