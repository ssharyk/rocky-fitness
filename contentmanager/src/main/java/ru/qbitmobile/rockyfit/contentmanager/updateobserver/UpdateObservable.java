package ru.qbitmobile.rockyfit.contentmanager.updateobserver;

import java.util.ArrayList;
import java.util.List;

public class UpdateObservable<T extends UpdateObserver> {
    protected List<T> updateObservers = new ArrayList<>();

    public void addObserver(T observer) {
        if (!this.updateObservers.contains(observer)) {
            updateObservers.add(observer);
        }
    }

    public void removeObserver(T observer) {
        updateObservers.remove(observer);
    }

    public void removeObservers(int type) {
        for (int i = 0; i < updateObservers.size(); i++) {
            if (updateObservers.get(i).getObserverType() == type) {
                updateObservers.remove(i);
                i--;
            }
        }
    }
}
