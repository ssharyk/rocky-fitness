package ru.qbitmobile.rockyfit.contentmanager.module.network.networktask

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service
import ru.qbitmobile.rockyfit.contentmanager.module.network.NetworkCallbackFailure
import ru.qbitmobile.rockyfit.contentmanager.module.network.NetworkCallbackSuccess
import ru.qbitmobile.rockyfit.contentmanager.module.network.arbitr.Task

/**
 * Управляет набором задач для передачи их в арбитр
 */
interface NetworkTaskService : Service {
    fun <T> taskCreateSuccessCallback(taskAddress: Int, onSuccessCallback: NetworkCallbackSuccess<T>?): NetworkCallbackSuccess<T>
    fun taskCreateErrorCallback(taskAddress: Int, onErrorCallback: NetworkCallbackFailure?): NetworkCallbackFailure

    fun wrapActiveTask(task: Task)
}