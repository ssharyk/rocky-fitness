package ru.qbitmobile.rockyfit.contentmanager.module.db;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.model.Coach;

import java.util.List;

public interface DatabaseCoachesService extends Service {
    List<Coach> getAllCoaches();

    Coach findCoachById(long id);

    void insertCoaches(List<Coach> coaches);

    void insertCoaches(Coach... coaches);
}
