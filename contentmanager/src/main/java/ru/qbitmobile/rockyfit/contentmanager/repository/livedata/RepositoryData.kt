package ru.qbitmobile.rockyfit.contentmanager.repository.livedata

/**
 * Базвый класс для передачи данных из репозитория наблюдателям с указанием источника данных
 */
data class RepositoryData<T>(val data: T, val dataSource: DataSource)