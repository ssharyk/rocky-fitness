package ru.qbitmobile.rockyfit.contentmanager.module.network.parser

import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson
import org.json.JSONArray
import org.json.JSONObject

object VideoLessonFactory {
    fun parse(jList: JSONArray): List<VideoLesson> {
        val result = ArrayList<VideoLesson>()
        for (i in 0 until jList.length()) {
            result.add(parse(jList.optJSONObject(i) ?: continue))
        }
        return result
    }

    fun parse(jVideo: JSONObject): VideoLesson {
        return VideoLesson().apply {
            id = jVideo.optLong("_id")
            name = jVideo.optString("name")
            description = jVideo.optString("description")
            trainingId = jVideo.optLong("categoryId")
            step = jVideo.optInt("number")
            duration = jVideo.optLong("duration")
            width = jVideo.optInt("width")
            height = jVideo.optInt("height")
            videoUri = jVideo.optString("video")
            previewImage = ImageFactory.parse(jVideo.optNestedJSONObject("image"))
            likes = jVideo.optInt("likes")
            isLike = jVideo.optBoolean("isLike")
            likeUpdateTimestamp = jVideo.optLong("dateLike")
            stringTags = parseTags(jVideo.optNestedJSONArray("tags"))
            setIsFree(jVideo.optBoolean("free"))
        }
    }

    private fun parseTags(jTags: JSONArray): List<String> {
        val list = ArrayList<String>()
        for (i in 0 until jTags.length()) {
            list.add(jTags.optString(i))
        }
        return list
    }
}