package ru.qbitmobile.rockyfit.contentmanager.repository.fake

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import ru.qbitmobile.rockyfit.commoncomponent.annotation.DebugOnly
import ru.qbitmobile.rockyfit.commoncomponent.helper.Now
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph
import ru.qbitmobile.rockyfit.contentmanager.enumeration.ComplexityLevel
import ru.qbitmobile.rockyfit.contentmanager.enumeration.LessonType
import ru.qbitmobile.rockyfit.contentmanager.model.MediaFile
import ru.qbitmobile.rockyfit.contentmanager.model.Training
import ru.qbitmobile.rockyfit.contentmanager.module.db.DatabaseService
import ru.qbitmobile.rockyfit.contentmanager.repository.IMediaFilesRepository
import ru.qbitmobile.rockyfit.contentmanager.repository.ITrainingRepository
import ru.qbitmobile.rockyfit.contentmanager.repository.livedata.DataSource
import ru.qbitmobile.rockyfit.contentmanager.repository.livedata.RepositoryData

@DebugOnly
class FakeCourcesRepository : IMediaFilesRepository, ITrainingRepository {
    companion object {

        val TRAININGS = mutableListOf(
                Training(1L, 1, LessonType.TRAINING, 1L, "Тренировка 1", FULL_DESCRIPTION.words(5), FULL_DESCRIPTION.random(150), 10, 10, ComplexityLevel.MEDIUM.toLong(), buildPreviewId(1L), 1, true, Now.get()),
                Training(2L, 2, LessonType.TRAINING, 1L, "Тренировка 2", FULL_DESCRIPTION.words(5), FULL_DESCRIPTION.random(150), 10, 10, ComplexityLevel.MEDIUM.toLong(), buildPreviewId(2L), 2, false, 0L),
                Training(3L, 3, LessonType.TRAINING, 1L, "Тренировка 3", FULL_DESCRIPTION.words(5), FULL_DESCRIPTION.random(150), 12, 10, ComplexityLevel.MEDIUM.toLong(), buildPreviewId(3L), 3, false, 0L),
                Training(4L, 4, LessonType.TRAINING, 1L, "Тренировка 4", FULL_DESCRIPTION.words(5), FULL_DESCRIPTION.random(150), 20, 10, ComplexityLevel.MEDIUM.toLong(), buildPreviewId(4L), 4, false, 0L),
                Training(5L, 5, LessonType.TRAINING, 1L, "Тренировка 5", FULL_DESCRIPTION.words(5), FULL_DESCRIPTION.random(150), 33, 10, ComplexityLevel.MEDIUM.toLong(), buildPreviewId(5L), 5, false, 0L),
                Training(6L, 6, LessonType.TRAINING, 1L, "Тренировка 6", FULL_DESCRIPTION.words(5), FULL_DESCRIPTION.random(150), 44, 10, ComplexityLevel.MEDIUM.toLong(), buildPreviewId(6L), 6, false, 0L),
                Training(7L, 7, LessonType.TRAINING, 1L, "Тренировка 7", FULL_DESCRIPTION.words(5), FULL_DESCRIPTION.random(150), 55, 10, ComplexityLevel.MEDIUM.toLong(), buildPreviewId(7L), 7, false, 0L),
                Training(8L, 8, LessonType.TRAINING, 1L, "Тренировка 8", FULL_DESCRIPTION.words(5), FULL_DESCRIPTION.random(150), 1, 10, ComplexityLevel.MEDIUM.toLong(), buildPreviewId(8L), 8, false, 0L),
                Training(9L, 9, LessonType.TRAINING, 1L, "Тренировка 9", FULL_DESCRIPTION.words(5), FULL_DESCRIPTION.random(150), 2, 10, ComplexityLevel.MEDIUM.toLong(), buildPreviewId(9L), 9, false, 0L),
                Training(10L, 10, LessonType.TRAINING, 1L, "Тренировка 10", FULL_DESCRIPTION.words(5), FULL_DESCRIPTION.random(150), 3, 10, ComplexityLevel.MEDIUM.toLong(), buildPreviewId(10L), 10, false, 0L)
        )

        private const val IMAGE_HEIGHT = 200

        val FILES = listOf(
                MediaFile.loremIpsum(100, 300, IMAGE_HEIGHT, 1),
                MediaFile.loremIpsum(123, 200, IMAGE_HEIGHT, 1),
                MediaFile.loremIpsum(146, 250, IMAGE_HEIGHT, 1),
                MediaFile.loremIpsum(169, 300, IMAGE_HEIGHT, 1),
                MediaFile.loremIpsum(192, 900, IMAGE_HEIGHT, 1),

                MediaFile.loremIpsum(201, 200, IMAGE_HEIGHT, 2),
                MediaFile.loremIpsum(224, 300, IMAGE_HEIGHT, 2),
                MediaFile.loremIpsum(247, 200, IMAGE_HEIGHT, 2),
                MediaFile.loremIpsum(270, 300, IMAGE_HEIGHT, 2),
                MediaFile.loremIpsum(293, 200, IMAGE_HEIGHT, 2),

                MediaFile.loremIpsum(333, 600, IMAGE_HEIGHT, 3),
                MediaFile.loremIpsum(334, 600, IMAGE_HEIGHT, 3),
                MediaFile.loremIpsum(335, 600, IMAGE_HEIGHT, 3),
                MediaFile.loremIpsum(336, 600, IMAGE_HEIGHT, 3),
                MediaFile.loremIpsum(337, 600, IMAGE_HEIGHT, 3),

                MediaFile.loremIpsum(444, 444, IMAGE_HEIGHT, 4)

                // nothing for the 5th
        )
    }

    override fun getTrainings(catalogId: Long): LiveData<RepositoryData<MutableList<Training>>> {
        val data = MutableLiveData<RepositoryData<MutableList<Training>>>()
        val specificTrainings = TRAININGS.filter { it.catalogId == catalogId }.toMutableList()
        data.value = RepositoryData(specificTrainings, DataSource.DATABASE)
        return data
    }

    override fun getTrainingById(trainingId: Long): Training? {
        return TRAININGS.firstOrNull { it.id == trainingId }
    }

    override fun updateTraining(training: Training) {
        val index = TRAININGS.indexOfFirst { it.id == training.id }
        if (index == -1) {
            TRAININGS.add(training)
        } else {
            TRAININGS[index] = training
        }
    }

    override fun getFilesByTrainingId(trainingId: Long): List<MediaFile> {
        //return FILES.filter { it.trainingId == trainingId }
        return DependencyGraph.get(DatabaseService::class.java).freeVideoLessons
                ?.filter { it.trainingId == trainingId }
                ?.map { MediaFile(it.id, 300, 200, buildPreviewUri(it.previewImageId), it.name) }
                ?: emptyList()
    }
}
