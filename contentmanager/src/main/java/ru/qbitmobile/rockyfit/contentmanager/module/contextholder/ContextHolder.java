package ru.qbitmobile.rockyfit.contentmanager.module.contextholder;

import android.app.Activity;
import android.content.Context;
import android.os.Build;

import androidx.fragment.app.FragmentActivity;

public class ContextHolder implements IContextHolder {
    private Context mContext;

    @Override
    public Context getContext() {
        return mContext;
    }

    @Override
    public void setContext(Context context) {
        mContext = context;
    }

    @Override
    public boolean isValid(Context context) {
        if (context == null) return false;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            if (context instanceof FragmentActivity) {
                if (((FragmentActivity) context).isDestroyed() || ((FragmentActivity) context).isFinishing()) {
                    return false;
                }
            } else if (context instanceof Activity) {
                if (((Activity) context).isDestroyed() || ((Activity) context).isFinishing()) {
                    return false;
                }
            }

            return true;
        } else {
            return true;
        }
    }
}
