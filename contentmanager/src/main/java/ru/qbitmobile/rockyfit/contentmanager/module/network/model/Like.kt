package ru.qbitmobile.rockyfit.contentmanager.module.network.model

data class Like(
        val totalLikes: Int,
        val likeTimestamp: Long,
        val isLike: Boolean
)
