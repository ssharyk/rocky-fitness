package ru.qbitmobile.rockyfit.contentmanager.module.network.connection

import ru.qbitmobile.rockyfit.contentmanager.module.network.arbitr.NetworkArbitr

class ConnectionModule : ConnectionService {
    init {
        InternetAvailabilityChecker.getInstance().addInternetConnectivityListener(NetworkArbitr)
    }

    override val connectionStatus: Int
        get() {
            if (InternetAvailabilityChecker.getInstance().currentInternetAvailabilityStatus) {
                return ConnectionService.CONNECTED
            } else {
                return ConnectionService.NO_CONNECTION
            }
        }


}