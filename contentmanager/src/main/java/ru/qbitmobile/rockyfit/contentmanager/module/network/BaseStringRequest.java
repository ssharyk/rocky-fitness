package ru.qbitmobile.rockyfit.contentmanager.module.network;

import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.module.sharedpreference.SharedPreferenceService;

/**
 * Базовый HTTP-запрос для Volley
 */
class BaseStringRequest extends StringRequest {

    private static Map<String, String> sHeaders;

    static {
        sHeaders = new HashMap<>();

        String deviceId = DependencyGraph.get(SharedPreferenceService.class).sharedGetDeviceId();
        sHeaders.put("DeviceId", deviceId);
    }

    public BaseStringRequest(String url, Response.Listener<String> listener, @Nullable Response.ErrorListener errorListener) {
        this(Method.GET, url, listener, errorListener);
    }

    public BaseStringRequest(int method, String url, Response.Listener<String> listener, @Nullable Response.ErrorListener errorListener) {
        super(method, url, listener, errorListener);
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        return sHeaders;
    }
}
