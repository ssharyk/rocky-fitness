package ru.qbitmobile.rockyfit.contentmanager.module.network.parser

import ru.qbitmobile.rockyfit.contentmanager.model.Catalog
import ru.qbitmobile.rockyfit.contentmanager.model.Coach
import ru.qbitmobile.rockyfit.contentmanager.model.Complexity
import ru.qbitmobile.rockyfit.contentmanager.model.Training
import org.json.JSONArray
import org.json.JSONObject

object CategoriesFactory {
    fun parse(jList: JSONArray): List<Training> {
        val result = ArrayList<Training>()
        for (i in 0 until jList.length()) {
            result.add(parse(jList.optJSONObject(i) ?: continue))
        }
        return result
    }

    fun parse(jCategory: JSONObject): Training {
        return Training().apply {
            id = jCategory.optLong("_id")
            number = jCategory.optInt("number")
            catalog = parseCatalog(jCategory.optNestedJSONObject("catalog"))
            coach = parseCoach(jCategory.optNestedJSONObject("coach"))
            name = jCategory.optString("name")
            description = jCategory.optString("description")
            descriptionFull = jCategory.optString("description_full")
            lessonsCount = jCategory.optInt("totalVideos")
            lessonsTotalDuration = jCategory.optLong("duration")
            complexity = parseComplexity(jCategory.optNestedJSONObject("complexity"))
            previewImage = ImageFactory.parse(jCategory.optNestedJSONObject("image"))
            likes = jCategory.optInt("likes")
            isLike = jCategory.optBoolean("isLike")
        }

    }

    private fun parseCatalog(jCatalog: JSONObject): Catalog {
        return Catalog(
                jCatalog.optLong("_id"),
                jCatalog.optString("name"),
                jCatalog.optString("alias")
        )
    }

    private fun parseComplexity(jComplexity: JSONObject): Complexity {
        return Complexity(
                jComplexity.optLong("_id"),
                jComplexity.optString("name")
        )
    }

    private fun parseCoach(jCoach: JSONObject): Coach {
        return Coach(
                jCoach.optLong("_id"),
                jCoach.optString("firstName"),
                jCoach.optString("lastName")
        )
    }
}