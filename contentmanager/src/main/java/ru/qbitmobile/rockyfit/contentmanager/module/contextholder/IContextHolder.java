package ru.qbitmobile.rockyfit.contentmanager.module.contextholder;

import android.content.Context;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;

/**
 * Обеспечивает доступ к текущему контексту
 */
public interface IContextHolder extends Service {
    Context getContext();

    void setContext(Context context);

    boolean isValid(Context contextToCheck);
}
