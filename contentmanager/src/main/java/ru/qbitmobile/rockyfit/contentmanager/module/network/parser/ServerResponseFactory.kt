package ru.qbitmobile.rockyfit.contentmanager.module.network.parser

import ru.qbitmobile.rockyfit.contentmanager.module.network.ServerErrorResponse
import ru.qbitmobile.rockyfit.contentmanager.module.network.ServerResponse
import org.json.JSONArray
import org.json.JSONObject

object ServerResponseFactory {
    fun <T> parse(json: JSONObject, data: T): ServerResponse<T> {
        val status = json.optInt("status")
        val message = json.optString("message")

        val jErrors = json.optJSONArray("errors") ?: JSONArray("[]")
        val errors = ArrayList<ServerErrorResponse>()
        for (i in 0 until jErrors.length()) {
            val err = parseError(jErrors.optJSONObject(i)) ?: continue
            errors.add(err)
        }
        val total = json.optInt("total")
        return ServerResponse(status, message, data, errors, total)
    }

    private fun parseError(jError: JSONObject?): ServerErrorResponse? {
        if (jError == null) return null
        return ServerErrorResponse(
                jError.optString("code"),
                jError.optString("text")
        )
    }
}