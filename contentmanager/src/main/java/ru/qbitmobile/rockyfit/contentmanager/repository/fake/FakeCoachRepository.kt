package ru.qbitmobile.rockyfit.contentmanager.repository.fake

import ru.qbitmobile.rockyfit.contentmanager.model.Coach
import ru.qbitmobile.rockyfit.contentmanager.repository.ICoachRepository

class FakeCoachRepository : ICoachRepository {
    companion object {
        val COACHES = listOf(
                Coach(1L, "Адам", "Суровый")
        )
    }
    override fun getCoach(id: Long): Coach? {
        return COACHES.firstOrNull { it.id == id }
    }
}