package ru.qbitmobile.rockyfit.contentmanager.repository;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.model.MediaFile;

import java.util.List;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.model.MediaFile;

public interface IMediaFilesRepository extends Service {
    List<MediaFile> getFilesByTrainingId(long trainingId);
}
