package ru.qbitmobile.rockyfit.contentmanager.module.sharedpreference;

public enum  AppStorage {
    DEVICE_ID,
    VIDEO_LIKE_IS_SET,
    VIDEO_LIKE_TIMESTAMP,
    HAS_SUBSCRIPTION,
}
