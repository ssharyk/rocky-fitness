package ru.qbitmobile.rockyfit.contentmanager.module.network;

public interface NetworkCallbackSuccess<T> {
    void onSuccess(T data);
}
