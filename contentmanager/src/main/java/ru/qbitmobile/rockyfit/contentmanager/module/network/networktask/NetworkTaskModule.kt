package ru.qbitmobile.rockyfit.contentmanager.module.network.networktask

import ru.qbitmobile.rockyfit.contentmanager.module.network.NetworkCallbackFailure
import ru.qbitmobile.rockyfit.contentmanager.module.network.NetworkCallbackSuccess
import ru.qbitmobile.rockyfit.contentmanager.module.network.arbitr.NetworkArbitr
import ru.qbitmobile.rockyfit.contentmanager.module.network.arbitr.Task

class NetworkTaskModule : NetworkTaskService {

    @Volatile
    private var tasks = HashMap<Int, Task>(10)

    override fun <T> taskCreateSuccessCallback(taskAddress: Int, onSuccessCallback: NetworkCallbackSuccess<T>?): NetworkCallbackSuccess<T> {
        return object : NetworkCallbackSuccess<T> {
            override fun onSuccess(data: T) {
                try {
                    onSuccessCallback?.onSuccess(data)
                } catch (t: Throwable) {
                    t.printStackTrace()
                }

                taskComplete(taskAddress)
            }
        }
    }

    override fun taskCreateErrorCallback(taskAddress: Int, onErrorCallback: NetworkCallbackFailure?): NetworkCallbackFailure {
        return object : NetworkCallbackFailure {
            override fun onFailure(httpResponseCode: Int, vararg customErrorCodes: String) {
                taskFail(taskAddress, httpResponseCode, *customErrorCodes)

                try {
                    onErrorCallback?.onFailure(httpResponseCode, *customErrorCodes)
                } catch (t: Throwable) {
                    t.printStackTrace()
                }
            }
        }
    }

    override fun wrapActiveTask(task: Task) {
        val taskAddress = task.taskAddress
        if (tasks.get(taskAddress) == null) {
            tasks.put(taskAddress, task)
            NetworkArbitr.addActiveTask(task)
        }
    }

    private fun taskComplete(taskAddress: Int) {
        try {
            tasks.get(taskAddress)?.onTaskCompleteListener?.onComplete()
            tasks.remove(taskAddress)
        } catch (t: Throwable) {
            NetworkArbitr.nullActiveCallback(taskAddress)
        }
    }

    private fun taskFail(taskAddress: Int, httpResponseCode: Int, vararg customErrorCodes: String) {
        try {
            val task = tasks.get(taskAddress)
            val isUserError = httpResponseCode != 200 || customErrorCodes.isNotEmpty()
            task?.onTaskCompleteListener?.onFailed(isUserError)
        } catch (t: Throwable) {
            NetworkArbitr.nullActiveCallback(taskAddress)
        }
    }
}