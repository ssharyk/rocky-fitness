package ru.qbitmobile.rockyfit.contentmanager.updateobserver

import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson

interface VideoLessonUpdateObserver : UpdateObserver {
    val videoLessonId: Long

    fun updateLike(videoLesson: VideoLesson)
}