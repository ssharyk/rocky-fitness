package ru.qbitmobile.rockyfit.contentmanager.updateobserver

interface UpdateObserver {
    val observerType: Int
}