package ru.qbitmobile.rockyfit.contentmanager.module.network.parser

import ru.qbitmobile.rockyfit.contentmanager.model.MediaFile
import org.json.JSONObject

object ImageFactory {
    fun parse(jImage: JSONObject): MediaFile {
        return MediaFile(
                jImage.optLong("_id"),
                jImage.optInt("width"),
                jImage.optInt("height"),
                jImage.optString("url"),
                jImage.optString("name")
        )
    }
}