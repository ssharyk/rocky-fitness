package ru.qbitmobile.rockyfit.contentmanager.module.sharedpreference;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;

public interface SharedPreferenceService extends Service {

    void dropAllRecords();

    void sharedSetHasSubscription(boolean hasSubscription);
    boolean sharedGetHasSubscription();

    void sharedSetDeviceId(String uniqueId);
    String sharedGetDeviceId();

    void sharedSetVideoLikeMark(long videoId, boolean isLike);
    boolean sharedGetVideoLikeMark(long videoId);

    void sharedSetVideoLikeMarkTimestamp(long videoId, long timestamp);
    long sharedGetVideoLikeMarkTimestamp(long videoId);
}
