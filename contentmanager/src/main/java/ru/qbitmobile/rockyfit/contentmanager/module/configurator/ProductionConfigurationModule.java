package ru.qbitmobile.rockyfit.contentmanager.module.configurator;

import java.util.List;

import ru.qbitmobile.rockyfit.commoncomponent.helper.CollectionConverter;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;

public class ProductionConfigurationModule implements ConfigurationService {
    @Override
    public String getServerDomain() {
        return "https://rockymobile.ru";
    }

    @Override
    public List<VideoLesson> shuffle(List<VideoLesson> origin) {
        return CollectionConverter.shuffle(origin);
    }
}
