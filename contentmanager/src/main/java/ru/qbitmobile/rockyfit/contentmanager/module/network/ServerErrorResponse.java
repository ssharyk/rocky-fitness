package ru.qbitmobile.rockyfit.contentmanager.module.network;

public class ServerErrorResponse {
    String code;
    String text;

    public ServerErrorResponse() {}
    public ServerErrorResponse(String code, String text) {
        this.code = code;
        this.text = text;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
