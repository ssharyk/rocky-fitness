package ru.qbitmobile.rockyfit.contentmanager.repository.usecase;

import androidx.annotation.Nullable;

import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.model.Coach;
import ru.qbitmobile.rockyfit.contentmanager.module.db.DatabaseService;
import ru.qbitmobile.rockyfit.contentmanager.repository.ICoachRepository;
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.model.Coach;

public class CoachRepository implements ICoachRepository {
    @Nullable
    @Override
    public Coach getCoach(long id) {
        return DependencyGraph.get(DatabaseService.class).findCoachById(id);
    }
}
