package ru.qbitmobile.rockyfit.contentmanager.repository.usecase;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.enumeration.LessonType;
import ru.qbitmobile.rockyfit.contentmanager.model.Catalog;
import ru.qbitmobile.rockyfit.contentmanager.model.Coach;
import ru.qbitmobile.rockyfit.contentmanager.model.Complexity;
import ru.qbitmobile.rockyfit.contentmanager.model.MediaFile;
import ru.qbitmobile.rockyfit.contentmanager.model.Training;
import ru.qbitmobile.rockyfit.contentmanager.module.db.DatabaseService;
import ru.qbitmobile.rockyfit.contentmanager.module.network.NetworkService;
import ru.qbitmobile.rockyfit.contentmanager.repository.ITrainingRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.IVideoLessonsRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.livedata.DataSource;
import ru.qbitmobile.rockyfit.contentmanager.repository.livedata.RepositoryData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class TrainingRepository implements ITrainingRepository {
    private Map<Long, MutableLiveData<RepositoryData<List<Training>>>> mAllTrainingsLiveData
            = new HashMap<>();

    public TrainingRepository() {
        mAllTrainingsLiveData.put(LessonType.TRAINING, new MutableLiveData<>());
        mAllTrainingsLiveData.put(LessonType.MASTER_CLASS, new MutableLiveData<>());
    }

    private boolean mCached;

    @Override
    public LiveData<RepositoryData<List<Training>>> getTrainings(long catalogId) {
        List<Training> cached = DependencyGraph.get(DatabaseService.class).getAllTrainingsByCatalogId(catalogId);

        if (mCached) {
            // ответ от сервера уже получен, вернуть его и закончить
            mAllTrainingsLiveData.get(catalogId).setValue(
                    new RepositoryData<>(mAllTrainingsLiveData.get(catalogId).getValue().getData(), DataSource.CACHE));
            return mAllTrainingsLiveData.get(catalogId);
        }

        // вернуть из БД
        mAllTrainingsLiveData.get(catalogId).setValue(new RepositoryData<>(cached, DataSource.DATABASE));

        // загрузить с сервера
        DependencyGraph.get(NetworkService.class).loadTrainings(
                trainings -> {

                    // установить поля и свойства для записи в БД на основе распаршенных вложенных объектов
                    List<Complexity> complexities = new ArrayList<>();
                    List<MediaFile> trainingPreviews = new ArrayList<>();
                    List<Coach> coaches = new ArrayList<>();
                    List<Catalog> catalogs = new ArrayList<>();
                    for (Training training : trainings) {
                        Coach coach = training.getCoach();
                        if (coach != null) {
                            coaches.add(coach);
                            training.setCoachId(coach.getId());
                        }

                        Complexity complexity = training.getComplexity();
                        if (complexity != null) {
                            complexities.add(complexity);
                            training.setComplexityLevel(complexity.getId());
                        }

                        MediaFile preview = training.getPreviewImage();
                        if (preview != null) {
                            trainingPreviews.add(preview);
                            training.setPreviewImageId(preview.getId());
                        }


                        Catalog catalog = training.getCatalog();
                        if (catalog != null) {
                            catalogs.add(catalog);
                            training.setCatalogId(catalog.getId());
                        }
                    }

                    DependencyGraph.get(DatabaseService.class).clearTrainings();
                    DependencyGraph.get(DatabaseService.class).insertTrainings(trainings);
                    DependencyGraph.get(DatabaseService.class).insertCatalogs(catalogs);
                    DependencyGraph.get(DatabaseService.class).insertCoaches(coaches);
                    DependencyGraph.get(DatabaseService.class).insertComplexity(complexities);
                    DependencyGraph.get(DatabaseService.class).insertMediaFiles(trainingPreviews);

                    DependencyGraph.get(IVideoLessonsRepository.class).updateAfterTrainings();

                    // разослать изменения наблюдателям
                    // так как запрос идет общий, на все типы каталогов
                    Set<Long> allRequestedCatalogs = mAllTrainingsLiveData.keySet();
                    for (Long requestedCatalog : allRequestedCatalogs) {
                        List<Training> updated = DependencyGraph.get(DatabaseService.class).getAllTrainingsByCatalogId(requestedCatalog);
                        if (updated != mAllTrainingsLiveData.get(requestedCatalog).getValue().getData()) {
                            RepositoryData<List<Training>> result = new RepositoryData<>(updated, DataSource.NETWORK);
                            mAllTrainingsLiveData.get(requestedCatalog).postValue(result);
                        }
                    }

                    mCached = true;
                },
                null);

        return mAllTrainingsLiveData.get(catalogId);
    }

    @Override
    public Training getTrainingById(long trainingId) {
        return DependencyGraph.get(DatabaseService.class).findTrainingById(trainingId);
    }

    @Override
    public void updateTraining(Training training) {
        // обновить запись в БД
        DependencyGraph.get(DatabaseService.class).insertTrainings(training);

        // отправить изменения наблюдателям
        long catalog = training.getCatalogId();
        MutableLiveData<RepositoryData<List<Training>>> current = mAllTrainingsLiveData.get(catalog);
        if (current != null) {
            List<Training> updated = DependencyGraph.get(DatabaseService.class).getAllTrainingsByCatalogId(catalog);
            DataSource source = mCached ? DataSource.CACHE : DataSource.DATABASE;
            RepositoryData<List<Training>> result = new RepositoryData<>(updated, source);
            current.setValue(result);
        }
    }
}
