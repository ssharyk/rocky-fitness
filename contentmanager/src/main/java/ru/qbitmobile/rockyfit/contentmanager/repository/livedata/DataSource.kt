package ru.qbitmobile.rockyfit.contentmanager.repository.livedata

enum class DataSource {
    // данные получены локально
    DATABASE,

    // данные получены с сервера
    NETWORK,

    // повторный запрос, подтвержденный данными с сервера
    CACHE,
}