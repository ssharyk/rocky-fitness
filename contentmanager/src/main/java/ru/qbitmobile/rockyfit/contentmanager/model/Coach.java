package ru.qbitmobile.rockyfit.contentmanager.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Property;

@Entity
public class Coach {
    @Id
    long id;
    @Property
    String firstName;
    @Property
    String lastName;

    // region Constructor

    public Coach() {
    }

    @Generated(hash = 795608645)
    public Coach(long id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    

    // endregion

    // region Get/set

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    // endregion

    // region Advanced get/set

    public String getFullName() {
        String name = (firstName != null) ? firstName : "";
        if (lastName != null) name += (" " + lastName);
        return name.trim();
    }
    // endregion
}
