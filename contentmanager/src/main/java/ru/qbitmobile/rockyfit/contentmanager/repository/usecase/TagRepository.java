package ru.qbitmobile.rockyfit.contentmanager.repository.usecase;

import androidx.lifecycle.MutableLiveData;

import ru.qbitmobile.rockyfit.commoncomponent.helper.CollectionConverter;
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.model.Tag;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;
import ru.qbitmobile.rockyfit.contentmanager.repository.livedata.DataSource;
import ru.qbitmobile.rockyfit.contentmanager.module.db.DatabaseService;
import ru.qbitmobile.rockyfit.contentmanager.repository.ITagRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.livedata.RepositoryData;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TagRepository implements ITagRepository {

    private MutableLiveData<RepositoryData<List<Tag>>> mAllTagsLiveData = new MutableLiveData<>();

    private boolean mCached;

    /**
     * Позволяет наблюдателям получать изменения в тегах
     * @return
     */
    @Override
    public MutableLiveData<RepositoryData<List<Tag>>> getTags() {
        List<Tag> cached = DependencyGraph.get(DatabaseService.class).getAllTags();
        DataSource source = mCached ? DataSource.CACHE : DataSource.DATABASE;
        mAllTagsLiveData.setValue(new RepositoryData<>(cached, source));
        return mAllTagsLiveData;
    }

    @Override
    public List<Tag> getTagsByCourseId(long courseId) {
        return new ArrayList<>();
    }

    @Override
    public List<Tag> getTagsByTrainingId(long trainingId) {
        Set<Tag> allTags = new HashSet<>();
        List<VideoLesson> lessonsInTraining = DependencyGraph.get(DatabaseService.class).findVideoLessonByTrainingId(trainingId);
        for (VideoLesson lesson : lessonsInTraining) {
            List<Tag> specificVideosTags = DependencyGraph.get(DatabaseService.class).findTagsByVideoId(lesson.getId());
            allTags.addAll(specificVideosTags);
        }
        return CollectionConverter.toList(allTags);
    }

    @Override
    public List<Tag> getTagsByVideoLessonId(long lessonId) {
        return DependencyGraph.get(DatabaseService.class).findTagsByVideoId(lessonId);
    }

    /**
     * Выполняет в БД обновление тегов, на основе данных, полученных из сети
     */
    @Override
    public void updateTagsWithNetwork(List<Tag> tags) {
        DependencyGraph.get(DatabaseService.class).clearTags();
        DependencyGraph.get(DatabaseService.class).insertTags(tags);

        mCached = true;
        List<Tag> updatedTags = DependencyGraph.get(DatabaseService.class).findTagsOfFreeVideos();
        mAllTagsLiveData.setValue(new RepositoryData<>(updatedTags, DataSource.NETWORK));
    }
}
