package ru.qbitmobile.rockyfit.contentmanager.repository.usecase;

import ru.qbitmobile.rockyfit.commoncomponent.helper.Now;
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph;
import ru.qbitmobile.rockyfit.contentmanager.model.Training;
import ru.qbitmobile.rockyfit.contentmanager.model.VideoLesson;
import ru.qbitmobile.rockyfit.contentmanager.module.db.DatabaseService;
import ru.qbitmobile.rockyfit.contentmanager.module.network.NetworkCallbackFailure;
import ru.qbitmobile.rockyfit.contentmanager.module.network.NetworkCallbackSuccess;
import ru.qbitmobile.rockyfit.contentmanager.module.network.NetworkService;
import ru.qbitmobile.rockyfit.contentmanager.module.network.model.Like;
import ru.qbitmobile.rockyfit.contentmanager.module.sharedpreference.SharedPreferenceService;
import ru.qbitmobile.rockyfit.contentmanager.repository.ILikeRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.ITrainingRepository;
import ru.qbitmobile.rockyfit.contentmanager.repository.IVideoLessonsRepository;

public class LikeRepository implements ILikeRepository {
    @Override
    public void setLike(VideoLesson videoLesson, boolean isSet) {
        // установить новые значения локально
        videoLesson.setIsLike(isSet);
        DependencyGraph.get(DatabaseService.class).insertVideoLessons(videoLesson);
        DependencyGraph.get(SharedPreferenceService.class).sharedSetVideoLikeMark(videoLesson.getId(), isSet);
        DependencyGraph.get(SharedPreferenceService.class).sharedSetVideoLikeMarkTimestamp(videoLesson.getId(), Now.get());

        // обновить данные у тренировки
        Training training = DependencyGraph.get(ITrainingRepository.class).getTrainingById(videoLesson.getTrainingId());
        if (training != null) {
            int delta = isSet ? 1 : -1;
            training.setLikes(training.getLikes() + delta);
            DependencyGraph.get(ITrainingRepository.class).updateTraining(training);
        }

        // отправить на сервер
        NetworkCallbackSuccess<Like> onOk = like -> {
            if (like.getLikeTimestamp() > 0L) {
                // успешно - обновить отметку времени, чтобы не повторять запрос в следующий раз
                DependencyGraph.get(SharedPreferenceService.class).sharedSetVideoLikeMarkTimestamp(videoLesson.getId(), like.getLikeTimestamp());

                videoLesson.setLikeUpdateTimestamp(like.getLikeTimestamp());
                videoLesson.setIsLike(like.isLike());
                videoLesson.setLikes(like.getTotalLikes());
                DependencyGraph.get(IVideoLessonsRepository.class).update(videoLesson);
                videoLesson.notifyUpdateLikes();
            }
        };
        NetworkCallbackFailure onError = (httpResponseCode, customErrorCodes) -> {
            // что-то пошло не так, повторить попытку при следующей загрузке
        };

        DependencyGraph.get(NetworkService.class).postLikeVideo(videoLesson.getId(), isSet, onOk, onError);
    }
}
