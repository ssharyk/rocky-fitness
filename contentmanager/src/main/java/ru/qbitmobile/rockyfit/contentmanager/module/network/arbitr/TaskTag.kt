package ru.qbitmobile.rockyfit.contentmanager.module.network.arbitr

sealed class TaskTag {
    class GetDataTaskTag() : TaskTag()

    data class LikeTaskTag(val videoId: Long): TaskTag()
}