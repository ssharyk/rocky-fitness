package ru.qbitmobile.rockyfit.contentmanager.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Transient;

@Entity
public class Tag {

    @Id
    String name;
    @Transient
    boolean isSelected;

    // region Constructor

    public Tag() {
    }

    @Generated(hash = 607833558)
    public Tag(String name) {
        this.name = name;
    }

    // endregion

    // region Get/set

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // endregion

    // region User interaction

    public boolean isSelected() {
        return this.isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    // endregion
}
