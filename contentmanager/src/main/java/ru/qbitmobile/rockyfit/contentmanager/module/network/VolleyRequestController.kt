package ru.qbitmobile.rockyfit.contentmanager.module.network

import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import ru.qbitmobile.rockyfit.contentmanager.BuildConfig
import ru.qbitmobile.rockyfit.contentmanager.DependencyGraph
import ru.qbitmobile.rockyfit.contentmanager.module.contextholder.IContextHolder
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

object VolleyRequestController {
    private val requestQueue: RequestQueue? =
            Volley.newRequestQueue(DependencyGraph.get(IContextHolder::class.java).context.applicationContext)

    @JvmStatic
    fun <T> addToRequestQueue(req: Request<T>) {
        requestQueue?.add(req)
    }

    @JvmStatic
    fun <T> buildGetRequest(uri: String, responseWrapper: ResponseWrapper<T>?): StringRequest {
        return this.buildRequest(Request.Method.GET, uri, responseWrapper)
    }

    @JvmStatic
    fun <T> buildPostRequest(uri: String, responseWrapper: ResponseWrapper<T>?): StringRequest {
        return this.buildRequest(Request.Method.POST, uri, responseWrapper)
    }

    private fun <T> buildRequest(method: Int, uri: String, responseWrapper: ResponseWrapper<T>?): StringRequest {
        log("Request [$method]: $uri")
        return BaseStringRequest(
                method,
                uri,
                Response.Listener {
                    doAsync {
                        log("Response ($uri): $it ${Thread.currentThread().name}")

                        try {
                            val data = responseWrapper?.parser?.parse(it)
                            uiThread {
                                if (data == null) {
                                    log("Data is NULL")
                                    responseWrapper?.errorCallback?.onFailure(NetworkCallbackFailure.NO_DATA)
                                    return@uiThread
                                }

                                if (data.status != 200) {
                                    log("Not 'ok' status (${data.status} -> ${data.errors.toTypedArray().contentToString()}")
                                    responseWrapper.errorCallback?.onFailure(data.status, *data.errorCodes)
                                    return@uiThread
                                }

                                if (data.data != null) {
                                    responseWrapper.successCallback?.onSuccess(data.data)
                                    log("Success: ${data.data} ${Thread.currentThread().name}")
                                } else {
                                    responseWrapper.errorCallback?.onFailure(data.status, *data.errorCodes)
                                }
                            }
                        } catch (e: Exception) {
                            log("ResponseError: ${e.message}")
                            responseWrapper?.errorCallback?.onFailure(NetworkCallbackFailure.PARSE_ERROR)
                        }
                    }
                },
                Response.ErrorListener {
                    log("NetworkError: ${it.message}")
                    responseWrapper?.errorCallback?.onFailure(it.networkResponse.statusCode)
                })
    }

    private val <T> ServerResponse<T>.errorCodes: Array<String>
        get() {
            val codesList = this.errors?.map { it.code } ?: emptyList()
            return codesList.toTypedArray()
        }

    private fun log(message: String) {
        if (BuildConfig.DEBUG) {
            println("Network:Volley > $message")
        }
    }
}