package ru.qbitmobile.rockyfit.contentmanager.module.network.arbitr

/**
 * Представляет сетевую операцию
 */
data class Task(
        var runnable: () -> Unit,       // код для исполнения
        var priority: Long,             // приоритет
        var taskAddress: Int,           // адрес задачи для исключения дублирования
        val apiName: String = "",       // название задачи
        val tag: TaskTag) {             // тег семейства задач

    // уникальный идентификатор
    var createTimestamp: Long = TaskIdGenerator.getNextId()

    @Volatile
    var onTaskCompleteListener: OnTaskCompleteListener? = null

    fun run() {
        runnable()
    }
}

object TaskIdGenerator {
    @Volatile
    private var counter: Long = 0

    @Synchronized
    fun getNextId(): Long {
        synchronized(counter) {
            counter++
        }
        return counter
    }
}