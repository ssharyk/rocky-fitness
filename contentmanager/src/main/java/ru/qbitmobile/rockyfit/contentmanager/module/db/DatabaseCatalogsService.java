package ru.qbitmobile.rockyfit.contentmanager.module.db;

import ru.qbitmobile.rockyfit.contentmanager.interfaces.Service;
import ru.qbitmobile.rockyfit.contentmanager.model.Catalog;

import java.util.List;

public interface DatabaseCatalogsService extends Service {
    Catalog findCatalogById(long catalogId);

    void insertCatalogs(Catalog... catalogs);
    void insertCatalogs(List<Catalog> catalogs);
}
