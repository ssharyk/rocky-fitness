package ru.qbitmobile.rockyfit.commoncomponent.helper

object Constant {
    object ObserverTypes {
        const val VIDEO_LESSON_MAIN_SCREEN = 1
        const val VIDEO_LESSON_SCHEDULE = 2
        const val VIDEO_LESSON_PREVIEWS = 3
        const val VIDEO_LESSON_VIEW = 4
    }

    object Subscription {
        const val SUBSCRIPTION_MONTH = "sub_mont"
    }
}