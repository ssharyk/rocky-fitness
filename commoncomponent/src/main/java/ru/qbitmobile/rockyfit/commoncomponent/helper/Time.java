package ru.qbitmobile.rockyfit.commoncomponent.helper;

/**
 * Обертка для преобразования формата времени
 */
public class Time {
    int hour;
    int minute;
    int second;

    public Time() {
    }

    public Time(long milliseconds) {
        setTime(milliseconds);
    }

    public Time(int hour, int minute, int second) {
        this(((hour * 60 + minute) * 60 + second) * 1000);
    }

    public static Time difference(long timestamp) {
        long now = System.currentTimeMillis();
        return new Time(timestamp - now);
    }

    public void setTime(long milliseconds) {
        second = (int) (milliseconds / 1000);
        if (second > 59) {
            minute += second / 60;
            second = second % 60;
            if (minute > 59) {
                hour += minute / 60;
                minute = minute % 60;
            }
        }
    }

    @Override
    public String toString() {
        if (hour > 0) {
            return hour + ":" + minute + ":" + getSecondString(second);
        } else
            return minute + ":" + getSecondString(second);
    }

    private String getSecondString(int seconds) {
        if (seconds >= 10) {
            return String.valueOf(seconds);
        } else {
            return '0' + String.valueOf(seconds);
        }
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }
}