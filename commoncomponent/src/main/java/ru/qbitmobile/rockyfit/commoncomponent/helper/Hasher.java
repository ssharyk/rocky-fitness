package ru.qbitmobile.rockyfit.commoncomponent.helper;

import java.util.ArrayList;
import java.util.List;

/**
 * Создает общий хеш всех переданных ему аргументов
 */
public class Hasher {

    public static int hash(Object argument) {
        List<Object> singleList = new ArrayList<>();
        singleList.add(argument);
        return hash(singleList);
    }

    public static int hash(List<Object> args) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < args.size(); i++) {
            if (args.get(i) instanceof long[]) {
                sb.append(hashArray((long[]) args.get(i)));
            } else if (args.get(i) instanceof Long[]) {
                sb.append(hashArray((Long[]) args.get(i)));
            } else {
                sb.append(args.get(i) == null ? "" : args.get(i).toString());
            }
        }

        return sb.toString().hashCode();
    }

    private static int hashArray(long[] args) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < args.length; i++) {
            sb.append(args[i]);
        }

        return sb.toString().hashCode();
    }

    private static int hashArray(Long[] args) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < args.length; i++) {
            sb.append(args[i]);
        }

        return sb.toString().hashCode();
    }
}