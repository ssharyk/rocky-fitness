package ru.qbitmobile.rockyfit.commoncomponent.helper;

public final class Now {
    public static long get() {
        return System.currentTimeMillis();
    }
}