package ru.qbitmobile.rockyfit.commoncomponent.helper

/**
 * Утилита для упрощения жизни Java
 */
object CollectionConverter {
    @JvmStatic
    fun <T> toList(set: Set<T>): List<T> = set.toList()

    @JvmStatic
    fun <T> shuffle(list: List<T>): List<T> = list.shuffled()
}