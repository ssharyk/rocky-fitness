package ru.qbitmobile.rockyfit.commoncomponent

import ru.qbitmobile.rockyfit.commoncomponent.helper.Time
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.ArgumentsProvider
import org.junit.jupiter.params.provider.ArgumentsSource
import java.util.stream.Stream

class TimeTest {
    @ParameterizedTest
    @ArgumentsSource(TimesProvider::class)
    fun whenToString_thenFormattedStringReturn(arg: TimeArgument) {
        Assertions.assertEquals(arg.str, arg.time.toString())
    }

}

class TimesProvider : ArgumentsProvider {
    override fun provideArguments(context: ExtensionContext?): Stream<out Arguments> = Stream.of(
            TimeArgument(Time(0, 0, 0), "0:00"),
            TimeArgument(Time(0, 1, 0), "1:00"),
            TimeArgument(Time(0, 20, 0), "20:00"),
            TimeArgument(Time(0, 60, 0), "1:0:00"),
            TimeArgument(Time(1, 23, 45), "1:23:45"),
            TimeArgument(Time(11, 23, 45), "11:23:45"),
            TimeArgument(Time(111, 23, 45), "111:23:45")

    ).map { Arguments.of(it) }
}

data class TimeArgument(val time: Time, val str: String)
