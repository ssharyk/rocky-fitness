package ru.qbitmobile.rockyfit.commoncomponent


import ru.qbitmobile.rockyfit.commoncomponent.helper.Hasher
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.extension.ExtensionContext
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.ArgumentsProvider
import org.junit.jupiter.params.provider.ArgumentsSource
import java.util.stream.Stream

class HasherTest {
    @ParameterizedTest
    @ArgumentsSource(ListHasherProvider::class)
    fun whenHashListOfValues_thenReturnHashCode(arg: ListHashArgument) {
        val actualHash = Hasher.hash(arg.args)
        Assertions.assertEquals(arg.hash, actualHash)
    }


    @ParameterizedTest
    @ArgumentsSource(SingleHasherProvider::class)
    fun whenHashSingleValue_thenReturnHashCode(arg: SingleHashArgument) {
        val actualHash = Hasher.hash(arg.argument)
        Assertions.assertEquals(arg.hash, actualHash)
    }
}

class ListHasherProvider : ArgumentsProvider {
    override fun provideArguments(context: ExtensionContext?): Stream<out Arguments> = Stream.of(
            ListHashArgument(listOf<Any>(), 0),
            ListHashArgument(listOf(1L, 2L, 3L, "4"), 1509442),
            ListHashArgument(listOf<Any>(arrayOf(1L, 2L, 3L)), 49745097)
    ).map { Arguments.of(it) }
}

class SingleHasherProvider : ArgumentsProvider {
    override fun provideArguments(context: ExtensionContext?): Stream<out Arguments> = Stream.of(
            SingleHashArgument("", 0),
            SingleHashArgument(0, "0".hashCode()),
            SingleHashArgument(1, "1".hashCode()),
            SingleHashArgument("1", "1".hashCode()),
            SingleHashArgument(arrayOf(1L, 2L, 3L), 49745097)
    ).map { Arguments.of(it) }
}

data class ListHashArgument(val args: List<Any>, val hash: Int)

data class SingleHashArgument(val argument: Any, val hash: Int)